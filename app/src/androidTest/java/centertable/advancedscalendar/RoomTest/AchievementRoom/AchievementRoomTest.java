package centertable.advancedscalendar.RoomTest.AchievementRoom;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import centertable.advancedscalendar.RoomTest.UserRoom.UserRoomTestUtil;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.AchievementDao;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.AchievementEntity;
import centertable.advancedscalendar.data.room.entity.UserEntity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rutkay on 08.02.2018.
 */

@RunWith(AndroidJUnit4.class)
public class AchievementRoomTest {
    private AchievementDao mAchievementDao;
    private UserDao mUserDao;
    private ASCDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, ASCDatabase.class).build();
        mAchievementDao = mDb.getAchievementDao();
        mUserDao = mDb.getUserDao();

        UserEntity userEntity = UserRoomTestUtil.createBasicEntity();

        User user = new User();
        user.loadEntity(userEntity);
        mUserDao.insert(userEntity);

        AuthenticationInfo.getInstance().setCurrentUser(context, user);
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void createNewAchievementAndReadInList() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mAchievementDao.clearAllAchievementsForUser(currentUserId);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 0);

        AchievementEntity achievement = AchievementRoomTestUtil.createBasicEntity(currentUserId);

        mAchievementDao.insert(achievement);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 1);

        AchievementEntity firstAchievement = mAchievementDao.getAllAchievementsForUser(currentUserId).get(0);

        assertEquals(AchievementRoomTestUtil.isEqual(firstAchievement, achievement), true);

    }

    @Test
    public void createNewAchievementAndModifyThatAchievement() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mAchievementDao.clearAllAchievementsForUser(currentUserId);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 0);

        AchievementEntity achievement = AchievementRoomTestUtil.createBasicEntity(currentUserId);

        boolean newStatus = !achievement.achievementStatus;

        mAchievementDao.insert(achievement);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 1);

        AchievementEntity firstAchievement = mAchievementDao.getAllAchievementsForUser(currentUserId).get(0);

        assertEquals(firstAchievement.achievementStatus, achievement.achievementStatus);

        firstAchievement.achievementStatus = newStatus;

        mAchievementDao.update(firstAchievement);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 1);

        AchievementEntity updatedAchievement = mAchievementDao.getAllAchievementsForUser(currentUserId).get(0);

        assertEquals(updatedAchievement.userId, achievement.userId);
        assertEquals(updatedAchievement.achievement, achievement.achievement);
        assertEquals(updatedAchievement.achievementStatus, newStatus);

    }

    @Test
    public void createNewAchievementAndInsertAchievementWithSameId() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mAchievementDao.clearAllAchievementsForUser(currentUserId);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 0);

        AchievementEntity achievement = AchievementRoomTestUtil.createBasicEntity(currentUserId);

        boolean newStatus = !achievement.achievementStatus;

        mAchievementDao.insert(achievement);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 1);

        AchievementEntity firstAchievement = mAchievementDao.getAllAchievementsForUser(currentUserId).get(0);

        assertEquals(firstAchievement.achievementStatus, achievement.achievementStatus);

        firstAchievement.achievementStatus = newStatus;

        long[] res = mAchievementDao.insert(firstAchievement);
        assertEquals(res[0], 1);

        assertEquals(mAchievementDao.getAllAchievementsForUser(currentUserId).size(), 1);

        AchievementEntity updatedAchievement = mAchievementDao.getAllAchievementsForUser(currentUserId).get(0);

        assertEquals(updatedAchievement.userId, achievement.userId);
        assertEquals(updatedAchievement.achievement, achievement.achievement);
        assertEquals(updatedAchievement.achievementStatus, newStatus);

    }
}
