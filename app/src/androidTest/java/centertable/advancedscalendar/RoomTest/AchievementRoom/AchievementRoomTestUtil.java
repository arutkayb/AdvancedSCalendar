package centertable.advancedscalendar.RoomTest.AchievementRoom;

import centertable.advancedscalendar.data.room.entity.AchievementEntity;

/**
 * Created by Rutkay on 08.02.2018.
 */

public class AchievementRoomTestUtil {
    
    public static boolean isEqual(AchievementEntity entity1, AchievementEntity entity2)
    {
       if(entity1.uId != entity2.uId)
           return false;
       
       if(entity1.userId != entity2.userId)
           return false;

        if(entity1.achievement != entity2.achievement)
            return false;

        if(entity1.achievementStatus != entity2.achievementStatus)
            return false;

        return true;
    }


    public static AchievementEntity createEntity(int uId, int userId, int achievement, boolean achievementStatus)
    {
        return (new AchievementEntity(
                uId,
                userId,
                achievement,
                achievementStatus));
    }

    public static AchievementEntity createBasicEntity(long userId)
    {
        long uId = 1;
        int achievement = 0;
        boolean achievementStatus = false;
            
        return (new AchievementEntity(
                uId,
                userId,
                achievement,
                achievementStatus));
    }
}
