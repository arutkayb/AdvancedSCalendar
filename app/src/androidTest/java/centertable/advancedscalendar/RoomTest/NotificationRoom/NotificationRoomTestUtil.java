package centertable.advancedscalendar.RoomTest.NotificationRoom;

import centertable.advancedscalendar.data.room.entity.NotificationEntity;

/**
 * Created by Rutkay on 08.02.2018.
 */

public class NotificationRoomTestUtil {
    
    public static boolean isEqual(NotificationEntity entity1, NotificationEntity entity2)
    {
       if(entity1.uId != entity2.uId)
           return false;
       
       if(entity1.userId != entity2.userId)
           return false;

        if(entity1.notification != entity2.notification)
            return false;

        if(entity1.notificationStatus != entity2.notificationStatus)
            return false;

        return true;
    }


    public static NotificationEntity createEntity(int uId, int userId, int notification, boolean notificationStatus)
    {
        return (new NotificationEntity(
                uId,
                userId,
                notification,
                notificationStatus));
    }

    public static NotificationEntity createBasicEntity(long userId)
    {
        int uId = 1;
        int notification = 0;
        boolean notificationStatus = false;

        return (new NotificationEntity(
                uId,
                userId,
                notification,
                notificationStatus));
    }
}
