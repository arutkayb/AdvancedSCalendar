package centertable.advancedscalendar.RoomTest.NotificationRoom;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import centertable.advancedscalendar.RoomTest.UserRoom.UserRoomTestUtil;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.NotificationDao;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.NotificationEntity;
import centertable.advancedscalendar.data.room.entity.UserEntity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rutkay on 08.02.2018.
 */

@RunWith(AndroidJUnit4.class)
public class NotificationRoomTest {
    private NotificationDao mNotificationDao;
    private UserDao mUserDao;
    private ASCDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, ASCDatabase.class).build();
        mNotificationDao = mDb.getNotificationDao();
        mUserDao = mDb.getUserDao();

        UserEntity userEntity = UserRoomTestUtil.createBasicEntity();

        User user = new User();
        user.loadEntity(userEntity);
        mUserDao.insert(userEntity);

        AuthenticationInfo.getInstance().setCurrentUser(context, user);
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void createNewNotificationAndReadInList() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mNotificationDao.clearAllNotificationsForUser(currentUserId);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 0);

        NotificationEntity notification = NotificationRoomTestUtil.createBasicEntity(currentUserId);

        mNotificationDao.insert(notification);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 1);

        NotificationEntity firstNotification = mNotificationDao.getAllNotificationsForUser(currentUserId).get(0);

        assertEquals(NotificationRoomTestUtil.isEqual(firstNotification, notification), true);

    }

    @Test
    public void createNewNotificationAndModifyThatNotification() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mNotificationDao.clearAllNotificationsForUser(currentUserId);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 0);

        NotificationEntity notification = NotificationRoomTestUtil.createBasicEntity(currentUserId);

        mNotificationDao.insert(notification);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 1);

        NotificationEntity firstNotification = mNotificationDao.getAllNotificationsForUser(currentUserId).get(0);

        boolean newStatus = !notification.notificationStatus;

        assertEquals(firstNotification.notificationStatus, notification.notificationStatus);

        firstNotification.notificationStatus = newStatus;

        mNotificationDao.update(firstNotification);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 1);

        NotificationEntity updatedNotification = mNotificationDao.getAllNotificationsForUser(currentUserId).get(0);

        assertEquals(updatedNotification.userId, firstNotification.userId);
        assertEquals(updatedNotification.notification, firstNotification.notification);
        assertEquals(updatedNotification.notificationStatus, newStatus);

    }

    @Test
    public void createNewNotificationAndInsertNotificationWithSameId() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mNotificationDao.clearAllNotificationsForUser(currentUserId);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 0);

        NotificationEntity notification = NotificationRoomTestUtil.createBasicEntity(currentUserId);

        mNotificationDao.insert(notification);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 1);

        NotificationEntity firstNotification = mNotificationDao.getAllNotificationsForUser(currentUserId).get(0);

        boolean newStatus = !notification.notificationStatus;

        assertEquals(firstNotification.notificationStatus, notification.notificationStatus);

        firstNotification.notificationStatus = newStatus;

        long[] res = mNotificationDao.insert(firstNotification);
        assertEquals(res[0], 1);

        assertEquals(mNotificationDao.getAllNotificationsForUser(currentUserId).size(), 1);

        NotificationEntity updatedNotification = mNotificationDao.getAllNotificationsForUser(currentUserId).get(0);

        assertEquals(updatedNotification.userId, notification.userId);
        assertEquals(updatedNotification.notification, notification.notification);
        assertEquals(updatedNotification.notificationStatus, newStatus);

    }
}
