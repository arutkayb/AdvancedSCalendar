package centertable.advancedscalendar.RoomTest.EntryRoom;

import centertable.advancedscalendar.data.room.entity.EntryEntity;
import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;
import centertable.advancedscalendar.data.room.entity.EntryPhotoEntity;
import centertable.advancedscalendar.data.room.entity.EntryPlaceEntity;
import centertable.advancedscalendar.data.room.entity.EntrySexTypeEntity;

/**
 * Created by Rutkay on 08.02.2018.
 */

public class EntryRoomTestUtil {
    public static boolean isEqual(EntryEntity entity1, EntryEntity entity2)
    {
        if(entity1.entryId != entity2.entryId )
            return false;

        if(entity1.userId != entity2.userId )
            return false;

        if(entity1.duration != entity2.duration )
            return false;

        if(entity1.rating != entity2.rating)
            return false;

        if(entity1.safetyStatus != entity2.safetyStatus )
            return false;

        if(entity1.totalOrg != entity2.totalOrg )
            return false;

        if(entity1.dateString.compareTo(entity2.dateString) != 0 )
            return false;

        if(entity1.note.compareTo(entity2.note) != 0 )
            return false;

        return true;
    }

    public static boolean isEqual(EntryPartnerEntity entity1, EntryPartnerEntity entity2)
    {
        if(entity1.uId != entity2.uId )
            return false;

        if(entity1.partnerId != entity2.partnerId )
            return false;

        if(entity1.entryId != entity2.entryId )
            return false;

        return true;
    }

    public static boolean isEqual(EntryPhotoEntity entity1, EntryPhotoEntity entity2)
    {
        if(entity1.uId != entity2.uId )
            return false;

        if(entity1.photoLocation.compareTo(entity2.photoLocation) != 0 )
            return false;

        if(entity1.entryId != entity2.entryId )
            return false;

        return true;
    }

    public static boolean isEqual(EntryPlaceEntity entity1, EntryPlaceEntity entity2)
    {
        if(entity1.uId != entity2.uId )
            return false;

        if(entity1.placeId != entity2.placeId )
            return false;

        if(entity1.entryId != entity2.entryId )
            return false;

        return true;
    }

    public static boolean isEqual(EntrySexTypeEntity entity1, EntrySexTypeEntity entity2)
    {
        if(entity1.uId != entity2.uId )
            return false;

        if(entity1.sexTypeId != entity2.sexTypeId)
            return false;

        if(entity1.entryId != entity2.entryId )
            return false;

        return true;
    }


    public static EntryEntity createBasicEntryEntity(long userId)
    {
        long entryId = 1;
        String dateString = "01.01.2000";
        int duration = 0;
        String note = "note";
        int rating = 0;
        boolean safetyStatus = false;
        int totalOrg = 0;
                
        return (new EntryEntity(entryId, userId, dateString, duration, note, rating, safetyStatus, totalOrg));
    }

    public static EntryPartnerEntity createBasicEntryPartnerEntity(long entryId,long partnerId)
    {
        int uId = 1;
        return (new EntryPartnerEntity(uId, entryId, entryId));
    }

    public static EntryPartnerEntity createEntryPartnerEntity(long uId, long entryId,long partnerId)
    {
        return (new EntryPartnerEntity(uId, entryId, entryId));
    }

    public static EntryPhotoEntity createBasicEntryPhotoEntity(long entryId)
    {
        int uId = 1;
        String photoLocation = "photoLocation";
        return (new EntryPhotoEntity(uId, entryId, photoLocation));
    }

    public static EntryPlaceEntity createBasicEntryPlaceEntity(long entryId)
    {
        int uId = 1;
        int placeId = 0;
        return (new EntryPlaceEntity(uId, entryId, placeId));
    }

    public static EntrySexTypeEntity createBasicEntrySexTypeEntity(long entryId)
    {
        int uId = 1;
        int sexTypeId = 0;
        return (new EntrySexTypeEntity(uId, sexTypeId, entryId));
    }
}
