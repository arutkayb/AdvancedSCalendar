package centertable.advancedscalendar.RoomTest.EntryRoom;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import centertable.advancedscalendar.RoomTest.PartnerRoom.PartnerRoomTestUtil;
import centertable.advancedscalendar.RoomTest.UserRoom.UserRoomTestUtil;
import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.EntryDao;
import centertable.advancedscalendar.data.room.dao.EntryPartnerDao;
import centertable.advancedscalendar.data.room.dao.EntryPhotoDao;
import centertable.advancedscalendar.data.room.dao.EntryPlaceDao;
import centertable.advancedscalendar.data.room.dao.EntrySexTypeDao;
import centertable.advancedscalendar.data.room.dao.PartnerDao;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.EntryEntity;
import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;
import centertable.advancedscalendar.data.room.entity.EntryPhotoEntity;
import centertable.advancedscalendar.data.room.entity.EntryPlaceEntity;
import centertable.advancedscalendar.data.room.entity.EntrySexTypeEntity;
import centertable.advancedscalendar.data.room.entity.PartnerEntity;
import centertable.advancedscalendar.data.room.entity.UserEntity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

import static org.junit.Assert.assertEquals;
/**
 * Created by Rutkay on 08.02.2018.
 */


@RunWith(AndroidJUnit4.class)
public class EntryRoomTest {
    private EntryDao mEntryDao;
    private EntrySexTypeDao mEntrySexTypeDao;
    private EntryPlaceDao mEntryPlaceDao;
    private EntryPhotoDao mEntryPhotoDao;
    private EntryPartnerDao mEntryPartnerDao;

    private UserDao mUserDao;
    private PartnerDao mPartnerDao;
    private ASCDatabase mDb;
    private int mPartnerId;
    private int mNewPartnerId;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, ASCDatabase.class).build();
        mEntryDao = mDb.getEntryDao();
        mEntrySexTypeDao = mDb.getEntrySexTypeDao();
        mEntryPlaceDao = mDb.getEntryPlaceDao();
        mEntryPhotoDao = mDb.getEntryPhotoDao();
        mEntryPartnerDao = mDb.getEntryPartnerDao();

        mUserDao = mDb.getUserDao();
        mPartnerDao = mDb.getPartnerDao();

        UserEntity userEntity = UserRoomTestUtil.createBasicEntity();
        User user = new User();
        user.loadEntity(userEntity);
        mUserDao.insert(userEntity);
        AuthenticationInfo.getInstance().setCurrentUser(context, user);

        PartnerEntity partnerEntity = PartnerRoomTestUtil.createBasicEntity(AuthenticationInfo.getInstance().getCurrentUserId());
        mPartnerId = 1;
        partnerEntity.partnerId = mPartnerId;
        Partner partner = new Partner();
        partner.loadEntity(partnerEntity);
        mPartnerDao.insert(partnerEntity);

        PartnerEntity newPartnerEntity = PartnerRoomTestUtil.createBasicEntity(AuthenticationInfo.getInstance().getCurrentUserId());
        mNewPartnerId = 2;
        newPartnerEntity.partnerId = mNewPartnerId;
        Partner partner2 = new Partner();
        partner2.loadEntity(newPartnerEntity);
        mPartnerDao.insert(newPartnerEntity);
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void createNewEntryWithRelatedEntitiesAndReadInList()
    {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        long currentEntryId;

        //EntryEntity
        mEntryDao.clearAllEntriesForUser(currentUserId);

        EntryEntity entryEntity = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        mEntryDao.insert(entryEntity);

        List<EntryEntity> entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(0), entryEntity), true);
        currentEntryId = entryEntity.entryId;

        //EntrySexTypeEntity
        mEntrySexTypeDao.clearAllSexTypesForEntry(currentEntryId);

        EntrySexTypeEntity entrySexTypeEntity = EntryRoomTestUtil.createBasicEntrySexTypeEntity(currentEntryId);
        mEntrySexTypeDao.insert(entrySexTypeEntity);

        List<EntrySexTypeEntity> entrySexTypeList = mEntrySexTypeDao.getAllSexTypesForEntry(currentEntryId);
        assertEquals(entrySexTypeList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entrySexTypeList.get(0), entrySexTypeEntity), true);

        //EntryPlaceEntity
        mEntryPlaceDao.clearAllPlacesForEntry(currentEntryId);

        EntryPlaceEntity entryPlaceEntity = EntryRoomTestUtil.createBasicEntryPlaceEntity(currentEntryId);
        mEntryPlaceDao.insert(entryPlaceEntity);

        List<EntryPlaceEntity> entryPlaceList = mEntryPlaceDao.getAllPlacesForEntry(currentEntryId);
        assertEquals(entryPlaceList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPlaceList.get(0), entryPlaceEntity), true);

        //EntryPhotoEntity
        mEntryPhotoDao.clearAllPhotosForEntry(currentEntryId);

        EntryPhotoEntity entryPhotoEntity = EntryRoomTestUtil.createBasicEntryPhotoEntity(currentEntryId);
        mEntryPhotoDao.insert(entryPhotoEntity);

        List<EntryPhotoEntity> entryPhotoList = mEntryPhotoDao.getAllPhotosForEntry(currentEntryId);
        assertEquals(entryPhotoList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPhotoList.get(0), entryPhotoEntity), true);

        //EntryPartnerEntity
        mEntryPartnerDao.clearAllPartnersForEntry(currentEntryId);

        EntryPartnerEntity entryPartnerEntity = EntryRoomTestUtil.createBasicEntryPartnerEntity(currentEntryId, mPartnerId);
        mEntryPartnerDao.insert(entryPartnerEntity);

        List<EntryPartnerEntity> entryPartnerList = mEntryPartnerDao.getAllPartnersForEntry(currentEntryId);
        assertEquals(entryPartnerList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPartnerList.get(0), entryPartnerEntity), true);
    }

    @Test
    public void createNewEntryWithRelatedEntitiesAndModifyThem()
    {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        long currentEntryId;

        String newNote = "newNote";
        int newPosId = 2;
        int newPlaceId = 3;
        String newPhotoLocation = "newPhotoLocation";
        int newPartnerId = 2;

        //EntryEntity
        mEntryDao.clearAllEntriesForUser(currentUserId);

        EntryEntity entryEntity = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        mEntryDao.insert(entryEntity);

        List<EntryEntity> entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(0), entryEntity), true);
        currentEntryId = entryEntity.entryId;

        //modify EntryEntity and update
        entryEntity.note = newNote;
        mEntryDao.update(entryEntity);

        entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 1);
        assertEquals(entryList.get(0).note, newNote);


        //EntrySexTypeEntity
        mEntrySexTypeDao.clearAllSexTypesForEntry(currentEntryId);

        EntrySexTypeEntity entrySexTypeEntity = EntryRoomTestUtil.createBasicEntrySexTypeEntity(currentEntryId);
        mEntrySexTypeDao.insert(entrySexTypeEntity);

        List<EntrySexTypeEntity> entrySexTypeList = mEntrySexTypeDao.getAllSexTypesForEntry(currentEntryId);
        assertEquals(entrySexTypeList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entrySexTypeList.get(0), entrySexTypeEntity), true);

        //modify EntrySexTypeEntity and update
        entrySexTypeEntity.sexTypeId = newPosId;
        mEntrySexTypeDao.update(entrySexTypeEntity);

        entrySexTypeList = mEntrySexTypeDao.getAllSexTypesForEntry(currentEntryId);
        assertEquals(entrySexTypeList.size(), 1);
        assertEquals(entrySexTypeList.get(0).sexTypeId, newPosId);

        //EntryPlaceEntity
        mEntryPlaceDao.clearAllPlacesForEntry(currentEntryId);

        EntryPlaceEntity entryPlaceEntity = EntryRoomTestUtil.createBasicEntryPlaceEntity(currentEntryId);
        mEntryPlaceDao.insert(entryPlaceEntity);

        List<EntryPlaceEntity> entryPlaceList = mEntryPlaceDao.getAllPlacesForEntry(currentEntryId);
        assertEquals(entryPlaceList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPlaceList.get(0), entryPlaceEntity), true);

        //modify EntryPlaceEntity and update
        entryPlaceEntity.placeId = newPlaceId;
        mEntryPlaceDao.update(entryPlaceEntity);

        entryPlaceList = mEntryPlaceDao.getAllPlacesForEntry(currentEntryId);
        assertEquals(entryPlaceList.size(), 1);
        assertEquals(entryPlaceList.get(0).placeId, newPlaceId);

        //EntryPhotoEntity
        mEntryPhotoDao.clearAllPhotosForEntry(currentEntryId);

        EntryPhotoEntity entryPhotoEntity = EntryRoomTestUtil.createBasicEntryPhotoEntity(currentEntryId);
        mEntryPhotoDao.insert(entryPhotoEntity);

        List<EntryPhotoEntity> entryPhotoList = mEntryPhotoDao.getAllPhotosForEntry(currentEntryId);
        assertEquals(entryPhotoList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPhotoList.get(0), entryPhotoEntity), true);

        //modify EntryPhotoEntity and update
        entryPhotoEntity.photoLocation = newPhotoLocation;
        mEntryPhotoDao.update(entryPhotoEntity);

        entryPhotoList = mEntryPhotoDao.getAllPhotosForEntry(currentEntryId);
        assertEquals(entryPhotoList.size(), 1);
        assertEquals(entryPhotoList.get(0).photoLocation, newPhotoLocation);

        //EntryPartnerEntity
        mEntryPartnerDao.clearAllPartnersForEntry(currentEntryId);

        EntryPartnerEntity entryPartnerEntity = EntryRoomTestUtil.createBasicEntryPartnerEntity(currentEntryId, mPartnerId);
        mEntryPartnerDao.insert(entryPartnerEntity);

        List<EntryPartnerEntity> entryPartnerList = mEntryPartnerDao.getAllPartnersForEntry(currentEntryId);
        assertEquals(entryPartnerList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPartnerList.get(0), entryPartnerEntity), true);

        //modify EntryPartnerEntity and update

        entryPartnerEntity.partnerId = newPartnerId;
        mEntryPartnerDao.update(entryPartnerEntity);

        entryPartnerList = mEntryPartnerDao.getAllPartnersForEntry(currentEntryId);
        assertEquals(entryPartnerList.size(), 1);
        assertEquals(entryPartnerList.get(0).partnerId, newPartnerId);

        //add second partner to entry
        EntryPartnerEntity entryPartnerEntity2 = EntryRoomTestUtil.createEntryPartnerEntity(2, currentEntryId, mNewPartnerId);
        mEntryPartnerDao.insert(entryPartnerEntity2);

        entryPartnerList = mEntryPartnerDao.getAllPartnersForEntry(currentEntryId);
        assertEquals(entryPartnerList.size(), 2);
        assertEquals(EntryRoomTestUtil.isEqual(entryPartnerList.get(0), entryPartnerEntity), true);
        assertEquals(EntryRoomTestUtil.isEqual(entryPartnerList.get(1), entryPartnerEntity2), true);
    }


    @Test
    public void createNewEntryWithRelatedEntitiesAndInsertThemWithSameIds()
    {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        long currentEntryId;

        String newNote = "newNote";
        int newPosId = 2;
        int newPlaceId = 3;
        String newPhotoLocation = "newPhotoLocation";
        int newPartnerId = 2;
        long[] res;

        //EntryEntity
        mEntryDao.clearAllEntriesForUser(currentUserId);

        EntryEntity entryEntity = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        mEntryDao.insert(entryEntity);

        List<EntryEntity> entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(0), entryEntity), true);
        currentEntryId = entryEntity.entryId;

        //modify EntryEntity and update
        String oldNote = entryEntity.note;
        entryEntity.note = newNote;
        res = mEntryDao.insert(entryEntity);
        assertEquals(res[0], 1);

        entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 1);
        assertEquals(entryList.get(0).note, newNote);


        //EntrySexTypeEntity
        mEntrySexTypeDao.clearAllSexTypesForEntry(currentEntryId);

        EntrySexTypeEntity entrySexTypeEntity = EntryRoomTestUtil.createBasicEntrySexTypeEntity(currentEntryId);
        mEntrySexTypeDao.insert(entrySexTypeEntity);

        List<EntrySexTypeEntity> entrySexTypeList = mEntrySexTypeDao.getAllSexTypesForEntry(currentEntryId);
        assertEquals(entrySexTypeList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entrySexTypeList.get(0), entrySexTypeEntity), true);

        //modify EntrySexTypeEntity and update
        int oldPosId = entrySexTypeEntity.sexTypeId;
        entrySexTypeEntity.sexTypeId = newPosId;
        res = mEntrySexTypeDao.insert(entrySexTypeEntity);
        assertEquals(res[0], 1);

        entrySexTypeList = mEntrySexTypeDao.getAllSexTypesForEntry(currentEntryId);
        assertEquals(entrySexTypeList.size(), 1);
        assertEquals(entrySexTypeList.get(0).sexTypeId, newPosId);

        //EntryPlaceEntity
        mEntryPlaceDao.clearAllPlacesForEntry(currentEntryId);

        EntryPlaceEntity entryPlaceEntity = EntryRoomTestUtil.createBasicEntryPlaceEntity(currentEntryId);
        mEntryPlaceDao.insert(entryPlaceEntity);

        List<EntryPlaceEntity> entryPlaceList = mEntryPlaceDao.getAllPlacesForEntry(currentEntryId);
        assertEquals(entryPlaceList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPlaceList.get(0), entryPlaceEntity), true);

        //modify EntryPlaceEntity and update
        int oldPlaceId = entryPlaceEntity.placeId;
        entryPlaceEntity.placeId = newPlaceId;
        res = mEntryPlaceDao.insert(entryPlaceEntity);
        assertEquals(res[0], 1);

        entryPlaceList = mEntryPlaceDao.getAllPlacesForEntry(currentEntryId);
        assertEquals(entryPlaceList.size(), 1);
        assertEquals(entryPlaceList.get(0).placeId, newPlaceId);

        //EntryPhotoEntity
        mEntryPhotoDao.clearAllPhotosForEntry(currentEntryId);

        EntryPhotoEntity entryPhotoEntity = EntryRoomTestUtil.createBasicEntryPhotoEntity(currentEntryId);
        mEntryPhotoDao.insert(entryPhotoEntity);

        List<EntryPhotoEntity> entryPhotoList = mEntryPhotoDao.getAllPhotosForEntry(currentEntryId);
        assertEquals(entryPhotoList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPhotoList.get(0), entryPhotoEntity), true);

        //modify EntryPhotoEntity and update
        String oldPhotoLocation = entryPhotoEntity.photoLocation;
        entryPhotoEntity.photoLocation = newPhotoLocation;
        res = mEntryPhotoDao.insert(entryPhotoEntity);
        assertEquals(res[0], 1);

        entryPhotoList = mEntryPhotoDao.getAllPhotosForEntry(currentEntryId);
        assertEquals(entryPhotoList.size(), 1);
        assertEquals(entryPhotoList.get(0).photoLocation, newPhotoLocation);

        //EntryPartnerEntity
        mEntryPartnerDao.clearAllPartnersForEntry(currentEntryId);

        EntryPartnerEntity entryPartnerEntity = EntryRoomTestUtil.createBasicEntryPartnerEntity(currentEntryId, mPartnerId);
        mEntryPartnerDao.insert(entryPartnerEntity);

        List<EntryPartnerEntity> entryPartnerList = mEntryPartnerDao.getAllPartnersForEntry(currentEntryId);
        assertEquals(entryPartnerList.size(), 1);
        assertEquals(EntryRoomTestUtil.isEqual(entryPartnerList.get(0), entryPartnerEntity), true);

        //modify EntryPartnerEntity and update
        long oldPartnerId = entryPartnerEntity.partnerId;
        entryPartnerEntity.partnerId = newPartnerId;
        res = mEntryPartnerDao.insert(entryPartnerEntity);
        assertEquals(res[0], 1);

        entryPartnerList = mEntryPartnerDao.getAllPartnersForEntry(currentEntryId);
        assertEquals(entryPartnerList.size(), 1);
        assertEquals(entryPartnerList.get(0).partnerId, newPartnerId);
    }

    @Test
    public void createTwoNewEntriesWithRelatedEntitiesAndReadInList()
    {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        long currentEntryId;

        mEntryDao.clearAllEntriesForUser(currentUserId);

        EntryEntity entryEntity = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        currentEntryId = mEntryDao.insert(entryEntity)[0];

        EntryEntity entryEntity2 = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        entryEntity2.entryId = (int)currentEntryId + 1;
        currentEntryId = mEntryDao.insert(entryEntity2)[0];

        List<EntryEntity> entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 2);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(0), entryEntity), true);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(1), entryEntity2), true);

    }

    @Test
    public void createEntriesAndSelectEntriesInParticularTimeInterval()
    {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        long currentEntryId;

        mEntryDao.clearAllEntriesForUser(currentUserId);

        EntryEntity entryEntity = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        entryEntity.dateString = "2000.01.02";
        currentEntryId = mEntryDao.insert(entryEntity)[0];

        EntryEntity entryEntity2 = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        entryEntity2.entryId = (int)currentEntryId + 1;
        entryEntity2.dateString = "2000.01.05";
        currentEntryId = mEntryDao.insert(entryEntity2)[0];

        EntryEntity entryEntity3 = EntryRoomTestUtil.createBasicEntryEntity(currentUserId);
        entryEntity3.entryId = (int)currentEntryId + 1;
        entryEntity3.dateString = "2000.01.07";
        currentEntryId = mEntryDao.insert(entryEntity3)[0];

        List<EntryEntity> entryList = mEntryDao.getAllEntriesForUser(currentUserId);
        assertEquals(entryList.size(), 3);

        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(0), entryEntity), true);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(1), entryEntity2), true);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(2), entryEntity3), true);

        entryList = mEntryDao.getEntriesForUserForTimeInterval(currentUserId,"2000.01.05", "2000.01.07");
        assertEquals(entryList.size(), 2);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(0), entryEntity2), true);
        assertEquals(EntryRoomTestUtil.isEqual(entryList.get(1), entryEntity3), true);

    }
}
