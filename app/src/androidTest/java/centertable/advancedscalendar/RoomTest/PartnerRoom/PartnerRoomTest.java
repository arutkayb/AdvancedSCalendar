package centertable.advancedscalendar.RoomTest.PartnerRoom;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import centertable.advancedscalendar.RoomTest.UserRoom.UserRoomTestUtil;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.PartnerDao;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.PartnerEntity;
import centertable.advancedscalendar.data.room.entity.UserEntity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rutkay on 08.02.2018.
 */

@RunWith(AndroidJUnit4.class)
public class PartnerRoomTest {
    private PartnerDao mPartnerDao;
    private UserDao mUserDao;
    private ASCDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, ASCDatabase.class).build();
        mPartnerDao = mDb.getPartnerDao();
        mUserDao = mDb.getUserDao();

        UserEntity userEntity = UserRoomTestUtil.createBasicEntity();

        User user = new User();
        user.loadEntity(userEntity);
        mUserDao.insert(userEntity);

        AuthenticationInfo.getInstance().setCurrentUser(context, user);
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void createNewPartnerAndReadInList() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();

        mPartnerDao.clearAllPartnersForUser(currentUserId);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 0);

        PartnerEntity partner = PartnerRoomTestUtil.createBasicEntity(currentUserId);

        mPartnerDao.insert(partner);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 1);

        PartnerEntity firstPartner = mPartnerDao.getAllPartnersForUser(currentUserId).get(0);

        assertEquals(PartnerRoomTestUtil.isEqual(firstPartner, partner), true);

    }

    @Test
    public void createNewPartnerAndModifyThatPartner() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        String newName = "rutkay2";

        mPartnerDao.clearAllPartnersForUser(currentUserId);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 0);

        PartnerEntity partner = PartnerRoomTestUtil.createBasicEntity(currentUserId);

        mPartnerDao.insert(partner);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 1);

        PartnerEntity firstPartner = mPartnerDao.getAllPartnersForUser(currentUserId).get(0);

        assertEquals(firstPartner.name, partner.name);

        firstPartner.name = newName;

        mPartnerDao.update(firstPartner);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 1);

        PartnerEntity updatedPartner = mPartnerDao.getAllPartnersForUser(currentUserId).get(0);

        assertEquals(updatedPartner.userId, partner.userId);
        assertEquals(updatedPartner.partnerId, partner.partnerId);
        assertEquals(updatedPartner.name, newName);

    }

    @Test
    public void createNewPartnerAndInsertUserWithSameId() throws Exception {
        long currentUserId = AuthenticationInfo.getInstance().getCurrentUserId();
        String newName = "rutkay2";

        mPartnerDao.clearAllPartnersForUser(currentUserId);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 0);

        PartnerEntity partner = PartnerRoomTestUtil.createBasicEntity(currentUserId);

        mPartnerDao.insert(partner);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 1);

        PartnerEntity firstPartner = mPartnerDao.getAllPartnersForUser(currentUserId).get(0);

        assertEquals(firstPartner.name, partner.name);

        firstPartner.name = newName;

        long[] res = mPartnerDao.insert(firstPartner);
        assertEquals(res[0], 1);

        assertEquals(mPartnerDao.getAllPartnersForUser(currentUserId).size(), 1);

        PartnerEntity updatedPartner = mPartnerDao.getAllPartnersForUser(currentUserId).get(0);

        assertEquals(updatedPartner.userId, partner.userId);
        assertEquals(updatedPartner.partnerId, partner.partnerId);
        assertEquals(updatedPartner.name, newName);

    }
}
