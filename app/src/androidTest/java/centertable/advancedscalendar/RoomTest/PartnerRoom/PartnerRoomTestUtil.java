package centertable.advancedscalendar.RoomTest.PartnerRoom;

import centertable.advancedscalendar.data.room.entity.PartnerEntity;

/**
 * Created by Rutkay on 08.02.2018.
 */

public class PartnerRoomTestUtil {
    public static boolean isEqual(PartnerEntity entity1, PartnerEntity entity2)
    {
        if(entity1.userId != entity2.userId)
            return false;
        
        if(entity1.relationshipType != entity2.relationshipType)
            return false;

        if(entity1.gender != entity2.gender)
            return false;
        
        if(entity1.name.compareTo(entity2.name) != 0)
            return false;

        if(entity1.dateCreatedString.compareTo(entity2.dateCreatedString) != 0)
            return false;

        if(entity1.photoLocation.compareTo(entity2.photoLocation) != 0)
            return false;

        if(entity1.note.compareTo(entity2.note) != 0)
            return false;
        
        if(entity1.partnerId != entity2.partnerId)
            return false;

        return true;
    }


    public static PartnerEntity createEntity(long partnerId,
                                             long userId,
                                             String name,
                                             int relationshipType,
                                             int gender,
                                             String dateCreatedString,
                                             String note,
                                             String photoLocation)
    {
        return (new PartnerEntity(partnerId,
        userId,
        name,
        relationshipType,
        gender,
        dateCreatedString,
        note,
        photoLocation));
    }

    public static PartnerEntity createBasicEntity(long userId)
    {
        long partnerId = 1;
        String name = "name";
        int relationshipType = 0;
        int gender = 0;
        String dateCreatedString = "";
        String note = "note";
        String photoLocation = "photoLocation";

        return (new PartnerEntity(partnerId,
                userId,
                name,
                relationshipType,
                gender,
                dateCreatedString,
                note,
                photoLocation));
    }
}
