package centertable.advancedscalendar.RoomTest.UserRoom;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.UserEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Rutkay on 29.01.2018.
 */


@RunWith(AndroidJUnit4.class)
public class UserRoomTest {
    private UserDao mUserDao;
    private ASCDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, ASCDatabase.class).build();
        mUserDao = mDb.getUserDao();
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void createNewUserAndReadInList() throws Exception {
        mUserDao.clearAll();

        assertEquals(mUserDao.getAll().size(), 0);

        UserEntity user = UserRoomTestUtil.createBasicEntity();
        mUserDao.insert(user);

        assertEquals(mUserDao.getAll().size(), 1);

        UserEntity firstUser = mUserDao.getAll().get(0);

        assertEquals(UserRoomTestUtil.isEqual(firstUser,user),true);

    }

    @Test
    public void createNewUserAndModifyThatUser() throws Exception {
        String newPwd = "email2";

        mUserDao.clearAll();

        assertEquals(mUserDao.getAll().size(), 0);

        UserEntity user = UserRoomTestUtil.createBasicEntity();
        mUserDao.insert(user);

        assertEquals(mUserDao.getAll().size(), 1);

        UserEntity firstUser = mUserDao.getAll().get(0);

        assertEquals(firstUser.email, user.email);

        firstUser.email = newPwd;

        mUserDao.update(firstUser);

        assertEquals(mUserDao.getAll().size(), 1);

        UserEntity updatedUser = mUserDao.getAll().get(0);

        assertEquals(updatedUser.userId, firstUser.userId);
        assertEquals(updatedUser.email, newPwd);
    }


    @Test
    public void createNewUserAndInsertUserWithSameId() throws Exception {
        String newPwd = "email2";

        mUserDao.clearAll();

        assertEquals(mUserDao.getAll().size(), 0);

        UserEntity user = UserRoomTestUtil.createBasicEntity();
        mUserDao.insert(user);

        assertEquals(mUserDao.getAll().size(), 1);

        UserEntity firstUser = mUserDao.getAll().get(0);

        assertEquals(firstUser.email, user.email);

        firstUser.email = newPwd;

        long[] res = mUserDao.insert(firstUser);
        assertEquals(res[0],1);

        assertEquals(mUserDao.getAll().size(), 1);

        UserEntity updatedUser = mUserDao.getAll().get(0);

        assertEquals(updatedUser.userId, user.userId);
        assertEquals(updatedUser.email, newPwd);
    }

    @Test
    public void createTwoNewUsersAndGetOneOfThemById() throws Exception {
        mUserDao.clearAll();

        assertEquals(mUserDao.getAll().size(), 0);

        UserEntity user = UserRoomTestUtil.createBasicEntity();
        mUserDao.insert(user);

        UserEntity user2 = UserRoomTestUtil.createBasicEntity();
        user2.userId = 999;
        mUserDao.insert(user2);

        assertEquals(mUserDao.getAll().size(), 2);

        UserEntity myUser = mUserDao.getUserById(999);

        assertEquals(UserRoomTestUtil.isEqual(myUser, user2), true);

    }
}
