package centertable.advancedscalendar.RoomTest.UserRoom;

import centertable.advancedscalendar.data.room.entity.UserEntity;

/**
 * Created by Rutkay on 29.01.2018.
 */

public class UserRoomTestUtil {
    public static boolean isEqual(UserEntity userEntity1, UserEntity userEntity2)
    {
        if(userEntity1.userId != userEntity2.userId)
            return false;

        if(userEntity1.name.compareTo(userEntity2.name) != 0)
            return false;

        if(userEntity1.email.compareTo(userEntity2.email) != 0)
            return false;

        return true;
    }


    public static UserEntity createEntity(long userId, String nickName, String email, int gender)
    {
        return (new UserEntity(userId, "",nickName,email, gender));
    }

    public static UserEntity createBasicEntity()
    {
        long userId =1;
        String nickName = "name";
        String email = "";

        return (new UserEntity(userId, "", nickName,email, 0));
    }
}
