package centertable.advancedscalendar.modules.shop;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.view.custom.CustomShoppingCart;
import centertable.advancedscalendar.data.remote.billing.InAppPurchase;
import centertable.advancedscalendar.common.indication.SnackBarBuilder;
import centertable.advancedscalendar.common.shop.ShoppingCartItem;
import centertable.advancedscalendar.data.remote.authorization.AuthorizationDatabase;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.util.view.ActionBarUtil;

public class ShopMainFragment extends Fragment {
    public static final String TAG = "shop_main_fragment";

    private View view;
    private Unbinder unbinder;

    private InAppPurchase inAppPurchase;

    private Map<String,CustomShoppingCart> productIdCartMap;

    public ShopMainFragment() {}

    public static ShopMainFragment newInstance() {
        ShopMainFragment fragment = new ShopMainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shop, container, false);
        unbinder = ButterKnife.bind(this, view);

        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.shop));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
    }

    private void initialise(){
        productIdCartMap = new HashMap<>();

        inAppPurchase = AuthenticationInfo.getInstance().getAuthorizationInfo().getPurchaseMethod();
        initialiseBilling();
    }

    private void initialiseBilling(){
        inAppPurchase.startConnection(getActivity(), new InAppPurchase.BillingClientStateListener() {
                    @Override
                    public void onBillingSetupFinished(int responseCode) {
                        if (responseCode == InAppPurchase.ResponseCode.OK) {
                            prepareProducts();
                        } else {
                            MainActivity.analyticsService.logError("Billing setup finished with error: " + String.valueOf(responseCode));
                            if (responseCode == InAppPurchase.ResponseCode.SERVICE_UNAVAILABLE){
                                Toast.makeText(getContext(), getString(R.string.in_app_purchase_method_not_supported_by_your_device), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getContext(), getString(R.string.unknown_error_try_later), Toast.LENGTH_LONG).show();
                            }
                            // TODO: if not OK, handle the situation.
                        }
                    }

                    @Override
                    public void onBillingServiceDisconnected() {
                        MainActivity.analyticsService.logError("Billing service disconnected");
                        Toast.makeText(getContext(), getString(R.string.unknown_error_try_later), Toast.LENGTH_SHORT).show();
                        // TODO: put a refresh button somewhere
                    }
                },
                new InAppPurchase.BillingClientPurchaseUpdateListener() {
                    @Override
                    public void onPurchasesUpdated(int responseCode, @Nullable List<InAppPurchase.Purchase> purchases) {
                        if(responseCode == BillingClient.BillingResponse.OK && purchases != null) {
                            consumePurchases(purchases);
                        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
                            MainActivity.analyticsService.logWarning("Purchase cancelled by user");
                            Log.i(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
                        } else {
                            MainActivity.analyticsService.logError("Purchase is not completed, error: " + String.valueOf(responseCode));
                            Log.w(TAG, "onPurchasesUpdated() got unknown responseCode: " + responseCode);
                        }
                    }
                });
    }

    private void consumePurchases(List<InAppPurchase.Purchase> purchases){
        for(InAppPurchase.Purchase purchase : purchases) {
            inAppPurchase.consumeProduct(purchase, new InAppPurchase.ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(int responseCode, String token) {
                    if(responseCode == InAppPurchase.ResponseCode.OK){
                        MainActivity.analyticsService.logInfo("Purchase consumed: " + purchase.getProductId());
                        applyPurchase(purchase);
                    }
                }
            });
        }
    }

    private void applyPurchase(InAppPurchase.Purchase purchase){
        AuthorizationDatabase.ProductPurchaseDetail detail = new AuthorizationDatabase.ProductPurchaseDetail();
        detail.setOrderId(purchase.getOrderId());
        detail.setProductId(purchase.getProductId());
        detail.setPurchased(true);

        refreshAuthorizationStatusForProduct(detail);
        setCartPurchasedStatus(productIdCartMap.get(purchase.getProductId()), true);
    }

    private void prepareProducts(){
        ArrayList<String> productIds = new ArrayList<> ();

        productIds.add(InAppPurchase.Product.LOCK_FEATURE);
        productIds.add(InAppPurchase.Product.LIMITLESS_PARTNER);
        productIds.add(InAppPurchase.Product.REMOVE_ADS);

        inAppPurchase.getProductList(new InAppPurchase.ProductDetailsResponseListener() {
            @Override
            public void onProductDetailsResponse(int responseCode, List<InAppPurchase.ProductDetails> productDetailsList) {
                if(responseCode == InAppPurchase.ResponseCode.OK && productDetailsList != null){
                    createShoppingCarts(productDetailsList);
                } else {
                    // TODO: if not OK, handle the situation.
                }

                if(productDetailsList == null || productDetailsList.isEmpty()){
                    MainActivity.analyticsService.logError("Products cannot be prepared");

                    SnackBarBuilder builder = new SnackBarBuilder(view.findViewById(R.id.shop_root));
                    builder.setMessage(getString(R.string.unknown_error_try_later));
                    builder.setDuration(SnackBarBuilder.INDEFINITE);
                    builder.show();
                }
            }
        }, productIds);
    }

    private void createShoppingCarts(List<InAppPurchase.ProductDetails> productDetailsList){
        LinearLayout cartContainer = view.findViewById(R.id.shopping_cart_container);

        cartContainer.removeAllViews();

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        int smallMargin = (int)getResources().getDimension(R.dimen.common_small_margin_views);
        params.setMargins(smallMargin,smallMargin,smallMargin,smallMargin);

        for(InAppPurchase.ProductDetails productDetails : productDetailsList){
            CustomShoppingCart cart = new CustomShoppingCart(getContext());
            cart.setLayoutParams(params);
            cart.setCardElevation(getResources().getDimension(R.dimen.common_elevation_card));

            ShoppingCartItem item = new ShoppingCartItem();
            item.setProductId(productDetails.getProductId());

            String itemTitle = "", itemDescription = "";
            switch(productDetails.getProductId()){
                case InAppPurchase.Product.REMOVE_ADS:
                    itemTitle = getString(R.string.shop_remove_ads);
                    itemDescription = getString(R.string.shop_remove_ads_description);
                    break;
                case InAppPurchase.Product.LIMITLESS_PARTNER:
                    itemTitle = getString(R.string.shop_limitless_partner);
                    itemDescription = getString(R.string.shop_limitless_partner_description);
                    break;
                case InAppPurchase.Product.LOCK_FEATURE:
                    itemTitle = getString(R.string.shop_lock_feature);
                    itemDescription = getString(R.string.shop_lock_feature_description);
                    break;
            }

            if(!itemTitle.isEmpty())
                item.setTitle(itemTitle);

            if(!itemDescription.isEmpty())
                item.setDescription(itemDescription);

            item.setPrice(productDetails.getPrice());

            cart.setCartItem(item);
            cart.setOnClickListener(view -> shoppingCartClicked(cart.getCartItem()));

            cartContainer.addView(cart);

            productIdCartMap.put(item.getProductId(), cart);

            setCartPurchasedStatus(cart,
                    AuthenticationInfo.getInstance().getAuthorizationInfo().getAuthorizationStatusForProduct(item.getProductId()));
        }
    }

    private void shoppingCartClicked(ShoppingCartItem item){
        int res = inAppPurchase.launchBillingFlow(getActivity(), item.getProductId());
        if(res != InAppPurchase.ResponseCode.OK) {
            MainActivity.analyticsService.logError("Billing flow cannot be launched, error: " + String.valueOf(res));
            Toast.makeText(getContext(), getString(R.string.unknown_error_try_later), Toast.LENGTH_SHORT).show();
        }
        //TODO: handle result for error cases
    }

    private void refreshAuthorizationStatusForProduct(AuthorizationDatabase.ProductPurchaseDetail productPurchaseDetail){
        AuthenticationInfo.getInstance().getAuthorizationInfo().setAuthorizationStatusForProduct(productPurchaseDetail);
    }

    private void setCartPurchasedStatus(CustomShoppingCart cart, boolean purchased){
        cart.setClickable(!purchased);
        if(purchased)
            cart.setImage(getResources().getDrawable(R.drawable.ic_done_black_24dp));
        else
            cart.setImage(getResources().getDrawable(R.drawable.ic_add_shopping_cart_black_24dp));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
