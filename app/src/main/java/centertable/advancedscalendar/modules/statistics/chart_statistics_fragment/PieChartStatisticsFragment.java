package centertable.advancedscalendar.modules.statistics.chart_statistics_fragment;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.util.chart.ChartSpecification;
import centertable.advancedscalendar.util.chart.value_formatter.ValueFormatter;


public class PieChartStatisticsFragment extends Fragment {
    public static final String TAG = "pie_chart";

    private static final String PIE_ENTRY_TAG = "pie_entry";
    private static final String VALUE_FORMATTER_TAG = "value_formatter";

    private View view;
    private ArrayList<PieEntry> pieEntries;
    private PieChart pieChart;
    private ChartSpecification chartSpecification;

    public PieChartStatisticsFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance(ArrayList<PieEntry> entryList, ChartSpecification formatter){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(PIE_ENTRY_TAG, entryList);
        bundle.putParcelable(VALUE_FORMATTER_TAG, formatter);

        PieChartStatisticsFragment fragment = new PieChartStatisticsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_graph_statistics, container, false);

        Bundle bundle = null;
        if(savedInstanceState != null) {
            bundle = savedInstanceState;
        } else if(getArguments()!=null) {
            bundle = getArguments();
        }

        if(bundle != null) {
            pieEntries = bundle.getParcelableArrayList(PIE_ENTRY_TAG);
            chartSpecification = bundle.getParcelable(VALUE_FORMATTER_TAG);
        }else{
            pieEntries = new ArrayList<>();
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(PIE_ENTRY_TAG, pieEntries);
        outState.putParcelable(VALUE_FORMATTER_TAG, chartSpecification);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            pieEntries = savedInstanceState.getParcelableArrayList(PIE_ENTRY_TAG);
            chartSpecification = savedInstanceState.getParcelable(VALUE_FORMATTER_TAG);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        generateGraph();
        fillGraph(pieEntries);
    }


    private void generateGraph(){
        // programmatically create a PieChart
        pieChart = new PieChart(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        pieChart.setLayoutParams(layoutParams);

        pieChart.getDescription().setEnabled(false);

        // get a layout defined in xml
        RelativeLayout graphContainer = view.findViewById(R.id.graph_container);
        graphContainer.addView(pieChart);
    }

    private void fillGraph(List<PieEntry> entryList){
        PieDataSet dataSet = new PieDataSet(entryList, "");
        dataSet.setColors(ColorTemplate.PASTEL_COLORS);

        // set pie data set(s) to pieData object
        PieData pieData = new PieData(dataSet);

        if(chartSpecification != null) {
            String description = chartSpecification.legendDestription;
            if(description != null && !description.isEmpty()){
                Description chartDescription = new Description();
                chartDescription.setTextAlign(Paint.Align.RIGHT);
                chartDescription.setText(description);

                pieChart.setDescription(chartDescription);
            }

            if (chartSpecification.getTopFormatter() != null)
                pieData.setValueFormatter(chartSpecification.getTopFormatter());
        }

        pieChart.setData(pieData);

        pieChart.invalidate(); // refresh
    }

}
