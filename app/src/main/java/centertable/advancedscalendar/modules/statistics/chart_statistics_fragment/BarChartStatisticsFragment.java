package centertable.advancedscalendar.modules.statistics.chart_statistics_fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.util.chart.ChartSpecification;
import centertable.advancedscalendar.util.chart.value_formatter.DayAxisValueFormatter;
import centertable.advancedscalendar.util.chart.value_formatter.IntegerAxisValueFormatter;

public class BarChartStatisticsFragment extends Fragment {
    public static final String TAG = "bar_chart";

    private static final String BAR_ENTRY_TAG = "bar_entry";
    private static final String VALUE_FORMATTER_TAG = "value_formatter";

    private View view;
    private ArrayList<BarEntry> barEntries;
    private BarChart barChart;
    private ChartSpecification chartSpecification;

    public BarChartStatisticsFragment() {}

    public static Fragment newInstance(ArrayList<BarEntry> entryList, ChartSpecification formatter){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BAR_ENTRY_TAG, entryList);
        bundle.putParcelable(VALUE_FORMATTER_TAG, formatter);

        BarChartStatisticsFragment fragment = new BarChartStatisticsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_graph_statistics, container, false);

        Bundle bundle = null;
        if(savedInstanceState != null) {
            bundle = savedInstanceState;
        } else if(getArguments()!=null) {
            bundle = getArguments();
        }

        if(bundle != null) {
            barEntries = bundle.getParcelableArrayList(BAR_ENTRY_TAG);
            chartSpecification = bundle.getParcelable(VALUE_FORMATTER_TAG);
        }else{
            barEntries = new ArrayList<>();
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(BAR_ENTRY_TAG, barEntries);
        outState.putParcelable(VALUE_FORMATTER_TAG, chartSpecification);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            barEntries = savedInstanceState.getParcelableArrayList(BAR_ENTRY_TAG);
            chartSpecification = savedInstanceState.getParcelable(VALUE_FORMATTER_TAG);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        generateGraph();
        fillGraph(barEntries);
    }

    private void generateGraph(){
        // programmatically create a BarChart
        barChart = new BarChart(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        barChart.setLayoutParams(layoutParams);

        barChart.getDescription().setEnabled(false);

        // get a layout defined in xml
        RelativeLayout graphContainer = view.findViewById(R.id.graph_container);
        graphContainer.addView(barChart);
    }

    private void fillGraph(List<BarEntry> entryList){
        Collections.sort(entryList, new EntryXComparator());
        BarDataSet dataSet = new BarDataSet(entryList, "");
        dataSet.setColors(ColorTemplate.PASTEL_COLORS);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setValueFormatter(new IntegerAxisValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularity(1f);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setEnabled(false);

        // set bar data set(s) to barData object
        BarData barData = new BarData(dataSet);

        if(entryList.size() >= 2) {
            float diff = (entryList.get(1).getX() - entryList.get(0).getX()) / 2;
            barData.setBarWidth(diff);
        } else if (entryList.size() == 1)
            barData.setBarWidth((entryList.get(0).getX()) * 0.75f);

        barChart.setData(barData);

        if(chartSpecification != null) {
            if (chartSpecification.getXFormatter() != null)
                barChart.getXAxis().setValueFormatter(chartSpecification.getXFormatter());

            if (chartSpecification.getYLeftFormatter() != null)
                barChart.getAxisLeft().setValueFormatter(chartSpecification.getYLeftFormatter());

            if (chartSpecification.getYRightFormatter() != null)
                barChart.getAxisRight().setValueFormatter(chartSpecification.getYRightFormatter());

            if (chartSpecification.getTopFormatter() != null)
                barData.setValueFormatter(chartSpecification.getTopFormatter());

            dataSet.setLabel(chartSpecification.legendDestription);

        }

        barChart.invalidate(); // refresh
    }

}
