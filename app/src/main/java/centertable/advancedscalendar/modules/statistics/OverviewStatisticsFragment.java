package centertable.advancedscalendar.modules.statistics;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.util.data.statistics.SimpleEntryStatistics;

public class OverviewStatisticsFragment extends Fragment {
    public static final String TAG = "overview_statistics";

    private View view;
    private Unbinder unbinder;

    @BindView(R.id.total_entry_count) TextView totalEntryCount;
    @BindView(R.id.average_rating) TextView averageRating;
    @BindView(R.id.average_orgasms) TextView averageOrgasms;
    @BindView(R.id.average_duration) TextView averageDuration;

    private SimpleEntryStatistics simpleEntryStatistics;

    public static Fragment newInstance(SimpleEntryStatistics simpleEntryStatistics){
        Bundle bundle = new Bundle();
        bundle.putParcelable(SimpleEntryStatistics.TAG_STATISTICS_PARCELABLE, simpleEntryStatistics);

        OverviewStatisticsFragment fragment = new OverviewStatisticsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    public OverviewStatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_overview_statistics, container, false);

        unbinder = ButterKnife.bind(this, view);

        if(getArguments() != null)
            simpleEntryStatistics = getArguments().getParcelable(SimpleEntryStatistics.TAG_STATISTICS_PARCELABLE);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        fillStatistics(simpleEntryStatistics);
    }

    private void fillStatistics(SimpleEntryStatistics statistics){
        DecimalFormat decimalFormat = new DecimalFormat(".#");
        
        String totalEntryCountStr = "0";
        String averageRatingStr = "0";
        String averageOrgasmsStr = "0";
        String averageDurationStr = "0";
        
        if(simpleEntryStatistics != null){
            totalEntryCountStr = String.valueOf(statistics.getTotalEntryCount());

            if(statistics.getAverageRating() != 0)
                averageRatingStr = String.valueOf(decimalFormat.format(statistics.getAverageRating()));

            if(statistics.getAverageOrgasmCount() != 0)
                averageOrgasmsStr = String.valueOf(decimalFormat.format(statistics.getAverageOrgasmCount()));

            if(statistics.getAverageDurationInMinutes() != 0)
                averageDurationStr = String.valueOf(decimalFormat.format(statistics.getAverageDurationInMinutes()));
        }
        
        totalEntryCount.setText(totalEntryCountStr);
        averageRating.setText(averageRatingStr);
        averageOrgasms.setText(averageOrgasmsStr);
        averageDuration.setText(averageDurationStr);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
