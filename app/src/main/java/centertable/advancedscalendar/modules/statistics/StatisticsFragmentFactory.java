package centertable.advancedscalendar.modules.statistics;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.data.definitions.EntryDefs;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.modules.statistics.chart_statistics_fragment.BarChartStatisticsFragment;
import centertable.advancedscalendar.modules.statistics.chart_statistics_fragment.PieChartStatisticsFragment;
import centertable.advancedscalendar.util.chart.ChartSpecification;
import centertable.advancedscalendar.util.chart.value_formatter.DayAxisValueFormatter;
import centertable.advancedscalendar.util.chart.value_formatter.IntegerAxisValueFormatter;
import centertable.advancedscalendar.util.chart.value_formatter.IntegerTopValueFormatter;
import centertable.advancedscalendar.util.data.statistics.ChartUtil;
import centertable.advancedscalendar.util.data.statistics.SimpleEntryStatistics;
import centertable.advancedscalendar.util.data.statistics.StatisticsUtil;

class StatisticsFragmentFactory {
    private static DayAxisValueFormatter dayAxisValueFormatter = new DayAxisValueFormatter();
    private static IntegerAxisValueFormatter integerAxisValueFormatter = new IntegerAxisValueFormatter();
    private static IntegerTopValueFormatter integerTopValueFormatter = new IntegerTopValueFormatter();

    private Context context;
    StatisticsFragmentFactory(Context context){
        this.context = context;
    }

    Fragment getEntryCountStatistics(ArrayList<Entry> entryList, Date dateStart, Date dateEnd) {
        ArrayList<BarEntry> barEntries = ChartUtil.getBarChartEntryList(
                StatisticsUtil.getNumberOfEntriesByDateMap(entryList, dateStart, dateEnd)
        );

        ChartSpecification chartSpecification =
                new ChartSpecification(
                    dayAxisValueFormatter,
                    integerAxisValueFormatter,
                    integerAxisValueFormatter,
                    integerTopValueFormatter
                );

        chartSpecification.legendDestription = context.getString(R.string.sexual_activities_chart_description);

        Fragment barFragment = BarChartStatisticsFragment.newInstance(barEntries,
                chartSpecification);

        return barFragment;
    }

    Fragment getOrgasmCountStatistics(ArrayList<Entry> entryList, Date dateStart, Date dateEnd) {
        ArrayList<BarEntry> barEntries = ChartUtil.getBarChartEntryList(
                StatisticsUtil.getNumberOfOrgasmsByDateMap(entryList, dateStart, dateEnd)
        );

        ChartSpecification chartSpecification =
                new ChartSpecification(
                        dayAxisValueFormatter,
                        integerAxisValueFormatter,
                        integerAxisValueFormatter,
                        integerTopValueFormatter
                );

        chartSpecification.legendDestription = context.getString(R.string.number_of_orgasms_chart_description);

        Fragment barFragment = BarChartStatisticsFragment.newInstance(barEntries,
                chartSpecification);

        return barFragment;
    }

    Fragment getTotalDurationStatistics(ArrayList<Entry> entryList, Date dateStart, Date dateEnd) {
        ArrayList<BarEntry> barEntries = ChartUtil.getBarChartEntryList(
                StatisticsUtil.getTotalDurationByDateMap(entryList, dateStart, dateEnd)
        );

        ChartSpecification chartSpecification =
                new ChartSpecification(
                        dayAxisValueFormatter,
                        integerAxisValueFormatter,
                        integerAxisValueFormatter,
                        integerTopValueFormatter
                );

        chartSpecification.legendDestription = context.getString(R.string.total_durations_chart_description);

        Fragment barFragment = BarChartStatisticsFragment.newInstance(barEntries,
                chartSpecification);

        return barFragment;
    }

    Fragment getOverviewStatistics(ArrayList<Entry> entryList) {
        SimpleEntryStatistics simpleEntryStatistics = new SimpleEntryStatistics();
        simpleEntryStatistics.calculateStatisticsForEntryList(entryList);

        Fragment overviewFragment = OverviewStatisticsFragment.newInstance(simpleEntryStatistics);

        return overviewFragment;
    }

    Fragment getPercentagesOfSexTypes(ArrayList<Entry> entryList, Date dateStart, Date dateEnd) {
        ArrayList<PieEntry> pieEntries = new ArrayList<>();

        for (Map.Entry<EntryDefs.SEX_TYPE, Float> entry : StatisticsUtil.getTotalNumbersBySexTypeMap(entryList, dateStart, dateEnd).entrySet()) {
            pieEntries.add(new PieEntry(entry.getValue(), entry.getKey().toString(context)));
        }

        ChartSpecification chartSpecification = new ChartSpecification();
        chartSpecification.legendDestription = context.getString(R.string.sex_types_percentages_chart_description);
        chartSpecification.setTopFormatter(integerTopValueFormatter);

        Fragment pieFragment = PieChartStatisticsFragment.newInstance(pieEntries, chartSpecification);

        return pieFragment;
    }
}
