package centertable.advancedscalendar.modules.statistics;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.fragment.exception.CannotGotoException;
import centertable.advancedscalendar.common.indication.SnackBarBuilder;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import centertable.advancedscalendar.util.time.TimeUtil;
import centertable.advancedscalendar.util.view.ActionBarUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticsMainFragment extends Fragment implements FragmentActionGenerator {
    public static final String TAG = "statistics_main";
    private View view;
    private Unbinder unbinder;
    private ChartPagerAdapter chartPagerAdapter;
    private int lastTab = 0;
    private Date dateStart = null, dateEnd = null;
    FragmentActionListener listener;

    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.date_interval) Spinner dateInterval;
    @BindView(R.id.ad_container) FrameLayout bannerAdContainer;

    @BindString(R.string.date_interval_all_time) String dateIntervalAllTime;

    public StatisticsMainFragment() {
        // Required empty public constructor
    }

    public static StatisticsMainFragment newInstance() {
        StatisticsMainFragment fragment = new StatisticsMainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_statistics, container, false);
        unbinder = ButterKnife.bind(this, view);

        MainActivity.analyticsService.logInfo(TAG);

        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.statistics));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
    }

    private void initialise(){
        chartPagerAdapter = new ChartPagerAdapter(getChildFragmentManager());

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tabLayout.getSelectedTabPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        initialiseDateIntervalPicker();

        MainActivity.adFactory.bindBannerAd(getContext(), bannerAdContainer);
    }

    private void initialiseDateIntervalPicker(){
        String dateIntervalLastXDays = getResources().getString(R.string.date_interval_last_x_days);

        ArrayList<String> dateIntervalAdapter = new ArrayList<>();
        dateIntervalAdapter.add(DateInterval.indexOf(DateInterval.LAST_7),
                String.format(dateIntervalLastXDays, DateInterval.LAST_7.getValue()));

        dateIntervalAdapter.add(DateInterval.indexOf(DateInterval.LAST_30),
                String.format(dateIntervalLastXDays, DateInterval.LAST_30.getValue()));

        dateIntervalAdapter.add(DateInterval.indexOf(DateInterval.ALL_TIME),
                dateIntervalAllTime);

        ArrayAdapter adapterDateInterval = new ArrayAdapter<>(
                getContext(), R.layout.spinner_item_simple, dateIntervalAdapter);
        adapterDateInterval.setDropDownViewResource( R.layout.spinner_item_simple_dropdown);

        dateInterval.setAdapter(adapterDateInterval);

        dateInterval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                lastTab = tabLayout.getSelectedTabPosition();

                Date now = Calendar.getInstance().getTime();

                dateStart = TimeUtil.getAnotherDateFromParticularDate(now,
                        -1 * DateInterval.valueOf(i));

                dateEnd = now;

                refreshStatistics();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void refreshStatistics(){
        chartPagerAdapter.clearFragments();
        chartPagerAdapter.notifyDataSetChanged();

        if(dateStart.compareTo(dateEnd) == 0)
            bindChartFragmentsForAllTime();
        else
            bindChartFragmentsForDateInterval(dateStart, dateEnd);
    }

    private void bindChartFragmentsForAllTime(){
        RoomDataAccessUtil.getAllEntries(new Observer<List<Entry>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Entry> entries) {
                //Update start and end dates according to incoming entry list
                if (!entries.isEmpty() && dateStart.compareTo(dateEnd) == 0) {
                    dateStart = TimeUtil.getDateFromDbDate(entries.get(0).getDateString());
                    dateEnd = TimeUtil.getDateFromDbDate(entries.get(entries.size() - 1).getDateString());
                }

                bindStatistics(new ArrayList<Entry>(entries));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void bindChartFragmentsForDateInterval(Date dateStart, Date dateEnd){
        RoomDataAccessUtil.getEntriesForTimeInterval(new Observer<List<Entry>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Entry> entries) {
                bindStatistics(new ArrayList<Entry>(entries));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        }, dateStart, dateEnd);
    }

    private void bindStatistics(ArrayList<Entry> entries){
        if(entries == null || entries.isEmpty()){
            SnackBarBuilder builder = new SnackBarBuilder(view.findViewById(R.id.snackbar_container));
            builder.setMessage(getString(R.string.cannot_find_statistics_data));
            builder.setDuration(SnackBarBuilder.INDEFINITE);
            builder.setAction(getString(R.string.goto_calendar), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        try{
                            listener.onGoto(FragmentActionListener.GOTO.CALENDAR);
                        } catch (CannotGotoException ex) {
                            Log.e(TAG, ex.toString());
                        }
                    }
                }
            });
            builder.show();
        }

        ArrayList<Fragment> graphFragmentList = generateGraphFragments(entries);
        chartPagerAdapter.setFragments(graphFragmentList);

        viewPager.setAdapter(chartPagerAdapter);
        viewPager.setCurrentItem(lastTab, true);
    }

    private ArrayList<Fragment> generateGraphFragments(ArrayList<Entry> entryList){
        tabLayout.removeAllTabs();

        ArrayList<Fragment> arrayList = new ArrayList();

        StatisticsFragmentFactory statisticsFragmentFactory = new StatisticsFragmentFactory(getContext());

        arrayList.add(statisticsFragmentFactory.getOverviewStatistics(entryList));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.overview));

        arrayList.add(statisticsFragmentFactory.getPercentagesOfSexTypes(entryList, dateStart, dateEnd));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.sex_types_percentages));

        arrayList.add(statisticsFragmentFactory.getEntryCountStatistics(entryList, dateStart, dateEnd));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.sexual_activities));

        arrayList.add(statisticsFragmentFactory.getOrgasmCountStatistics(entryList, dateStart, dateEnd));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.number_of_orgasms));

        arrayList.add(statisticsFragmentFactory.getTotalDurationStatistics(entryList, dateStart, dateEnd));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.total_durations));

        return arrayList;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this);
    }

    private class ChartPagerAdapter extends FragmentPagerAdapter{
        private ArrayList<Fragment> fragmentList;

        ChartPagerAdapter(FragmentManager fm){
            super(fm);
            fragmentList = new ArrayList<>();
        }

        void setFragments(ArrayList<? extends Fragment> fragments){
            fragmentList.addAll(fragments);
        }

        void clearFragments(){
            for(Fragment fragment : fragmentList)
                getChildFragmentManager().beginTransaction().remove(fragment).commit();

            fragmentList.clear();
        }

        @Override
        public Fragment getItem(int i) {
            return fragmentList.get(i);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

    }

    private enum DateInterval{
        LAST_7(7), LAST_30(30), ALL_TIME(0);

        private static ArrayList<DateInterval> list = new ArrayList();
        static {
            list.addAll(Arrays.asList(DateInterval.values()));
        }

        public static int indexOf(DateInterval interval) {
            int index = 0;

            try{
                index = list.indexOf(interval);
            }catch (Exception ex){
                Log.e(StatisticsMainFragment.TAG,
                        "cannot find the index of given DateInterval: " + interval.getValue());
            }

            return index;
        }

        public static int valueOf(int index) {
            int value = 0;
            try{
                value = list.get(index).getValue();
            }catch (Exception ex){
                Log.e(StatisticsMainFragment.TAG,
                        "cannot find the value of DateInterval with index: " + String.valueOf(index));

            }

            return value;
        }

        private int value = 0;
        DateInterval(int value){
            this.value = value;
        }

        public int getValue(){return this.value;}
    }

    @Override
    public void setFragmentActionListener(FragmentActionListener listener) {
        this.listener = listener;
    }
}
