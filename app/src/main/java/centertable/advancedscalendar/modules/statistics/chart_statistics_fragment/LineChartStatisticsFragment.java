package centertable.advancedscalendar.modules.statistics.chart_statistics_fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.util.chart.ChartSpecification;
import centertable.advancedscalendar.util.chart.value_formatter.IntegerAxisValueFormatter;

public class LineChartStatisticsFragment extends Fragment {
    public static final String TAG = "line_chart";

    private static final String LINE_ENTRY_TAG = "line_entry";
    private static final String VALUE_FORMATTER_TAG = "value_formatter";

    private View view;
    private ArrayList<Entry> lineEntries;
    private LineChart lineChart;
    private ChartSpecification chartSpecification;

    public LineChartStatisticsFragment() {}

    public static Fragment newInstance(ArrayList<Entry> entryList, ChartSpecification formatter){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(LINE_ENTRY_TAG, entryList);
        bundle.putParcelable(VALUE_FORMATTER_TAG, formatter);

        LineChartStatisticsFragment fragment = new LineChartStatisticsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_graph_statistics, container, false);

        Bundle bundle = null;
        if(savedInstanceState != null) {
            bundle = savedInstanceState;
        } else if(getArguments()!=null) {
            bundle = getArguments();
        }

        if(bundle != null) {
            lineEntries = bundle.getParcelableArrayList(LINE_ENTRY_TAG);
            chartSpecification = bundle.getParcelable(VALUE_FORMATTER_TAG);
        }else{
            lineEntries = new ArrayList<>();
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(LINE_ENTRY_TAG, lineEntries);
        outState.putParcelable(VALUE_FORMATTER_TAG, chartSpecification);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            lineEntries = savedInstanceState.getParcelableArrayList(LINE_ENTRY_TAG);
            chartSpecification = savedInstanceState.getParcelable(VALUE_FORMATTER_TAG);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        generateGraph();
        fillGraph(lineEntries);
    }

    private void generateGraph(){
        // programmatically create a LineChart
        lineChart = new LineChart(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lineChart.setLayoutParams(layoutParams);

        lineChart.getDescription().setEnabled(false);

        // get a layout defined in xml
        RelativeLayout graphContainer = view.findViewById(R.id.graph_container);
        graphContainer.addView(lineChart);
    }

    private void fillGraph(List<Entry> entryList){
        Collections.sort(entryList, new EntryXComparator());
        LineDataSet dataSet = new LineDataSet(entryList, "DefaultLabel");
        dataSet.setColors(ColorTemplate.PASTEL_COLORS);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setValueFormatter(new IntegerAxisValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularity(1f);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setEnabled(false);

        // set line data set(s) to lineData object
        LineData lineData = new LineData(dataSet);
        lineChart.setData(lineData);

        if(chartSpecification != null) {
            if (chartSpecification.getXFormatter() != null)
                lineChart.getXAxis().setValueFormatter(chartSpecification.getXFormatter());

            if (chartSpecification.getYLeftFormatter() != null)
                lineChart.getAxisLeft().setValueFormatter(chartSpecification.getYLeftFormatter());

            if (chartSpecification.getYRightFormatter() != null)
                lineChart.getAxisRight().setValueFormatter(chartSpecification.getYRightFormatter());

            if (chartSpecification.getTopFormatter() != null)
                lineData.setValueFormatter(chartSpecification.getTopFormatter());

            //TODO: get axis and label descriptions from chartSpecification
        }

        lineChart.invalidate(); // refresh
    }

}
