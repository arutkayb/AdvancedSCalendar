package centertable.advancedscalendar.modules.partner;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.view.spinner.SpinnerAdapterFactory;
import centertable.advancedscalendar.common.indication.DialogFactory;
import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.common.android_component.view.app_bar.AppBarStateChangeListener;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import centertable.advancedscalendar.data.definitions.ProfileDefinitions;
import centertable.advancedscalendar.util.time.TimeUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class PartnerProfileFragment extends Fragment implements FragmentActionGenerator {
    public static final String TAG = "partner_profile";

    private View view;
    private FragmentActionListener listener;
    private Partner partner;
    private boolean isNewPartner;
    private String submitResultNegativeMessage = "";
    private String submitResultPositiveMessage = "";
    private String selectedPhotoLocation = "";

    @BindView(R.id.app_bar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.profile_photo) ImageView profilePhoto;
    @BindView(R.id.date_created) TextView dateCreated;
    @BindView(R.id.relationship_type) Spinner relationshipType;
    @BindView(R.id.gender) Spinner gender;
    @BindView(R.id.notes) EditText notes;
    @BindView(R.id.full_name) EditText fullName;

    @BindView(R.id.delete) Button deleteButton;
    @BindView(R.id.submit) Button submitButton;
    @BindView(R.id.cancel) Button cancelButton;

    @BindView(R.id.ad_container) FrameLayout bannerAdContainer;


    @BindString(R.string.data_input_partner_deleted) String partnerDeletedMessage;
    @BindString(R.string.data_input_submit_partner_added) String partnerAddedMessage;
    @BindString(R.string.data_input_submit_partner_updated) String partnerUpdatedMessage;
    @BindString(R.string.successful) String successfulString;
    @BindString(R.string.unsuccessful) String unsuccessfulString;
    
    public PartnerProfileFragment() {
        // Required empty public constructor
    }

    public static PartnerProfileFragment newInstance(Partner partner) {
        PartnerProfileFragment fragment = new PartnerProfileFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(Partner.PARTNER_PARCELABLE_TAG, partner);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle;
        if ((bundle = getArguments()) != null) {
            Partner partner = bundle.getParcelable(Partner.PARTNER_PARCELABLE_TAG);
            setPartner(partner);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_partner_profile, container, false);
        ButterKnife.bind(this, view);
        MainActivity.analyticsService.logInfo(TAG);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();

        initialise();
    }

    private void setPartner(Partner partner){
        if(partner == null)
            partner = new Partner();

        this.partner = partner;

        isNewPartner = this.partner.getPrimaryId() == 0;
    }

    private void initialise(){
        ArrayList<String> relationshipList;
        ArrayList<String> genderList;

        relationshipList = new ArrayList<String>(Arrays.asList(
                getContext().getResources().getStringArray(R.array.relationship_types)
        ));
        relationshipType.setAdapter(SpinnerAdapterFactory.getCommonSpinnerArrayAdapter(
                getContext(),
                relationshipList)
        );

        genderList = new ArrayList<String>(Arrays.asList(
                getContext().getResources().getStringArray(R.array.genders)
        ));
        gender.setAdapter(SpinnerAdapterFactory.getCommonSpinnerArrayAdapter(
                getContext(),
                genderList)
        );

        profilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPhotoFromGallery();
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                RelativeLayout rootView = view.findViewById(R.id.partner_profile_root);

                if(state == State.COLLAPSED) {
                    rootView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            appBarLayout.setExpanded(true);
                        }
                    });
                } else {
                    rootView.setOnClickListener(null);
                }
            }
        });

        initialiseButtons();

        fillFormByPartner();

        MainActivity.adFactory.bindBannerAd(getContext(), bannerAdContainer);
    }

    private void selectPhotoFromGallery(){
        //TODO: select photo here
    }

    private void initialiseButtons(){
        deleteButton.setEnabled(!isNewPartner);
        deleteButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                DialogFactory.getCustomAreYouSureDialog(getContext()
                                                        , getString(R.string.are_you_sure_delete_partner)
                                                        , new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                if (i == DialogInterface.BUTTON_POSITIVE) {
                                                                    deletePartner();
                                                                }
                                                            }
                                                        });
                                            }
                                        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null)
                    listener.onExit();
            }
        });


        if(isNewPartner){
            submitResultPositiveMessage = partnerAddedMessage + ": " + successfulString;
            submitResultNegativeMessage = partnerAddedMessage + ": " + unsuccessfulString;
        }else{
            submitResultPositiveMessage = partnerUpdatedMessage + ": " + successfulString;
            submitResultNegativeMessage = partnerUpdatedMessage + ": " + unsuccessfulString;
        }
        
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    private void deletePartner(){
        MainActivity.analyticsService.logInfo("Partner deleted");

        RoomDataAccessUtil.deletePartner(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Boolean result) {
                if (result) {
                    if (listener != null) {
                        listener.onUpdate();
                        listener.onExit();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        }, partner);
    }
    
    private void submitForm(){
        if (validateForm()) {
            ProfileDefinitions.RELATIONSHIP relationshipValue = ProfileDefinitions.RELATIONSHIP.fromValue(
                    (int)relationshipType.getSelectedItemId()
            );
            ProfileDefinitions.GENDER genderValue = ProfileDefinitions.GENDER.fromValue(
                    (int)gender.getSelectedItemId()
            );
            String createdDateValue = dateCreated.getText().toString();
            String nameValue = fullName.getText().toString();
            String noteValue = notes.getText().toString();

            partner.setRelationshipType(relationshipValue);
            partner.setDateCreatedString(createdDateValue);
            partner.setGender(genderValue);
            partner.setName(nameValue);
            partner.setNote(noteValue);

            String newPhotoLocation = savePartnerPhoto();
            partner.setPhotoLocation(newPhotoLocation);

            RoomDataAccessUtil.insertPartner(new Observer<Boolean>() {
                 @Override
                 public void onSubscribe(Disposable d) {

                 }

                 @Override
                 public void onNext(Boolean result) {
                    //TODO: if result negative, action
                     String toastMessage = "";
                     if (result) {
                         toastMessage = submitResultPositiveMessage;

                         if (listener != null)
                             listener.onUpdate();
                     } else
                         toastMessage = submitResultNegativeMessage;

                     Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT).show();

                     if (listener != null)
                         listener.onExit();
                 }

                 @Override
                 public void onError(Throwable e) {

                 }

                 @Override
                 public void onComplete() {

                 }
             }, partner);
        } else {
            Toast.makeText(getContext(), getString(R.string.cannot_submit_empty_form), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateForm(){
        // Validation of forms at least a name should be there
        boolean ret = false;

        if(!fullName.getText().toString().isEmpty())
            ret = true;

        return ret;
    }

    private void fillFormByPartner(){
        if(isNewPartner)
            dateCreated.setText(TimeUtil.getTodayAsDbDateString());
        else
            dateCreated.setText(partner.getDateCreatedString());

        relationshipType.setSelection(partner.getRelationshipType().getValue());
        gender.setSelection(partner.getGender().getValue());
        notes.setText(partner.getNote());
        fullName.setText(partner.getName());
        setPartnerPhoto();

        //TODO: set layout by using partner variable
    }

    private void setPartnerPhoto(){
        if(!partner.getPhotoLocation().isEmpty()) {
            Bitmap photo = BitmapFactory.decodeFile(partner.getPhotoLocation());

            if(photo != null) {
                profilePhoto.setImageBitmap(photo);
            }
        }
    }

    private String savePartnerPhoto(){
        //TODO: save the profile photo
        return "";
    }

    public void setFragmentActionListener(FragmentActionListener listener){
        this.listener = listener;
    }

}
