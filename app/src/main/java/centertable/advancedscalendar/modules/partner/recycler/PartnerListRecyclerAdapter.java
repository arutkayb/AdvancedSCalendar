package centertable.advancedscalendar.modules.partner.recycler;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.view.recycler_view.CommonRecyclerViewListener;
import centertable.advancedscalendar.data.pojo.Partner;

public class PartnerListRecyclerAdapter extends RecyclerView.Adapter<PartnerListRecyclerAdapter.PartnerListRecyclerHolder>{
    private final static String TAG = "PartnerListRecycler";

    private ArrayList<Partner> partnerList;
    private CommonRecyclerViewListener commonRecyclerViewListener;

    public PartnerListRecyclerAdapter(CommonRecyclerViewListener listener, ArrayList<Partner> partnerList){
        this.partnerList = partnerList;
        this.commonRecyclerViewListener = listener;
    }

    @NonNull
    @Override
    public PartnerListRecyclerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_item_partner, viewGroup, false);

        return new PartnerListRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PartnerListRecyclerHolder partnerListRecyclerHolder, int i) {
        partnerListRecyclerHolder.bindHolder(partnerList.get(i));
    }

    @Override
    public int getItemCount() {
        return partnerList == null ? 0 : partnerList.size();
    }

    class PartnerListRecyclerHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.full_name) TextView fullName;
        @BindView(R.id.profile_photo_mini) ImageView partnerProfilePhotoMini;

        PartnerListRecyclerHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(commonRecyclerViewListener != null)
                        commonRecyclerViewListener.onItemClick(getAdapterPosition());
                    else
                        Log.d(TAG, "commonRecyclerViewListener is null");
                }
            });
        }

        void bindHolder(Partner partner){
            fullName.setText(partner.getName());

            if(!partner.getPhotoLocation().isEmpty()) {
                Bitmap profilePhoto = BitmapFactory.decodeFile(partner.getPhotoLocation());

                if(profilePhoto != null)
                    partnerProfilePhotoMini.setImageBitmap(profilePhoto);
            }
        }
    }
}
