package centertable.advancedscalendar.modules.partner;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.fragment.exception.CannotGotoException;
import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.util.view.ActionBarUtil;

public class PartnerMainFragment extends Fragment implements FragmentActionGenerator {
    public static final String TAG = "partner_main_fragment";
    private static final String BACKSTACK = "partner_backstack";
    private View view;
    private Unbinder unbinder;
    private PartnerListFragment partnerListFragment;
    private PartnerProfileFragment partnerProfileFragment;

    private FragmentActionListener listener;

    public static PartnerMainFragment newInstance(){
        return new PartnerMainFragment();
    }

    public PartnerMainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_partner_main, container, false);
        unbinder = ButterKnife.bind(this, view);

        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.partners));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, partnerListFragment, PartnerListFragment.TAG)
                .commit();
    }

    private void initialise(){
        partnerListFragment = PartnerListFragment.newInstance();
        partnerListFragment.setPartnerListFragmentListener(new FragmentActionListener<Partner>() {
            @Override
            public void onAction(Partner partner) {
                openPartnerProfile(partner);
            }

            @Override
            public void onGoto(GOTO item) throws CannotGotoException {
                if(listener != null)
                    listener.onGoto(item);
            }
        });
    }

    private void openPartnerProfile(Partner partner){
        partnerProfileFragment = PartnerProfileFragment.newInstance(partner);
        partnerProfileFragment.setFragmentActionListener(new FragmentActionListener() {
            @Override
            public void onExit() {
                getChildFragmentManager().popBackStack(BACKSTACK, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            @Override
            public void onUpdate() {
                if(partnerListFragment != null) {
                    partnerListFragment.refreshPartnerList();

                    MainActivity.adFactory.loadFullScreenAd(getContext());
                    MainActivity.adFactory.showFullScreenAd();
                }
            }
        });

        getChildFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK)
                .replace(R.id.fragment_container, partnerProfileFragment, PartnerListFragment.TAG)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener = null;
        unbinder.unbind();
    }

    @Override
    public void setFragmentActionListener(FragmentActionListener listener) {
        this.listener = listener;
    }
}
