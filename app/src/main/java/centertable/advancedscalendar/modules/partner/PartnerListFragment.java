package centertable.advancedscalendar.modules.partner;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.fragment.exception.CannotGotoException;
import centertable.advancedscalendar.common.indication.DialogFactory;
import centertable.advancedscalendar.common.indication.SnackBarBuilder;
import centertable.advancedscalendar.common.android_component.view.recycler_view.CommonRecyclerViewListener;
import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.modules.partner.recycler.PartnerListRecyclerAdapter;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class PartnerListFragment extends Fragment {
    public static final String TAG = "partner_list_fragment";
    private View view;
    @BindView(R.id.recycler_view_partner) RecyclerView recyclerViewPartner;
    @BindView(R.id.fab_add_partner) FloatingActionButton fabAddPartner;
    @BindView(R.id.root_coordinator) CoordinatorLayout rootCoordinator;

    FragmentActionListener<Partner> listener;

    private int numberOfPartners = 0;

    public PartnerListFragment() {
        // Required empty public constructor
    }

    public static PartnerListFragment newInstance() {
        PartnerListFragment fragment = new PartnerListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_partner_list, container, false);
        ButterKnife.bind(this, view);

        MainActivity.analyticsService.logInfo(TAG);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
    }

    private void initialise(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewPartner.setLayoutManager(linearLayoutManager);

        refreshPartnerList();

        fabAddPartner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    if(AuthenticationInfo.getInstance().getAuthorizationInfo().isAuthorizedToAddPartner(numberOfPartners))
                        listener.onAction(new Partner());
                    else
                        notAuthorizedToAddPartner();
                }
            }
        });
    }

    private void notAuthorizedToAddPartner(){
        MainActivity.analyticsService.logInfo("Not authorized to add partner");

        DialogFactory.getNotAuthorizedToAddPartnerDialog(getContext(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int whichButton) {
                if(whichButton == DialogInterface.BUTTON_POSITIVE){
                    if(listener != null) {
                        try {
                            listener.onGoto(FragmentActionListener.GOTO.SHOP);
                        } catch (CannotGotoException ex) {
                            Log.e(TAG, ex.toString());
                        }
                    };
                }
            }
        });
    }

    void refreshPartnerList(){
        RoomDataAccessUtil.getAllPartners(new Observer<List<Partner>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Partner> partners) {
                fillRecyclerView(partners);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void fillRecyclerView(List<Partner> partners) {
        numberOfPartners = partners.size();

        if(numberOfPartners > 0) {
            PartnerListRecyclerAdapter partnerListRecyclerAdapter = new PartnerListRecyclerAdapter(new CommonRecyclerViewListener() {
                @Override
                public void onItemClick(int itemNo) {
                    if (listener != null)
                        listener.onAction(partners.get(itemNo));
                }
            }, (ArrayList<Partner>) partners);
            recyclerViewPartner.setAdapter(partnerListRecyclerAdapter);
        } else {
            noPartnerCreatedIndication();
        }
    }

    private void noPartnerCreatedIndication(){
        SnackBarBuilder builder = new SnackBarBuilder(rootCoordinator);
        builder.setMessage(getString(R.string.snack_no_partners_created))
                .setDuration(Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.add_partner), view -> { fabAddPartner.performClick();})
                .show();
    }

    public void setPartnerListFragmentListener(FragmentActionListener<Partner> listener){
        this.listener = listener;
    }

    @Override
    public void onDetach() {
        view = null;
        listener = null;
        super.onDetach();
    }
}
