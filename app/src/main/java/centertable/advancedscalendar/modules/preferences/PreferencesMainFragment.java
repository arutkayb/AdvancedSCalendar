package centertable.advancedscalendar.modules.preferences;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.fragment.exception.CannotGotoException;
import centertable.advancedscalendar.common.rules.AppRules;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.common.indication.DialogFactory;
import centertable.advancedscalendar.util.view.ActionBarUtil;

public class PreferencesMainFragment extends Fragment implements FragmentActionGenerator {
    public static final String TAG = "preferences_main";

    private View view;
    private Unbinder unbinder;

    @BindView(R.id.image_button_theme_selection) ImageView buttonThemeSelection;
    @BindView(R.id.switch_notification) SwitchCompat switchNotification;
    @BindView(R.id.switch_lock_app_option) SwitchCompat switchLockApp;
    @BindView(R.id.root_clear_user_data) View viewClearData;

    private FragmentActionListener listener;

    public PreferencesMainFragment() {}

    public static PreferencesMainFragment newInstance() {
        PreferencesMainFragment fragment = new PreferencesMainFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_preferences, container, false);
        unbinder = ButterKnife.bind(this, view);

        MainActivity.analyticsService.logInfo(TAG);

        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.preferences));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
    }

    private void initialise(){
        buttonThemeSelection.setOnClickListener(view -> startThemeSelection());

        switchNotification.setOnClickListener(view -> notificationOptionChanged());

        switchLockApp.setOnClickListener(view -> lockOptionChanged());

        viewClearData.setOnClickListener(view -> clearUserData());

        refreshPreferencesStatus();
    }

    private void refreshPreferencesStatus(){
        if(!AuthenticationInfo.getInstance().getUserPinPrimary(getContext()).isEmpty())
            switchLockApp.setChecked(true);
        else
            switchLockApp.setChecked(false);
    }

    private void startThemeSelection(){
        // TODO:
    }

    private void notificationOptionChanged(){
        // TODO:
    }

    private void lockOptionChanged(){
        switchLockApp.setEnabled(false);

        boolean previousSwitchState = !switchLockApp.isChecked();

        Bundle resultBundle = new Bundle();

        DialogInterface.OnCancelListener onCancelListener =  new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                switchLockApp.setEnabled(true);
                switchLockApp.setChecked(previousSwitchState);
            }
        };

        if(switchLockApp.isChecked()) {
            // switch status turned into on
            if(AuthenticationInfo.getInstance().getAuthorizationInfo().isAuthorizedToUseLockFeature()) {
                DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int whichButton) {
                        switch (whichButton) {
                            case DialogInterface.BUTTON_POSITIVE:
                                String primary = resultBundle.getString(AuthenticationInfo.USER_PIN_PRIMARY);
                                String secondary = resultBundle.getString(AuthenticationInfo.USER_PIN_SECONDARY);

                                if (primary != null && secondary != null && primary.length() != AppRules.PIN_LENGTH && secondary.length() != AppRules.PIN_LENGTH) {
                                    Toast.makeText(getContext(), getString(R.string.setup_pin_message), Toast.LENGTH_SHORT).show();
                                    DialogFactory.getSetupPinDialog(getContext(), this, onCancelListener, resultBundle);
                                } else {
                                    Toast.makeText(getContext(), getString(R.string.successful), Toast.LENGTH_SHORT).show();
                                    AuthenticationInfo.getInstance().saveUserPin(getContext(), primary, secondary);
                                }

                                break;
                            default:
                                switchLockApp.setChecked(previousSwitchState);
                                break;
                        }

                        switchLockApp.setEnabled(true);
                    }
                };

                DialogFactory.getSetupPinDialog(getContext(), onClickListener, onCancelListener, resultBundle);
            } else {
                switchLockApp.setChecked(previousSwitchState);
                switchLockApp.setEnabled(true);
                notAuthorizedToLock();
            }
        } else {
            // switch status turned into off
            DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int whichButton) {
                    switch (whichButton) {
                        case DialogInterface.BUTTON_POSITIVE:
                            boolean rightPin = false;

                            String pin = resultBundle.getString(AuthenticationInfo.USER_PIN);
                            String userPinPrimary = AuthenticationInfo.getInstance().getUserPinPrimary(getContext());
                            String userPinSecondary = AuthenticationInfo.getInstance().getUserPinSecondary(getContext());

                            if(pin != null && (pin.compareTo(userPinPrimary) == 0 || pin.compareTo(userPinSecondary) == 0)){
                                rightPin = true;
                            }

                            if(rightPin) {
                                //Clear pin
                                AuthenticationInfo.getInstance().saveUserPin(getContext(), "", "");
                                Toast.makeText(getContext(), getString(R.string.successful), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), getString(R.string.ask_pin_error_message), Toast.LENGTH_SHORT).show();
                                DialogFactory.getRemovePinDialog(getContext(), this, onCancelListener, resultBundle);
                            }

                            break;
                        default:
                            switchLockApp.setChecked(previousSwitchState);
                            break;
                    }

                    switchLockApp.setEnabled(true);
                }
            };

            DialogFactory.getRemovePinDialog(getContext(),onClickListener, onCancelListener, resultBundle);
        }
    }

    private void notAuthorizedToLock() {
        DialogFactory.getNotAuthorizedToUseLockFeatureDialog(getContext(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int whichButton) {
                if (whichButton == DialogInterface.BUTTON_POSITIVE) {
                    if (listener != null) {
                        try {
                            listener.onGoto(FragmentActionListener.GOTO.SHOP);
                        } catch (CannotGotoException ex) {
                            Log.e(TAG, ex.toString());
                        }
                    }
                    ;
                }
            }
        });
    }

    private void clearUserData(){
        DialogFactory.getAreYouSureDialog(getContext(),  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        AuthenticationInfo.getInstance().clearAllUserData(getContext());
                        refreshPreferencesStatus();
                        break;
                }
            }
        });
    }

    @Override
    public void setFragmentActionListener(FragmentActionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener = null;
        unbinder.unbind();
    }
}
