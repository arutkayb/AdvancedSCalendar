package centertable.advancedscalendar.modules.action_menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.util.view.ActionBarUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyPolicyFragment extends Fragment {
    public static final String TAG = "privacy_policy_fragment";

    private WebView webview;
    private FrameLayout bannerAdContainer;

    public static PrivacyPolicyFragment newInstance() {
        PrivacyPolicyFragment fragment = new PrivacyPolicyFragment();
        return fragment;
    }

    public PrivacyPolicyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.privacy_policy));

        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);

        webview = view.findViewById(R.id.webview_privacy_policy);
        bannerAdContainer = view.findViewById(R.id.ad_container);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        webview.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Toast.makeText(getContext(), getString(R.string.unknown_error_try_later), Toast.LENGTH_SHORT).show();
                super.onReceivedError(view, request, error);
            }
        });

        webview.loadUrl(getString(R.string.privacy_policy_url));

        MainActivity.adFactory.bindBannerAd(getContext(), bannerAdContainer);

        MainActivity.analyticsService.logInfo(TAG);
    }

}
