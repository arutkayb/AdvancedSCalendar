package centertable.advancedscalendar.modules.action_menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.analytics.AnalyticsService;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.util.view.ActionBarUtil;


public class AboutUsFragment extends Fragment {
    public static final String TAG = "about_us_fragment";

    private FrameLayout bannerAdContainer;

    public static AboutUsFragment newInstance() {
        AboutUsFragment fragment = new AboutUsFragment();
        return fragment;
    }

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.about_us));

        View view = inflater.inflate(R.layout.fragment_about_us, container, false);

        bannerAdContainer = view.findViewById(R.id.ad_container);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        MainActivity.adFactory.bindBannerAd(getContext(), bannerAdContainer);

        MainActivity.analyticsService.logInfo(TAG);
    }
}
