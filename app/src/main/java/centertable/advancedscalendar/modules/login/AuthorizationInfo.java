package centertable.advancedscalendar.modules.login;

import java.util.HashMap;
import java.util.Map;

import centertable.advancedscalendar.data.remote.billing.InAppPurchase;
import centertable.advancedscalendar.data.remote.billing.google.GooglePurchase;
import centertable.advancedscalendar.common.rules.AppRules;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.remote.authorization.AuthorizationDatabase;
import centertable.advancedscalendar.data.remote.authorization.firebase.FirebaseDBAuthorization;

public class AuthorizationInfo {
    private AuthorizationDatabase authorizationDatabase;
    private InAppPurchase inAppPurchase;

    private Map<String, AuthorizationDatabase.ProductPurchaseDetail> productIdProductPurchaseDetailsMap;

    AuthorizationInfo(User user){
        productIdProductPurchaseDetailsMap = new HashMap<>();

        inAppPurchase = new GooglePurchase();

        authorizationDatabase = new FirebaseDBAuthorization();
        authorizationDatabase.initialise(user);
        authorizationDatabase.readUser(new AuthorizationDatabase.OnStatusListener() {
            @Override
            public void onSuccess() {
                pullAuthorizationInfoFromRemote();
            }

            @Override
            public void onFailure() {
                authorizationDatabase.insertUser();
                authorizationDatabase.readUser(this);
            }
        });
    }

    public boolean isAuthorizedToAddPartner(int numberOfCreatedPartners){
        boolean unlimitedPartner = getAuthorizationStatusForProduct(InAppPurchase.Product.LIMITLESS_PARTNER);
        return (numberOfCreatedPartners < AppRules.NUMBER_OF_PARTNERS || unlimitedPartner);
    }

    public boolean isAuthorizedToUseLockFeature(){
        return getAuthorizationStatusForProduct(InAppPurchase.Product.LOCK_FEATURE);
    }

    public boolean isAuthorizedToRemoveAds(){
        return getAuthorizationStatusForProduct(InAppPurchase.Product.REMOVE_ADS);
    }

    // TODO: handle refunds
    private void pullAuthorizationInfoFromRemote(){
        authorizationDatabase.readPurchaseData(new AuthorizationDatabase.DataEventListener<AuthorizationDatabase.ProductPurchaseDetail>() {
            @Override
            public void onDataChange(AuthorizationDatabase.ProductPurchaseDetail result) {
                if(result != null) {
                    productIdProductPurchaseDetailsMap.put(result.getProductId(), result);
                }
            }
            @Override
            public void onCancelled() {

            }
        });
    }

    public InAppPurchase getPurchaseMethod(){
        return inAppPurchase;
    }

    public boolean getAuthorizationStatusForProduct(String productId){
        boolean res = false;

        try {
            res = productIdProductPurchaseDetailsMap.get(productId).isPurchased();
        } catch (Exception ex) {}

        return res;
    }

    public void setAuthorizationStatusForProduct(AuthorizationDatabase.ProductPurchaseDetail productPurchaseDetail) {
        productIdProductPurchaseDetailsMap.put(productPurchaseDetail.getProductId(), productPurchaseDetail);
        authorizationDatabase.updatePurchaseData(productPurchaseDetail);
    }
}
