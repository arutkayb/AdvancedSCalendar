package centertable.advancedscalendar.modules.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.data.remote.authentication.Authentication;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import centertable.advancedscalendar.common.indication.DialogFactory;
import centertable.advancedscalendar.util.network.NetworkUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class LoginActivity extends AppCompatActivity {
    private Authentication authentication;

    @BindString(R.string.cannot_login_message) String cannotLoginMessage;
    @BindString(R.string.check_your_connection) String checkConnectionMessage;
    @BindString(R.string.wrong_credentials) String wrongCredentialsMessage;
    @BindString(R.string.ok) String okString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        Intent intent = getIntent();
        Bundle bundle;

        if(intent == null || (authentication = intent.getParcelableExtra(Authentication.AUTHENTICATION_PARCELABLE)) == null) {
            Log.e(getClass().getName(), "Please pass an authentication object");
            returnMainWithResult(Authentication.RESPONSE_CODE.CANCELED.getValue());
        }

        ButterKnife.bind(this);
        RoomDataAccessUtil.initialise(this);

        //TODO: splash screen and non-default user options will be implemented to this class
        start();
    }

    private void start(){
        long lastUserId = AuthenticationInfo.getInstance().getSavedLastUserId(getApplicationContext());

        if(lastUserId != 0 && authentication.isUserSignedIn(lastUserId)){
            RoomDataAccessUtil.getUserById(new Observer<User>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(User user) {
                    AuthenticationInfo.getInstance().setCurrentUser(getApplicationContext(), user);

                    if(!AuthenticationInfo.getInstance().getUserPinPrimary(getApplicationContext()).isEmpty()){
                        authenticateWithPin();
                    } else {
                        returnMainWithResult(Authentication.RESPONSE_CODE.SUCCESSFUL.getValue());
                    }
                }

                @Override
                public void onError(Throwable e) {
                    login();
                }

                @Override
                public void onComplete() {

                }
            }, lastUserId);
        } else
            login();

    }

    private void authenticateWithPin(){
        Bundle resultBundle = new Bundle();

        DialogInterface.OnCancelListener onCancelListener =  new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                authenticateWithPin();
            }
        };

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int whichButton) {
                switch (whichButton) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean rightPin = false;

                        String pin = resultBundle.getString(AuthenticationInfo.getInstance().USER_PIN);
                        String userPinPrimary = AuthenticationInfo.getInstance().getUserPinPrimary(LoginActivity.this);
                        String userPinSecondary = AuthenticationInfo.getInstance().getUserPinSecondary(LoginActivity.this);

                        if(pin != null && (pin.compareTo(userPinPrimary) == 0 || pin.compareTo(userPinSecondary) == 0)){
                            rightPin = true;
                        }

                        if(rightPin) {
                            returnMainWithResult(Authentication.RESPONSE_CODE.SUCCESSFUL.getValue());
                        } else {
                            authenticateWithPin();
                            Toast.makeText(LoginActivity.this, getString(R.string.ask_pin_error_message), Toast.LENGTH_SHORT).show();
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                    case DialogInterface.BUTTON_NEUTRAL:
                        returnMainWithResult(Authentication.RESPONSE_CODE.CANCELED.getValue());
                        break;
                    default:
                        authenticateWithPin();
                        break;
                }

            }
        };

        DialogFactory.getAskPinDialog(LoginActivity.this,onClickListener, onCancelListener, resultBundle);
    }

    private void login() {
        if(NetworkUtil.isNetworkAvailable(this)) {
            authentication.signIn(this);
        } else {
            DialogFactory.getCheckConnectionDialog(this,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            login();
                        }
                    },
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            login();
                        }
                    }
            );
        }
    }

    private void insertUserIfNotInserted(User user) {
        RoomDataAccessUtil.getUserById(new Observer<User>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(User resUser) {
                AuthenticationInfo.getInstance().setCurrentUser(getApplicationContext(), user);
                returnMainWithResult(Authentication.RESPONSE_CODE.SUCCESSFUL.getValue());
            }

            @Override
            public void onError(Throwable e) {
                insertUser(user);
            }

            @Override
            public void onComplete() {

            }
        }, user.getUserId());
    }

    private void insertUser(User user){
        RoomDataAccessUtil.insertUser(
                new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean result) {
                        if (result) {
                            AuthenticationInfo.getInstance().setCurrentUser(getApplicationContext(), user);
                            returnMainWithResult(Authentication.RESPONSE_CODE.SUCCESSFUL.getValue());
                        } else {
                            tryLoginAgainWithError(cannotLoginMessage);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(getClass().getName(), e.toString());
                        tryLoginAgainWithError(cannotLoginMessage);
                    }

                    @Override
                    public void onComplete() {

                    }
                }
                , user);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == Authentication.REQUEST_CODE.SIGN_IN.getValue()){
            responseToSignIn(resultCode, data);
        }
    }

    private void responseToSignIn(int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            User user = new User();

            Authentication.RESPONSE_CODE response = authentication.getSignInResult(user, data);

            switch (response){
                case SUCCESSFUL:
                    if(user.isNewUser) {
                        authentication.sendWelcomeMail();
                    }

                    insertUserIfNotInserted(user);
                    break;
                case CANCELED:
                    returnMainWithResult(response.getValue());
                    break;
                case NO_NETWORK:
                    DialogFactory.getCheckConnectionDialog(this,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    login();
                                }
                            },
                            new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {
                                    login();
                                }
                            }
                    );
                case PROVIDER_ERROR:
                    tryLoginAgainWithError(getString(R.string.login_method_unsupported_device_please_try_another_method));
                    break;
                case WRONG_CREDENTIAL:
                    tryLoginAgainWithError(wrongCredentialsMessage);
                    break;
                case USER_NOT_FOUND:
                    authentication.signUp(this);
                default:
                    tryLoginAgainWithError(cannotLoginMessage);
                    break;
            }
        } else {
            Log.e(getClass().getName(), "Login error, result code " + resultCode);
            login();
        }
    }

    private void setActivityResult(int result){
        setResult(result);
        finish();
    }

    private void tryLoginAgainWithError(String error){
        if(error != null && !error.isEmpty())
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

        login();
    }

    private void returnMainWithResult(int result){
        setActivityResult(result);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
