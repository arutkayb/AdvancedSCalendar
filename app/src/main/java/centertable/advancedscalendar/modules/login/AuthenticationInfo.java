package centertable.advancedscalendar.modules.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.widget.Toast;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.modules.widget.StatisticsIntentService;
import centertable.advancedscalendar.util.data.SharedPreferencesUtil;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class AuthenticationInfo {
    private static final String LAST_USER_ID = "last_user_id";
    public static final String USER_PIN = "user_pin";
    public static final String USER_PIN_PRIMARY = "user_pin_primary";
    public static final String USER_PIN_SECONDARY = "user_pin_secondary";

    private static final String APP_PREF = "app_pref";
    private static String USER_PREF = "user_pref";
    private static final String USER_PREF_DEFAULT = "user_pref";

    private User currentUser;
    private AuthorizationInfo authorizationInfo;

    private static AuthenticationInfo authenticationInfo;
    private AuthenticationInfo(){
        currentUser = new User();
    }

    public static AuthenticationInfo getInstance(){
        if(authenticationInfo == null)
            authenticationInfo = new AuthenticationInfo();
        
        return authenticationInfo;
    }

    public AuthorizationInfo getAuthorizationInfo() {
        return authorizationInfo;
    }

    public long getCurrentUserId()
    {
        return currentUser.getUserId();
    }

    public void setCurrentUser(Context context, @NonNull User user)
    {
        USER_PREF = USER_PREF_DEFAULT + String.valueOf(user.getUserId());
        currentUser = user;
        saveLastUserId(context, currentUser.getPrimaryId());
        StatisticsIntentService.startActionUpdateWidget(context);
        authorizationInfo = new AuthorizationInfo(currentUser);
    }

    public User getCurrentUser()
    {
        return currentUser;
    }

    public int getThemeSelection(long userId)
    {
        int ret = 0;
        //TODO fill here
        return ret;
    }

    private void saveLastUserId(Context context, long userId){
        SharedPreferences.Editor editor = SharedPreferencesUtil.getSharedPrefEditor(context, APP_PREF);
        editor.putLong(LAST_USER_ID, userId).apply();
    }

    public long getSavedLastUserId(Context context){
        SharedPreferences pref = SharedPreferencesUtil.getSharedPref(context, APP_PREF);
        return pref.getLong(LAST_USER_ID, 0);
    }

    public void saveUserPin(Context context, String primaryPin, String secondaryPin){
        SharedPreferences.Editor editor = SharedPreferencesUtil.getSharedPrefEditor(context, USER_PREF);
        editor.putString(USER_PIN_PRIMARY, primaryPin)
                .putString(USER_PIN_SECONDARY, secondaryPin)
                .apply();
    }

    public String getUserPinPrimary(Context context){
        SharedPreferences pref = SharedPreferencesUtil.getSharedPref(context, USER_PREF);
        return pref.getString(USER_PIN_PRIMARY, "");
    }

    public String getUserPinSecondary(Context context){
        SharedPreferences pref = SharedPreferencesUtil.getSharedPref(context, USER_PREF);
        return pref.getString(USER_PIN_SECONDARY, "");
    }

    public void clearAllUserData(Context context){
        SharedPreferences pref = SharedPreferencesUtil.getSharedPref(context, USER_PREF);
        pref.edit().clear().apply();

        RoomDataAccessUtil.clearUser(new Observer<Boolean>() {
                                         @Override
                                         public void onSubscribe(Disposable d) {

                                         }

                                         @Override
                                         public void onNext(Boolean res) {
                                             if(res){
                                                 Toast.makeText(context, context.getString(R.string.successful), Toast.LENGTH_SHORT).show();
                                             }
                                         }

                                         @Override
                                         public void onError(Throwable e) {

                                         }

                                         @Override
                                         public void onComplete() {

                                         }
                                     }
                ,getCurrentUser());

        saveLastUserId(context, currentUser.getPrimaryId());
    }

    public void signOutCurrentUser(Context context){
        saveUserPin(context, "", "");
        saveLastUserId(context, 0);
        StatisticsIntentService.startActionUpdateWidget(context);
        USER_PREF = USER_PREF_DEFAULT;
    }
}
