package centertable.advancedscalendar.modules;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.ad.AdFactory;
import centertable.advancedscalendar.common.ad.admob.AdmobAdFactory;
import centertable.advancedscalendar.common.analytics.AnalyticsService;
import centertable.advancedscalendar.common.analytics.firebase.FirebaseAnalyticsService;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.fragment.exception.CannotGotoException;
import centertable.advancedscalendar.data.remote.authentication.Authentication;
import centertable.advancedscalendar.data.remote.authentication.firebase.FirebaseAuthentication;
import centertable.advancedscalendar.modules.action_menu.AboutUsFragment;
import centertable.advancedscalendar.modules.action_menu.PrivacyPolicyFragment;
import centertable.advancedscalendar.modules.calendar.CalendarMainFragment;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.modules.login.LoginActivity;
import centertable.advancedscalendar.modules.partner.PartnerMainFragment;
import centertable.advancedscalendar.modules.preferences.PreferencesMainFragment;
import centertable.advancedscalendar.modules.shop.ShopMainFragment;
import centertable.advancedscalendar.modules.statistics.StatisticsMainFragment;
import centertable.advancedscalendar.common.indication.DialogFactory;
import centertable.advancedscalendar.modules.user_profile.UserMainFragment;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "main_activity";

    private static final String BACKSTACK_CALENDAR = "root_backstack";

    public static AnalyticsService analyticsService;
    public static AdFactory adFactory;

    private StatisticsMainFragment statisticsMainFragment;
    private CalendarMainFragment calendarMainFragment;
    private PartnerMainFragment partnerMainFragment;
    private ShopMainFragment shopMainFragment;
    private PreferencesMainFragment preferencesMainFragment;

    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @BindView(R.id.nav_view) NavigationView navigationView;

    private FragmentActionListener fragmentActionListener;

    private Authentication authentication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initialise();

        if(savedInstanceState == null) {
            startLoginActivityForSignIn();
        }else{
            restoreState(savedInstanceState);
            userSignedIn();
        }
    }

    private void userSignedIn(){
        if(adFactory == null)
            adFactory = AdmobAdFactory.getInstance();

        launchCalendarMainFragment();
        setMenus();

        analyticsService.logInfo("User signed in");
    }

    private void initialise(){
        analyticsService = new FirebaseAnalyticsService(getApplicationContext());
        analyticsService.logInfo("Analytics initialised");

        fragmentActionListener = new FragmentActionListener() {
            @Override
            public void onGoto(GOTO param) {
                int menuId = -1;

                switch (param){
                    case SHOP:
                        analyticsService.logInfo("GOTO: SHOP");
                        menuId = R.id.nav_shop;
                        break;
                    case CALENDAR:
                        analyticsService.logInfo("GOTO: CALENDAR");
                        menuId = R.id.nav_calendar;
                        break;
                    default:
                        break;
                }

                try {
                    launchNavigationMenuItem(menuId);
                } catch (CannotGotoException ex){
                    Log.e(getClass().getName(), ex.toString());
                }
            }

            @Override
            public void onExit() {
                onBackPressed();
            }
        };

        authentication = new FirebaseAuthentication();
    }

    private void startLoginActivityForSignIn(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Authentication.AUTHENTICATION_PARCELABLE, authentication);
        startActivityForResult(intent, Authentication.REQUEST_CODE.SIGN_IN.getValue());
    }

    private void setMenus(){
        setToolbar();
        setNavigationMenu();
    }

    private void setToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24);
    }

    private void setNavigationMenu(){
        //set first item selected in the first place
        navigationView.getMenu().getItem(0).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();

                        try {
                            launchNavigationMenuItem(menuItem.getItemId());
                        } catch (CannotGotoException ex){
                            Log.e(getClass().getName(), ex.toString());
                        }

                        return true;
                    }
                });

        setNavigationMenuHeader();
    }

    private void instantSignOut(){
        authentication.signOut(this, new Authentication.OnCompleteListener() {
            @Override
            public void onComplete(Authentication.RESPONSE_CODE response) {
                if(response == Authentication.RESPONSE_CODE.SUCCESSFUL) {
                    analyticsService.logInfo("User signed out");

                    AuthenticationInfo.getInstance().signOutCurrentUser(MainActivity.this);

                    clearFragments();

                    startLoginActivityForSignIn();
                }
            }
        });
    }

    private void clearFragments() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if(fragment != null)
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }

        calendarMainFragment = null;
        statisticsMainFragment = null;
        partnerMainFragment = null;
        shopMainFragment = null;
        preferencesMainFragment = null;
    }

    private void signOut(boolean showDialog){
        if(showDialog) {
            DialogFactory.getAreYouSureDialog(this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            instantSignOut();
                            break;
                    }
                }
            });
        } else {
            instantSignOut();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == Authentication.REQUEST_CODE.SIGN_IN.getValue()){
            if(resultCode == Authentication.RESPONSE_CODE.SUCCESSFUL.getValue()) {
                userSignedIn();
            } else if(resultCode == Authentication.RESPONSE_CODE.CANCELED.getValue()){
                analyticsService.logInfo("Login request cancelled");
                signOut(false);
            } else {
                analyticsService.logInfo("Login request failed");
                finish();
            }
        }
    }

    private void setNavigationMenuHeader(){
        View headerView = navigationView.getHeaderView(0);
        if(headerView != null) {
            TextView headerText = headerView.findViewById(R.id.header_text);

            String userNick = AuthenticationInfo.getInstance().getCurrentUser().getName();
            headerText.setText(userNick);

            headerText.setOnClickListener(view -> {
                drawerLayout.closeDrawers();
                launchUserMainFragment();
            });
        }
    }

    private void launchNavigationMenuItem(int itemId) throws CannotGotoException {
        switch (itemId) {
            case R.id.nav_calendar:
                launchCalendarMainFragment();
                break;
            case R.id.nav_statistics:
                launchStatisticsMainFragment();
                break;
            case R.id.nav_partners:
                launchPartnerMainFragment();
                break;
            case R.id.nav_shop:
                launchShopMainFragment();
                break;
            case R.id.nav_preferences:
                launchPreferencesMainFragment();
                break;
            default:
                throw new CannotGotoException();
        }

        // set item as selected to persist highlight
        navigationView.setCheckedItem(itemId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            /*case R.id.report_a_problem:

                break;*/
            case R.id.privacy_policy:
                launchPrivacyPolicyFragment();
                break;

            case R.id.about_us:
                launchAboutUsFragment();
                break;

            case R.id.sign_out:
                signOut(true);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    //TODO: make the application to keep app state
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(calendarMainFragment != null && calendarMainFragment.isAdded())
            getSupportFragmentManager().putFragment(outState, CalendarMainFragment.TAG, calendarMainFragment);

        if(statisticsMainFragment != null && statisticsMainFragment.isAdded())
            getSupportFragmentManager().putFragment(outState, StatisticsMainFragment.TAG, statisticsMainFragment);

        if(partnerMainFragment != null && partnerMainFragment.isAdded())
            getSupportFragmentManager().putFragment(outState, PartnerMainFragment.TAG, partnerMainFragment);

        if(shopMainFragment != null && shopMainFragment.isAdded())
            getSupportFragmentManager().putFragment(outState, ShopMainFragment.TAG, shopMainFragment);

        if(preferencesMainFragment != null && preferencesMainFragment.isAdded())
            getSupportFragmentManager().putFragment(outState, PreferencesMainFragment.TAG, preferencesMainFragment);

    }

    //TODO: refactor the fragment restoration method in order to fulfill oop aspect
    private void restoreState(Bundle savedInstanceState){
        calendarMainFragment = (CalendarMainFragment) getSupportFragmentManager().getFragment(savedInstanceState, CalendarMainFragment.TAG);
        statisticsMainFragment = (StatisticsMainFragment) getSupportFragmentManager().getFragment(savedInstanceState, StatisticsMainFragment.TAG);
        partnerMainFragment = (PartnerMainFragment) getSupportFragmentManager().getFragment(savedInstanceState, PartnerMainFragment.TAG);
        shopMainFragment = (ShopMainFragment) getSupportFragmentManager().getFragment(savedInstanceState, ShopMainFragment.TAG);
        preferencesMainFragment = (PreferencesMainFragment) getSupportFragmentManager().getFragment(savedInstanceState, PreferencesMainFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            int popped = 0;

            List<Fragment> fragments = getSupportFragmentManager().getFragments();

            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible()) {
                    if(fragment.getChildFragmentManager().popBackStackImmediate())
                        popped++;
                }
            }

            if(popped == 0) {
                if(getSupportFragmentManager().getBackStackEntryCount()>0) {
                    getSupportFragmentManager().popBackStack(BACKSTACK_CALENDAR,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    // It is set to calendar because of the root backstack is calendar permanently
                    // TODO: here is a dependency, fix it
                    navigationView.setCheckedItem(R.id.nav_calendar);
                } else
                    super.onBackPressed();
            }
        }
    }

    //TODO: Refactor here, think about better way to prevent duplication
    private void launchCalendarMainFragment(){
        if(calendarMainFragment == null) {
            calendarMainFragment = CalendarMainFragment.newInstance();
            calendarMainFragment.setRetainInstance(true);
        }

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(CalendarMainFragment.TAG);

        //Prevent doubling the calendar fragment
        if(fragment == null){

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, calendarMainFragment, CalendarMainFragment.TAG)
                    .commit();
        }else{
            getSupportFragmentManager().popBackStack(BACKSTACK_CALENDAR, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private void launchStatisticsMainFragment(){
        statisticsMainFragment = StatisticsMainFragment.newInstance();

        statisticsMainFragment.setFragmentActionListener(fragmentActionListener);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, statisticsMainFragment, StatisticsMainFragment.TAG)
                .commit();
    }

    private void launchPartnerMainFragment(){
        partnerMainFragment = PartnerMainFragment.newInstance();

        partnerMainFragment.setFragmentActionListener(fragmentActionListener);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, partnerMainFragment, PartnerMainFragment.TAG)
                .commit();
    }

    private void launchShopMainFragment(){
        shopMainFragment = ShopMainFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, shopMainFragment, ShopMainFragment.TAG)
                .commit();
    }

    private void launchPreferencesMainFragment(){
        preferencesMainFragment = PreferencesMainFragment.newInstance();

        preferencesMainFragment.setFragmentActionListener(fragmentActionListener);

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, preferencesMainFragment, PreferencesMainFragment.TAG)
                .commit();
    }

    private void launchAboutUsFragment(){
        AboutUsFragment aboutUsFragment = AboutUsFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, aboutUsFragment, AboutUsFragment.TAG)
                .commit();
    }

    private void launchPrivacyPolicyFragment(){
        PrivacyPolicyFragment privacyPolicyFragment = PrivacyPolicyFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, privacyPolicyFragment, PrivacyPolicyFragment.TAG)
                .commit();
    }

    private void launchUserMainFragment(){
        UserMainFragment userMainFragment = UserMainFragment.newInstance();

        userMainFragment.setFragmentActionListener(new FragmentActionListener() {
            @Override
            public void onExit() {
                onBackPressed();
            }

            @Override
            public void onUpdate() {
                setNavigationMenuHeader();
            }
        });

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK_CALENDAR)
                .replace(R.id.fragment_container, userMainFragment, UserMainFragment.TAG)
                .commit();
    }

}
