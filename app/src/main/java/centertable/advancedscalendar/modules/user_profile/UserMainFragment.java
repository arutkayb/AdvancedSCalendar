package centertable.advancedscalendar.modules.user_profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.util.view.ActionBarUtil;


public class UserMainFragment extends Fragment implements FragmentActionGenerator {
    public static final String TAG = "user_main_fragment";
    private View view;
    private Unbinder unbinder;
    private FragmentActionListener listener;

    public UserMainFragment() {
        // Required empty public constructor
    }

    public static UserMainFragment newInstance() {
        UserMainFragment fragment = new UserMainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_main, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.profile));

        launchUserProfileFragment();
    }

    private void launchUserProfileFragment(){
        UserProfileFragment userProfileFragment = UserProfileFragment.newInstance();

        userProfileFragment.setFragmentActionListener(new FragmentActionListener() {
            @Override
            public void onExit() {
                if(listener != null)
                    listener.onExit();
            }

            @Override
            public void onUpdate() {
                if(listener != null)
                    listener.onUpdate();
            }
        });

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, userProfileFragment, UserProfileFragment.TAG)
                .commit();
    }

    @Override
    public void setFragmentActionListener(FragmentActionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
