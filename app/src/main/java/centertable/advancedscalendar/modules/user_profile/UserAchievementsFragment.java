package centertable.advancedscalendar.modules.user_profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;

public class UserAchievementsFragment extends Fragment {
    private View view;
    private Unbinder unbinder;

    public UserAchievementsFragment() {
        // Required empty public constructor
    }

    public static UserAchievementsFragment newInstance() {
        UserAchievementsFragment fragment = new UserAchievementsFragment();
        Bundle args = new Bundle();
        // TODO: set args here
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //TODO: get args here
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_achievements, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
