package centertable.advancedscalendar.modules.user_profile;


import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.view.app_bar.AppBarStateChangeListener;
import centertable.advancedscalendar.common.android_component.view.spinner.SpinnerAdapterFactory;
import centertable.advancedscalendar.data.definitions.ProfileDefinitions;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UserProfileFragment extends Fragment implements FragmentActionGenerator {
    public static final String TAG = "user_profile";
    private View view;
    private Unbinder unbinder;
    private FragmentActionListener listener;

    private User user;

    @BindView(R.id.full_name) EditText fullName;
    @BindView(R.id.email) TextView email;
    @BindView(R.id.gender) Spinner gender;

    @BindView(R.id.app_bar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.submit) Button submitButton;
    @BindView(R.id.cancel) Button cancelButton;
    @BindView(R.id.delete) Button deleteEntryButton;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        MainActivity.analyticsService.logInfo(TAG);

        initialise();

        return view;
    }

    private void initialise(){
        user = AuthenticationInfo.getInstance().getCurrentUser();

        // There is no delete option for users here
        deleteEntryButton.setVisibility(View.GONE);

        submitButton.setOnClickListener(view -> submitUserProfile());

        cancelButton.setOnClickListener(view -> cancelPressed());

        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                RelativeLayout rootView = view.findViewById(R.id.user_profile_root);

                if(state == State.COLLAPSED) {
                    rootView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            appBarLayout.setExpanded(true);
                        }
                    });
                } else {
                    rootView.setOnClickListener(null);
                }
            }
        });


        ArrayList genderList = new ArrayList<String>(Arrays.asList(
                getContext().getResources().getStringArray(R.array.genders)
        ));
        gender.setAdapter(SpinnerAdapterFactory.getCommonSpinnerArrayAdapter(
                getContext(),
                genderList)
        );

        gender.setSelection(user.getGender().getValue());
        fullName.setText(user.getName());
        email.setText(user.getEmail());
    }

    private void submitUserProfile(){
        MainActivity.analyticsService.logInfo("Submit user profile is clicked");

        String newName = fullName.getText().toString();
        if(!newName.isEmpty()) {
            user.setName(newName);
            user.setGender(ProfileDefinitions.GENDER.fromValue((int)gender.getSelectedItemId()));

            RoomDataAccessUtil.updateUser(new Observer<Boolean>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(Boolean aBoolean) {
                    Toast.makeText(getContext(), getString(R.string.successful), Toast.LENGTH_SHORT).show();

                    if(listener != null){
                        listener.onUpdate();
                        listener.onExit();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    Toast.makeText(getContext(), getString(R.string.unknown_error_try_later), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onComplete() {

                }
            }, user);

        } else {
            Toast.makeText(getContext(), getString(R.string.write_your_name), Toast.LENGTH_SHORT).show();
        }
    }

    private void cancelPressed(){
        if(listener != null){
            listener.onExit();
        }
    }

    @Override
    public void setFragmentActionListener(FragmentActionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
