package centertable.advancedscalendar.modules.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import java.util.List;

import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import centertable.advancedscalendar.util.data.statistics.SimpleEntryStatistics;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class StatisticsIntentService extends IntentService {
    public static final String ACTION_UPDATE_STATISTICS = "centertable.advancedscalendar.modules.widget.action.UPDATE_STATISTICS";

    public StatisticsIntentService() {
        super("StatisticsIntentService");
    }

    public static void startActionUpdateWidget(Context context) {
        RoomDataAccessUtil.initialise(context);

        if(AuthenticationInfo.getInstance().getCurrentUser().getUserId() == 0){
            long lastUserId = AuthenticationInfo.getInstance().getSavedLastUserId(context);

            if(lastUserId != 0) {
                RoomDataAccessUtil.getUserById(
                        new Observer<User>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(User user) {
                                AuthenticationInfo.getInstance().setCurrentUser(context, user);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        }
                        , lastUserId);

            }else {
                Log.d("StatisticsIntentService", "There is no created user.");
            }
        }

        Intent intent = new Intent(context, StatisticsIntentService.class);
        intent.setAction(ACTION_UPDATE_STATISTICS);
        context.getApplicationContext().startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_STATISTICS.equals(action)) {
                handleActionUpdateStatistics();
            }
        }
    }

    private void handleActionUpdateStatistics() {
        refreshContent();
    }

    private void refreshContent(){
        if(AuthenticationInfo.getInstance().getCurrentUser().getUserId() != 0) {
            //TODO: loading image may be good here
            RoomDataAccessUtil.getAllEntries(new Observer<List<Entry>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(List<Entry> entryList) {
                    if (entryList != null) {
                        SimpleEntryStatistics simpleEntryStatistics = new SimpleEntryStatistics();
                        simpleEntryStatistics.calculateStatisticsForEntryList(entryList);

                        Intent intent = new Intent(getApplicationContext(), StatisticsWidget.class);
                        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), StatisticsWidget.class));
                        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                        intent.putExtra(SimpleEntryStatistics.TAG_STATISTICS_PARCELABLE, simpleEntryStatistics);
                        sendBroadcast(intent);
                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });
        }

    }
}
