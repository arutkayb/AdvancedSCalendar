package centertable.advancedscalendar.modules.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.text.DecimalFormat;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.util.data.statistics.SimpleEntryStatistics;

public class StatisticsWidget extends AppWidgetProvider {
    private static DecimalFormat decimalFormat = new DecimalFormat(".#");

    private static String totalEntryCountString = "";
    private static String averageRatingString = "";
    private static String averageOrgasmsString = "";
    private static String averageDurationString = "";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_statistics);

        views.setTextViewText(R.id.total_entry_count, totalEntryCountString);
        views.setTextViewText(R.id.average_rating, averageRatingString);
        views.setTextViewText(R.id.average_orgasms, averageOrgasmsString);
        views.setTextViewText(R.id.average_duration, averageDurationString);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.widget_content);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
        ComponentName thisWidget = new ComponentName(context.getApplicationContext(), StatisticsWidget.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        if (appWidgetIds != null && appWidgetIds.length > 0 ) {
            if(intent != null) {
                if(intent.getAction() != null && intent.getAction().compareTo(AppWidgetManager.ACTION_APPWIDGET_ENABLED) == 0){
                    StatisticsIntentService.startActionUpdateWidget(context);
                }else {
                    SimpleEntryStatistics simpleEntryStatistics = intent.getParcelableExtra(SimpleEntryStatistics.TAG_STATISTICS_PARCELABLE);
                    if(simpleEntryStatistics != null) {
                        updateWidgetContent(context, simpleEntryStatistics);
                        onUpdate(context, appWidgetManager, appWidgetIds);
                    }
                }
            }

        }
    }

    private static void updateWidgetContent(Context context, SimpleEntryStatistics statistics){
        if(statistics != null) {
            totalEntryCountString =
                    context.getString(R.string.total_entry_count) + ": " + statistics.getTotalEntryCount();
            averageRatingString =
                    context.getString(R.string.average_rating) + ": " + decimalFormat.format(statistics.getAverageRating());
            averageOrgasmsString =
                    context.getString(R.string.average_orgasms) + ": " + decimalFormat.format(statistics.getAverageOrgasmCount());
            averageDurationString =
                    context.getString(R.string.average_duration) + ": " + decimalFormat.format(statistics.getAverageDurationInMinutes());
        }
    }

}

