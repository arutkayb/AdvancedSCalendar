package centertable.advancedscalendar.modules.calendar;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.modules.widget.StatisticsIntentService;
import centertable.advancedscalendar.util.view.ActionBarUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarMainFragment extends Fragment {
    public static final String TAG = "calendar_main_fragment";
    public static final String BACKSTACK = "calendar_backstack";
    private View view;
    private Unbinder unbinder;
    DataInputFragment dataInputFragment;
    CalendarFragment calendarFragment;
    private FragmentActionListener<Entry> listener;

    public static CalendarMainFragment newInstance(){
        return new CalendarMainFragment();
    }

    public CalendarMainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_calendar_main, container, false);

        ActionBarUtil.setSupportActionBarTitle((AppCompatActivity)getActivity(), getResources().getString(R.string.calendar));

        unbinder = ButterKnife.bind(this, view);

        listener = new FragmentActionListener<Entry>() {
            @Override
            public void onAction(Entry entry) {
                launchDataInputFragment(entry);
            }
        };

        if(savedInstanceState != null){
            calendarFragment = (CalendarFragment)getChildFragmentManager().getFragment(savedInstanceState, CalendarMainFragment.TAG);
        }

        launchCalendarFragment();

        return view;
    }

    //TODO: Refactor here, think about better way to prevent duplication
    private void launchCalendarFragment(){
        if(calendarFragment == null) {
            calendarFragment = CalendarFragment.newInstance();
            calendarFragment.setRetainInstance(true);
        }

        setCalendarCallback();

        Fragment fragment = getChildFragmentManager().findFragmentByTag(CalendarFragment.TAG);

        //Prevent doubling the calendar fragment
        if(fragment == null) {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, calendarFragment, CalendarFragment.TAG)
                    .commit();
        }
    }

    private void setCalendarCallback(){
        if(calendarFragment != null) {
            calendarFragment.setFragmentActionListener(listener);
        }
    }

    private void launchDataInputFragment(Entry entry){
        dataInputFragment = DataInputFragment.newInstance(entry);

        setDataInputCallBack();

        getChildFragmentManager()
                .beginTransaction()
                .addToBackStack(BACKSTACK)
                .replace(R.id.fragment_container, dataInputFragment, DataInputFragment.TAG)
                .commit();
    }

    private void setDataInputCallBack(){
        if(dataInputFragment != null) {
            dataInputFragment.setFragmentActionListener(new FragmentActionListener() {
                @Override
                public void onExit() {
                    getChildFragmentManager().popBackStack(BACKSTACK, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }

                @Override
                public void onUpdate() {
                    if(calendarFragment != null) {
                        calendarFragment.refreshCalendar();
                        refreshHomeScreenWidget();

                        MainActivity.adFactory.loadFullScreenAd(getContext());
                        MainActivity.adFactory.showFullScreenAd();
                    }
                }
            });
        }
    }

    private void refreshHomeScreenWidget(){
        StatisticsIntentService.startActionUpdateWidget(getContext());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if(calendarFragment != null && calendarFragment.isAdded())
            getChildFragmentManager().putFragment(outState, CalendarMainFragment.TAG, calendarFragment);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
