package centertable.advancedscalendar.modules.calendar;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.view.custom.CustomCheckBoxedTextView;
import centertable.advancedscalendar.common.android_component.view.spinner.SpinnerAdapterFactory;
import centertable.advancedscalendar.data.definitions.EntryDefs;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.data.pojo.EntrySexType;
import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DataInputFragment extends Fragment implements FragmentActionGenerator{
    public static final String TAG = "data_input_fragment";

    private FragmentActionListener listener;

    private boolean isNewEntry;
    private Unbinder unbinder;
    private Entry entry;
    private String submitResultNegativeMessage = "";
    private String submitResultPositiveMessage = "";
    private List<CustomCheckBoxedTextView> sexTypeCheckBoxList;

    @BindView(R.id.rating) Spinner rating;
    @BindView(R.id.protection) Spinner protection;
    @BindView(R.id.partner) Spinner partner;
    @BindView(R.id.duration) EditText duration;
    @BindView(R.id.total_orgasms) EditText totalOrg;
    @BindView(R.id.notes) EditText notes;
    @BindView(R.id.submit) Button submitButton;
    @BindView(R.id.cancel) Button cancelButton;
    @BindView(R.id.delete) Button deleteEntryButton;
    @BindView(R.id.ad_container) FrameLayout bannerAdContainer;
    @BindView(R.id.sex_type_container) LinearLayout sexTypeContainer;

    @BindString(R.string.no) String simpleNo;
    @BindString(R.string.yes) String simpleYes;

    @BindString(R.string.and) String simpleAnd;
    @BindString(R.string.or) String simpleOr;

    @BindString(R.string.data_input_entry_deleted) String entryDeletedMessage;
    @BindString(R.string.data_input_submit_entry_added) String entryAddedMessage;
    @BindString(R.string.data_input_submit_entry_updated) String entryUpdatedMessage;
    @BindString(R.string.successful) String successfulString;
    @BindString(R.string.unsuccessful) String unsuccessfulString;
    @BindString(R.string.please_rate) String ratingMessage;
    @BindString(R.string.cannot_submit_empty_form) String cannotSubmitEmptyForm;
    @BindString(R.string.duration_in_minutes) String durationString;
    @BindString(R.string.rating) String ratingString;
    @BindString(R.string.total_orgasms) String numberOfOrgasmsString;
    @BindString(R.string.please_select_partner) String selectPartner;

    private List<Partner> allPartners;

    public DataInputFragment() {}

    public static DataInputFragment newInstance(Entry entry) {
        DataInputFragment fragment = new DataInputFragment();
        Bundle args = new Bundle();
        args.putParcelable(Entry.ENTRY_PARCELABLE_TAG, entry);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Entry temp = getArguments().getParcelable(Entry.ENTRY_PARCELABLE_TAG);
            setEntry(temp);
        }else{
            //TODO: error here
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar_data_input, container, false);
        unbinder = ButterKnife.bind(this, view);

        MainActivity.adFactory.bindBannerAd(getContext(), bannerAdContainer);
        MainActivity.analyticsService.logInfo(TAG);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
        fillFormByEntry();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void initialise(){
        RoomDataAccessUtil.initialise(getContext());

        submitButton.setOnClickListener(view->{submitForm();});
        cancelButton.setOnClickListener(view-> {
            listener.onExit();
        });
        deleteEntryButton.setOnClickListener(view-> {
            RoomDataAccessUtil.deleteEntry(
                    new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Boolean res) {
                            // res is always true for delete operation
                            Toast.makeText(getContext(), entryDeletedMessage, Toast.LENGTH_SHORT).show();

                            if (listener != null) {
                                listener.onUpdate();
                                listener.onExit();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    }
                    , entry);
        });

        deleteEntryButton.setEnabled(!isNewEntry);

        if(isNewEntry){
            submitResultPositiveMessage = entryAddedMessage + ": " + successfulString;
            submitResultNegativeMessage = entryAddedMessage + ": " + unsuccessfulString;
        }else{
            submitResultPositiveMessage = entryUpdatedMessage + ": " + successfulString;
            submitResultNegativeMessage = entryUpdatedMessage + ": " + unsuccessfulString;
        }

        sexTypeCheckBoxList = new ArrayList<>();
        LinearLayout.LayoutParams sexTypeCheckBoxLayoutParams
                = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        sexTypeCheckBoxLayoutParams.setMargins(0
                ,0
                ,(int)getResources().getDimension(R.dimen.common_large_margin_views)
                ,0);

        List<String> sexTypeList = new ArrayList<String>(Arrays.asList(
                getContext().getResources().getStringArray(R.array.sex_types)
        ));

        for(String sexType : sexTypeList){
            CustomCheckBoxedTextView sexTypeView = new CustomCheckBoxedTextView(getContext());
            sexTypeView.setLayoutParams(sexTypeCheckBoxLayoutParams);

            sexTypeView.setText(sexType);

            sexTypeCheckBoxList.add(sexTypeView);

            sexTypeContainer.addView(sexTypeView);
        }
    }

    private void fillFormByEntry(){
        ArrayList<String> ratingNumbersArray = new ArrayList<>();
        ratingNumbersArray.add(ratingMessage);
        ratingNumbersArray.add("1");
        ratingNumbersArray.add("2");
        ratingNumbersArray.add("3");
        ratingNumbersArray.add("4");
        ratingNumbersArray.add("5");
        ArrayAdapter<String> adapterRating = SpinnerAdapterFactory.getCommonSpinnerArrayAdapter(getContext(), ratingNumbersArray);
        rating.setAdapter(adapterRating);
        int userRating = entry.getRating();
        rating.setSelection(userRating>(ratingNumbersArray.size()-1)?0:userRating);

        ArrayList<String> safetyStatusArray = new ArrayList<>();
        safetyStatusArray.add(simpleNo);
        safetyStatusArray.add(simpleYes);
        ArrayAdapter<String> adapterProtection = SpinnerAdapterFactory.getCommonSpinnerArrayAdapter(getContext(), safetyStatusArray);
        protection.setAdapter(adapterProtection);
        protection.setSelection(entry.isSafe()?1:0);

        ArrayList<String> partnersArray = new ArrayList<>();
        partnersArray.add(selectPartner);
        partner.setEnabled(false);
        RoomDataAccessUtil.getAllPartners(new Observer<List<Partner>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Partner> partners) {
                allPartners = partners;

                int indexOfAlreadyAttachedPartner = 0;

                for(int i = 0; i < partners.size(); i++) {
                    partnersArray.add(partners.get(i).getName());

                    //TODO: here is designed as an entry has only 1 partner, there may be a need for extension
                    if(!entry.getEntryPartners().isEmpty() && partners.get(i).getName().compareTo(entry.getEntryPartners().get(0).getName()) == 0)
                        indexOfAlreadyAttachedPartner = i + 1; // partners start from 1, not 0
                }

                partner.setEnabled(true);

                ArrayAdapter<String> adapterPartner = SpinnerAdapterFactory.getCommonSpinnerArrayAdapter(getContext(), partnersArray);
                partner.setAdapter(adapterPartner);
                partner.setSelection(indexOfAlreadyAttachedPartner);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

        RoomDataAccessUtil.getAllSexTypes(new Observer<List<EntryDefs.SEX_TYPE>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<EntryDefs.SEX_TYPE> sexTypes) {
                for (EntryDefs.SEX_TYPE type : sexTypes)
                    sexTypeCheckBoxList.get(type.getValue()).setChecked(true);
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {

            }
        }, entry.getEntryId());

        if(isNewEntry)
            sexTypeCheckBoxList.get(0).setChecked(true);

        String durationStr = String.valueOf(entry.getDuration());
        if(durationStr.isEmpty() || durationStr.compareTo("0")!=0)
            duration.setText(durationStr, TextView.BufferType.EDITABLE);

        String totalOrgStr = String.valueOf(entry.getTotalOrg());
        if(totalOrgStr.isEmpty() || totalOrgStr.compareTo("0")!=0)
            totalOrg.setText(totalOrgStr, TextView.BufferType.EDITABLE);

        notes.setText(String.valueOf(entry.getNote()), TextView.BufferType.EDITABLE);
    }

    private void submitForm() {
        int ratingValue = rating.getSelectedItemPosition();
        int durationValue = duration.getText().toString().isEmpty() ? 0 : Integer.valueOf(duration.getText().toString());
        int totalOrgValue = totalOrg.getText().toString().isEmpty() ? 0 : Integer.valueOf(totalOrg.getText().toString());
        boolean protectionValue = protection.getSelectedItemPosition() != 0;
        String noteString = notes.getText().toString();
        int partnerPos = partner.getSelectedItemPosition();

        List<EntryDefs.SEX_TYPE> sexTypes = new ArrayList<>();
        for(int i = 0; i < sexTypeCheckBoxList.size(); i++) {
            if(sexTypeCheckBoxList.get(i).getChecked())
                sexTypes.add(EntryDefs.SEX_TYPE.fromValue(i));
        }

        // Validation logic to fill at least 1 field
        if (ratingValue != 0
                || durationValue != 0
                || totalOrgValue != 0
                || !noteString.isEmpty()
                || partnerPos != 0) {
            entry.setRating(ratingValue);
            entry.setNote(noteString);
            entry.setDuration(durationValue);
            entry.setTotalOrg(totalOrgValue);
            entry.setIsSafe(protectionValue);
            entry.setEntrySexTypes(sexTypes);

            //TODO: here is designed as an entry has only 1 partner, there may be a need for extension
            if(partnerPos != 0 && allPartners != null) {
                ArrayList<Partner> selectedPartners = new ArrayList<>();
                selectedPartners.add(allPartners.get(partnerPos - 1));
                entry.setEntryPartners(selectedPartners);
            } else {
                // empty the partner list
                entry.setEntryPartners(new ArrayList<>());
            }

            RoomDataAccessUtil.insertEntry(
                    new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Boolean result) {
                            //TODO: if result negative, action
                            String toastMessage = "";
                            if (result) {
                                toastMessage = submitResultPositiveMessage;

                                if (listener != null)
                                    listener.onUpdate();
                            } else
                                toastMessage = submitResultNegativeMessage;

                            Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT).show();

                            if (listener != null)
                                listener.onExit();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "Entry cannot be inserted, error: " + e.getMessage());
                            Toast.makeText(getContext(), getString(R.string.unknown_error_try_later), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onComplete() {

                        }
                    }
                    , entry);
        } else {
            Toast.makeText(getContext(), cannotSubmitEmptyForm, Toast.LENGTH_LONG).show();
        }
    }

    public void setFragmentActionListener(FragmentActionListener listener){
        this.listener = listener;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if(isVisible()) {
            int ratingValue = rating.getSelectedItemPosition();
            int durationValue = duration.getText().toString().isEmpty() ? 0 : Integer.valueOf(duration.getText().toString());
            int totalOrgValue = totalOrg.getText().toString().isEmpty() ? 0 : Integer.valueOf(totalOrg.getText().toString());
            boolean protectionValue = protection.getSelectedItemPosition() != 0;
            String noteString = notes.getText().toString();

            entry.setRating(ratingValue);
            entry.setNote(noteString);
            entry.setDuration(durationValue);
            entry.setTotalOrg(totalOrgValue);
            entry.setIsSafe(protectionValue);

            outState.putParcelable(Entry.ENTRY_PARCELABLE_TAG, entry);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if(savedInstanceState != null){
            Entry entry = savedInstanceState.getParcelable(Entry.ENTRY_PARCELABLE_TAG);
            if(entry != null)
                setEntry(entry);
        }
}

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void setEntry(Entry entry){
        if(entry == null)
            entry = new Entry();

        isNewEntry = (entry.getEntryId() == 0);

        this.entry = entry;
    }


}
