package centertable.advancedscalendar.modules.calendar;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.analytics.AnalyticsService;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionGenerator;
import centertable.advancedscalendar.common.android_component.fragment.FragmentActionListener;
import centertable.advancedscalendar.common.android_component.view.recycler_view.CommonRecyclerViewListener;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.modules.MainActivity;
import centertable.advancedscalendar.modules.calendar.recycler.DayEntryRecyclerAdapter;
import centertable.advancedscalendar.util.calendar.MaterialCalendarDecorator;
import centertable.advancedscalendar.util.data.data_access.RoomDataAccessUtil;
import centertable.advancedscalendar.util.time.TimeUtil;
import io.reactivex.disposables.Disposable;

public class CalendarFragment extends Fragment implements CommonRecyclerViewListener, FragmentActionGenerator<Entry> {
    public static final String TAG = "calendar_fragment";

    private final static String ENTRY_RECYCLER_POSITION = "entry_recycler_position";
    private final static String LAST_SELECTED_DATE = "last_selected_day";
    private View view;

    @BindView(R.id.calendar_view)
    MaterialCalendarView calendarView;
    @BindView(R.id.day_entry_recycler_view_container)
    RecyclerView recyclerView;
    @BindView(R.id.add_fab)
    FloatingActionButton fab_add;

    private List<Entry> allEntries, selectedDayEntries;

    String lastSelectedDate = "";

    private DayEntryRecyclerAdapter recyclerAdapter;
    private FragmentActionListener<Entry> listener;

    public CalendarFragment() {

    }

    public static CalendarFragment newInstance() {
        CalendarFragment fragment = new CalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            lastSelectedDate = savedInstanceState.getString(LAST_SELECTED_DATE);
        }else{
            if(lastSelectedDate == null || lastSelectedDate.isEmpty()) {
                lastSelectedDate = TimeUtil.getDbDate(TimeUtil.getToday());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_calendar, container, false);

        ButterKnife.bind(this, view);

        MainActivity.analyticsService.logInfo(TAG);

        initialise();

        if(savedInstanceState != null){
            int firstVisibleItem = savedInstanceState.getInt(ENTRY_RECYCLER_POSITION);
            setScrollToPosition(firstVisibleItem);
        }

        return view;
    }

    private void initialise(){
        setCalendarCallbacks();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fab_add.setOnClickListener(view-> {
            startDataInput(new Entry());
        });

        calendarView.setDateSelected(TimeUtil.getDateFromDbDate(lastSelectedDate), true);

        RoomDataAccessUtil.initialise(getContext());

        getAllEntriesFromDatabase();
    }

    private void setCalendarCallbacks(){
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                lastSelectedDate = TimeUtil.getDbDate(date);
                updateEntryListForDate(lastSelectedDate);
            }
        });
    }

    private void getAllEntriesFromDatabase(){
        //TODO: loading image may be good here
        RoomDataAccessUtil.getAllEntries(new io.reactivex.Observer<List<Entry>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Entry> entries) {
                refreshEntryList(entries);
                updateDateNotEmptyIndicator();
                updateEntryListForDate(lastSelectedDate);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void refreshEntryList(List<Entry> entries){
        allEntries = entries;
    }

    private void updateDateNotEmptyIndicator(){
        Collection<CalendarDay> dates = new HashSet<>();

        for(Entry entry:allEntries){
            dates.add(TimeUtil.getCalendarDayFromDbDate(entry.getDateString()));
        }

        //TODO: after the first update for all dates, only updated dates should be changed instead of all
        calendarView.removeDecorators();

        //TODO: make this theme part in a util class
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getContext().getTheme();
        theme.resolveAttribute(R.attr.themeColorAccentReverse, typedValue, true);

        calendarView.addDecorator(new MaterialCalendarDecorator(ContextCompat.getColor(getContext(), typedValue.resourceId), dates));
    }

    private void updateEntryListForDate(String dbDate) {
        selectedDayEntries = getEntryListByDate(dbDate);
        updateDayEntries(selectedDayEntries, false);
    }

    private List<Entry> getEntryListByDate(String dbDate){
        List<Entry> newList = new ArrayList<>();

        if(allEntries != null) {
            for (Entry entry : allEntries) {
                if(entry.getDateString().compareTo(dbDate) == 0){
                    newList.add(entry);
                }
            }
        }

        return newList;
    }

    private void updateDayEntries(List<Entry> entries, boolean extend){
        if(recyclerAdapter == null) {
            recyclerAdapter = new DayEntryRecyclerAdapter(this,
                    entries);
        }

        if(recyclerView.getAdapter() == null)
            recyclerView.setAdapter(recyclerAdapter);

        if(extend)
            recyclerAdapter.extendEntryList(entries);
        else
            recyclerAdapter.setEntryList(entries);

        recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int itemNo) {
        startDataInput(selectedDayEntries.get(itemNo));
    }

    private void startDataInput(Entry entry){
        entry.setDateString(lastSelectedDate);

        if(listener!=null)
            listener.onAction(entry);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int firstVisibleItem = getFirstVisibleItemOfRecyclerView();
        outState.putInt(ENTRY_RECYCLER_POSITION, firstVisibleItem);
        outState.putString(LAST_SELECTED_DATE, lastSelectedDate);
    }

    private int getFirstVisibleItemOfRecyclerView(){
        int position = 0;

        try{
            position = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        }catch (NullPointerException ex){
            Log.e(getClass().getName(), "Layout manager is not found");
        }catch (Exception ex){
            Log.e(getClass().getName(), "Exception: " + ex.toString());
        }
        return position;
    }

    private void setScrollToPosition(int position){
        if(position <= 0)
            position = 0;

        recyclerView.scrollToPosition(position);
    }

    public void refreshCalendar(){
        getAllEntriesFromDatabase();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void setFragmentActionListener(FragmentActionListener<Entry> listener) {
        this.listener = listener;
    }
}
