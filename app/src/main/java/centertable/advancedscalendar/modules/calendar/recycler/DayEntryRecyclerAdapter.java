package centertable.advancedscalendar.modules.calendar.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.android_component.view.recycler_view.CommonRecyclerViewListener;
import centertable.advancedscalendar.data.pojo.Entry;

public class DayEntryRecyclerAdapter extends RecyclerView.Adapter<DayEntryRecyclerAdapter.DayEntryHolder>{
    private static final String TAG = "DayEntryRecyclerAdapter";

    private List<Entry> entryList;
    private CommonRecyclerViewListener commonRecyclerViewListener;

    public DayEntryRecyclerAdapter(CommonRecyclerViewListener listener, List<Entry> entries){
        entryList = entries;
        commonRecyclerViewListener = listener;
    }

    @NonNull
    @Override
    public DayEntryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_item_entry, viewGroup, false);

        return new DayEntryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DayEntryHolder dayEntryHolder, int i) {
        dayEntryHolder.bindHolder(entryList.get(i));
    }

    @Override
    public int getItemCount() {
        return entryList == null ? 0 : entryList.size();
    }

    public void setEntryList(List<Entry> list){
        entryList = list;
    }

    public void extendEntryList(List<Entry> list){
        if(entryList != null){
            entryList.addAll(list);
        }
    }

    class DayEntryHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.recycler_entry_no) TextView entryNo;
        @BindView(R.id.recycler_entry_rating) TextView entryRating;
        @BindView(R.id.recycler_entry_note) TextView entryNote;

        @BindString(R.string.entry_recycler_entry_no_description) String entryNoDescriptionString;
        @BindString(R.string.entry_recycler_entry_rating_description) String entryRatingDescriptionString;
        @BindString(R.string.entry_recycler_entry_note_description) String entryNoteDescriptionString;

        DayEntryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(commonRecyclerViewListener != null)
                        commonRecyclerViewListener.onItemClick(getAdapterPosition());
                    else
                        Log.d(TAG, "commonRecyclerViewListener is null");
                }
            });
        }

        void bindHolder(Entry entry){
            setEntryNoString(getAdapterPosition() + 1);
            setEntryRatingString(entry.getRating());
            setEntryNoteString(entry.getNote());
        }

        private void setEntryNoString(int no){
            String entryNoText = entryNoDescriptionString + ": " + String.valueOf(no);
            entryNo.setText(entryNoText);
        }

        private void setEntryRatingString(int rating){
            if(rating != 0) {
                String entryNoText = entryRatingDescriptionString + ": " + String.valueOf(rating);
                entryRating.setText(entryNoText);
            } else {
                entryRating.setText("");
            }
        }

        private void setEntryNoteString(String note){
            if(!note.isEmpty()) {
                String entryNoteText = entryNoteDescriptionString + ": " + note;
                entryNote.setText(entryNoteText);
            }else{
                entryNote.setText("");
            }
        }
    }

}
