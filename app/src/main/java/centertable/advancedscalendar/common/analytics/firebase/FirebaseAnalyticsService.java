package centertable.advancedscalendar.common.analytics.firebase;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import centertable.advancedscalendar.common.analytics.AnalyticsService;

public class FirebaseAnalyticsService implements AnalyticsService{
    private FirebaseAnalytics firebaseAnalytics;

    public FirebaseAnalyticsService(Context context){
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void logEvent(String tag, Bundle params) {
        if(firebaseAnalytics != null)
            firebaseAnalytics.logEvent(tag, params);
    }

    @Override
    public void logEvent(String tag, String body) {
        Bundle bundle = new Bundle();
        bundle.putString(tag, body);

        if(firebaseAnalytics != null)
            firebaseAnalytics.logEvent(tag, bundle);
    }

    @Override
    public void logInfo(String body) {
        this.logEvent(AnalyticsService.INFO, body);
    }

    @Override
    public void logWarning(String body) {
        this.logEvent(AnalyticsService.WARNING, body);
    }

    @Override
    public void logError(String body) {
        this.logEvent(AnalyticsService.ERROR, body);
    }
}
