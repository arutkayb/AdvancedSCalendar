package centertable.advancedscalendar.common.analytics;

import android.content.Context;

import centertable.advancedscalendar.common.analytics.firebase.FirebaseAnalyticsService;

public class AnalyticsFactory {
    private AnalyticsFactory(){

    }

    public static AnalyticsService getAnalyticsService(Context context, String serviceProvider){
        AnalyticsService service = null;

        if(serviceProvider.compareToIgnoreCase("firebase") == 0){
            service = new FirebaseAnalyticsService(context);
        }
        //If needed, add other providers here

        return service;
    }
}
