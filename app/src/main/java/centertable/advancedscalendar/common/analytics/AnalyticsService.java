package centertable.advancedscalendar.common.analytics;

import android.os.Bundle;

public interface AnalyticsService {
    String INFO = "info";
    String WARNING = "warning";
    String ERROR = "error";

    void logEvent(String tag, Bundle params);
    void logEvent(String tag, String body);

    void logInfo(String body);
    void logWarning(String body);
    void logError(String body);
}
