package centertable.advancedscalendar.common.android_component.view.recycler_view;

public interface CommonRecyclerViewListener {
    void onItemClick(int itemNo);
}
