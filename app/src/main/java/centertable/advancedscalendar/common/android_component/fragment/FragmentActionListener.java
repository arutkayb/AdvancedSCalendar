package centertable.advancedscalendar.common.android_component.fragment;

import centertable.advancedscalendar.common.android_component.fragment.exception.CannotGotoException;

public abstract class FragmentActionListener <PARAMETER_TYPE> {
    public enum GOTO{
        SHOP, CALENDAR;
    }

    public void onAction(PARAMETER_TYPE param){}
    public void onGoto(GOTO param) throws CannotGotoException {}
    public void onExit(){}
    public void onUpdate(){}
}
