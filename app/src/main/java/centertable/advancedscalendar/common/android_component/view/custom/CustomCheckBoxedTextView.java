package centertable.advancedscalendar.common.android_component.view.custom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.resources.TextAppearance;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.shop.ShoppingCartItem;


public class CustomCheckBoxedTextView extends LinearLayout {
    private TextView text;
    private CheckBox checkBox;
    private LinearLayout root;

    private ShoppingCartItem item;

    public CustomCheckBoxedTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    public CustomCheckBoxedTextView(@NonNull Context context) {
        super(context);
        inflateView(context);
    }

    public CustomCheckBoxedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    private void inflateView(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.custom_checkboxed_text, this, true);

        text = view.findViewById(R.id.text);
        checkBox = view.findViewById(R.id.checkbox);
        root = view.findViewById(R.id.root);

        root.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkBox.setChecked(!checkBox.isChecked());
            }
        });
    }

    public void setChecked(boolean isChecked){
        checkBox.setChecked(isChecked);
    }

    public boolean getChecked(){
        return checkBox.isChecked();
    }

    public void setText(String textStr){
        text.setText(textStr);
    }

    public String getText(){
        return text.getText().toString();
    }

    public void setTextAppearance(Context context, int appearance){
        text.setTextAppearance(context, appearance);
    }

    @Override
    public void setLayoutParams(ViewGroup.LayoutParams params) {
        super.setLayoutParams(params);
    }
}
