package centertable.advancedscalendar.common.android_component.view.spinner;


import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import centertable.advancedscalendar.R;

public class SpinnerAdapterFactory {
    private SpinnerAdapterFactory(){ }

    public static <T> ArrayAdapter<T> getCommonSpinnerArrayAdapter(Context context, ArrayList<T> items){
        ArrayAdapter<T> adapter = new ArrayAdapter<T>(context, R.layout.spinner_item_simple, items);
        adapter.setDropDownViewResource(R.layout.spinner_item_simple_dropdown);

        return adapter;
    }
}
