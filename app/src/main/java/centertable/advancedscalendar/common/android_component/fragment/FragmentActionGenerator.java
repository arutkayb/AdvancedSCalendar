package centertable.advancedscalendar.common.android_component.fragment;

public interface FragmentActionGenerator<PARAMETER_TYPE> {
    void setFragmentActionListener(FragmentActionListener<PARAMETER_TYPE> listener);
}
