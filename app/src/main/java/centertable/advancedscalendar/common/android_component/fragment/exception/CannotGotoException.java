package centertable.advancedscalendar.common.android_component.fragment.exception;

public class CannotGotoException extends Exception{
    @Override
    public String toString() {
        return "Cannot find the item to go";
    }
}
