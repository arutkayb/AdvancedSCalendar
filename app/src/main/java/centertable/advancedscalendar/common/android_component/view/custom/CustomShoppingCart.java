package centertable.advancedscalendar.common.android_component.view.custom;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.shop.ShoppingCartItem;


public class CustomShoppingCart extends CardView {
    private TextView title;
    private TextView description;
    private TextView price;
    private ImageView image;

    private ShoppingCartItem item;

    public CustomShoppingCart(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    public CustomShoppingCart(@NonNull Context context) {
        super(context);
        inflateView(context);
    }

    public CustomShoppingCart(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    private void inflateView(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.custom_shopping_cart, this, true);

        title = view.findViewById(R.id.shopping_cart_title);
        description = view.findViewById(R.id.shopping_cart_description);
        price = view.findViewById(R.id.shopping_cart_price);
        image = view.findViewById(R.id.shopping_cart_image);
    }
    
    public void setCartItem(ShoppingCartItem cartItem){
        item = cartItem;

        String titleStr = "";
        if(!(titleStr = cartItem.getTitle()).isEmpty())
            title.setText(titleStr);

        String descriptionStr = "";
        if(!(descriptionStr = cartItem.getDescription()).isEmpty())
            description.setText(descriptionStr);

        String priceStr = "";
        if(!(priceStr = cartItem.getPrice()).isEmpty())
            price.setText(priceStr);

        Drawable imageDrawable = null;
        if((imageDrawable = cartItem.getImage()) != null)
            image.setImageDrawable(imageDrawable);
    }

    public ShoppingCartItem getCartItem() {
        return item;
    }

    public String getTitle() {
        return title.getText().toString();
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public String getDescription() {
        return description.getText().toString();
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    public String getPrice() {
        return price.getText().toString();
    }

    public void setPrice(String price) {
        this.price.setText(price);
    }

    public Drawable getImage() {
        return image.getDrawable();
    }

    public void setImage(Drawable image) {
        this.image.setImageDrawable(image);
    }
}
