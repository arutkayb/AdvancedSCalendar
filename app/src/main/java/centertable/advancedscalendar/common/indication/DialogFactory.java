package centertable.advancedscalendar.common.indication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

public class DialogFactory {
    private static AlertDialog.Builder getDialog(Context context
            ,@NonNull DialogInterface.OnClickListener dialogClickListener
            ,DialogInterface.OnCancelListener dialogCancelListener
            ,@NonNull String title
            ,@NonNull String message
            ,@NonNull String positiveButton
            ,@NonNull String negativeButton
            ,@NonNull String neutralButton){
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialogTheme));
        builder.setMessage(message);

        builder.setTitle(title);

        builder.setOnCancelListener(dialogCancelListener);

        if(!positiveButton.isEmpty())
            builder.setPositiveButton(positiveButton, dialogClickListener);

        if(!negativeButton.isEmpty())
            builder.setNegativeButton(negativeButton, dialogClickListener);

        if(!neutralButton.isEmpty())
            builder.setNeutralButton(neutralButton, dialogClickListener);

        return builder;
    }

    public static void getAreYouSureDialog(Context context, DialogInterface.OnClickListener dialogClickListener){
        getDialog(
                context
                , dialogClickListener
                , null
                ,""
                , context.getString(R.string.are_you_sure)
                , context.getString(R.string.yes)
                , context.getString(R.string.no)
                , "")
                .show();
    }

    public static void getCustomAreYouSureDialog(Context context, String message, DialogInterface.OnClickListener dialogClickListener){
        getDialog(
                context
                , dialogClickListener
                , null
                ,context.getString(R.string.are_you_sure)
                , message
                , context.getString(R.string.yes)
                , context.getString(R.string.no)
                , "")
                .show();
    }

    public static void getCheckConnectionDialog(Context context, DialogInterface.OnClickListener onClickListener, DialogInterface.OnCancelListener onCancelListener){
        getDialog(
                context
                , onClickListener
                , onCancelListener
                ,""
                , context.getString(R.string.check_your_connection)
                , context.getString(R.string.ok)
                , ""
                , "")
                .show();
    }

    public static void getSetupPinDialog(Context context, DialogInterface.OnClickListener dialogClickListener, DialogInterface.OnCancelListener onCancelListener, @NonNull final Bundle bundle){
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.custom_dialog_setup_pin,null);

        EditText primary = viewInflated.findViewById(R.id.dialog_pin);
        EditText secondary = viewInflated.findViewById(R.id.dialog_pin_secondary);

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                bundle.putString(AuthenticationInfo.USER_PIN_PRIMARY,primary.getText().toString());
                bundle.putString(AuthenticationInfo.USER_PIN_SECONDARY,secondary.getText().toString());

                dialogClickListener.onClick(dialogInterface, i);
            }
        };

        AlertDialog.Builder builder = getDialog(
                context
                , onClickListener
                , onCancelListener
                ,context.getString(R.string.setup_pin_title)
                , context.getString(R.string.setup_pin_message)
                , context.getString(R.string.ok)
                , context.getString(R.string.cancel)
                , "");

        builder.setView(viewInflated);

        builder.show();
    }

    public static void getRemovePinDialog(Context context, DialogInterface.OnClickListener dialogClickListener, DialogInterface.OnCancelListener onCancelListener, Bundle bundle){
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.custom_dialog_ask_pin,null);

        EditText pin = viewInflated.findViewById(R.id.dialog_pin);

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                bundle.putString(AuthenticationInfo.USER_PIN,pin.getText().toString());

                dialogClickListener.onClick(dialogInterface, i);
            }
        };

        AlertDialog.Builder builder = getDialog(
                context
                , onClickListener
                , onCancelListener
                ,context.getString(R.string.remove_pin_title)
                , context.getString(R.string.remove_pin_message)
                , context.getString(R.string.ok)
                , context.getString(R.string.cancel)
                , "");

        builder.setView(viewInflated);

        builder.show();
    }

    public static void getAskPinDialog(Context context, DialogInterface.OnClickListener dialogClickListener, DialogInterface.OnCancelListener onCancelListener, Bundle bundle){
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.custom_dialog_ask_pin,null);

        EditText pin = viewInflated.findViewById(R.id.dialog_pin);

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                bundle.putString(AuthenticationInfo.USER_PIN,pin.getText().toString());

                dialogClickListener.onClick(dialogInterface, i);
            }
        };

        AlertDialog.Builder builder = getDialog(
                context
                , onClickListener
                , onCancelListener
                , context.getString(R.string.ask_pin_title)
                , context.getString(R.string.ask_pin_message)
                , context.getString(R.string.ok)
                , context.getString(R.string.cancel)
                , context.getString(R.string.sign_out));

        builder.setView(viewInflated);

        builder.show();
    }

    public static void getNotAuthorizedToAddPartnerDialog(Context context, DialogInterface.OnClickListener onClickListener){
        getDialog(
                context
                , onClickListener
                , null
                ,context.getString(R.string.partners_limit_reached)
                , context.getString(R.string.partners_limit_reached_description)
                , context.getString(R.string.yes)
                , context.getString(R.string.no)
                , "")
                .show();
    }

    public static void getNotAuthorizedToUseLockFeatureDialog(Context context, DialogInterface.OnClickListener onClickListener){
        getDialog(
                context
                , onClickListener
                , null
                ,context.getString(R.string.cannot_use_lock_feature)
                , context.getString(R.string.cannot_use_lock_feature_description)
                , context.getString(R.string.yes)
                , context.getString(R.string.no)
                , "")
                .show();
    }
}
