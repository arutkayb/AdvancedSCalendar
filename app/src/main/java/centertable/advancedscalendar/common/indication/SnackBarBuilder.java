package centertable.advancedscalendar.common.indication;

import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

public class SnackBarBuilder {
    public static final int INDEFINITE = Snackbar.LENGTH_INDEFINITE;
    public static final int LONG = Snackbar.LENGTH_LONG;
    public static final int SHORT = Snackbar.LENGTH_SHORT;

    private Snackbar snackBar;

    public SnackBarBuilder(CoordinatorLayout rootLayout){
        snackBar = Snackbar.make(rootLayout, "", Snackbar.LENGTH_SHORT);
        snackBar.setActionTextColor(Color.WHITE);
    }

    public Snackbar setMessage(String message){
        snackBar.setText(message);
        return snackBar;
    }

    public Snackbar setDuration(int duration){
        if(duration != Snackbar.LENGTH_INDEFINITE && duration != Snackbar.LENGTH_LONG && duration != Snackbar.LENGTH_SHORT)
            duration = Snackbar.LENGTH_SHORT;

        snackBar.setDuration(duration);

        return snackBar;
    }

    public Snackbar setAction(String action, View.OnClickListener listener){
        snackBar.setAction(action, listener);
        return snackBar;
    }

    public void show(){
        snackBar.show();
    }

}
