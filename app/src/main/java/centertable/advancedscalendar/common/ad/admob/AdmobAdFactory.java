package centertable.advancedscalendar.common.ad.admob;

import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.common.ad.AdFactory;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;
import centertable.advancedscalendar.modules.login.AuthorizationInfo;

public class AdmobAdFactory implements AdFactory {
    private static AuthorizationInfo authorizationInfo;

    private InterstitialAd interstitialAd;

    private static AdmobAdFactory admobAdFactory;
    private AdmobAdFactory(){}
    public static AdmobAdFactory getInstance(){
        if(admobAdFactory == null)
            admobAdFactory = new AdmobAdFactory();

        authorizationInfo = AuthenticationInfo.getInstance().getAuthorizationInfo();

        return admobAdFactory;
    }

    private void initialise(Context context){
        String appId = context.getString(R.string.admob_app_id);

        MobileAds.initialize(context, appId);
    }

    @Override
    public void bindBannerAd(Context context, FrameLayout adContainer) {
        if (authorizationInfo != null && !authorizationInfo.isAuthorizedToRemoveAds()) {
            initialise(context);

            AdView bannerAd = new AdView(context);
            bannerAd.setAdSize(AdSize.SMART_BANNER);
            bannerAd.setAdUnitId(context.getString(R.string.admob_common_banner_ad_unit_id));

            adContainer.addView(bannerAd);

            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            bannerAd.loadAd(adRequest);
        }
    }

    @Override
    public void loadFullScreenAd(Context context) {
        if (authorizationInfo != null && !authorizationInfo.isAuthorizedToRemoveAds()) {
            initialise(context);

            interstitialAd = new InterstitialAd(context);
            interstitialAd.setAdUnitId(context.getString(R.string.admob_common_interstitial_ad_unit_id));

            interstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    @Override
    public void showFullScreenAd() {
        if(interstitialAd != null) {
            if (interstitialAd.isLoaded()) {
                interstitialAd.show();
            } else {
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        interstitialAd.show();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.e(getClass().getName(), "Failed to load ad, error code: " + String.valueOf(errorCode));
                    }
                });
            }
        } else {
            Log.e(getClass().getName(), "Please load full screen ad at first");
        }
    }
}
