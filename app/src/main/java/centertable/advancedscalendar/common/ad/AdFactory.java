package centertable.advancedscalendar.common.ad;

import android.content.Context;
import android.widget.FrameLayout;

public interface AdFactory {
    void bindBannerAd(Context context, FrameLayout adContainer);
    void loadFullScreenAd(Context context);
    void showFullScreenAd();
}
