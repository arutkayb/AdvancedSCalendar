package centertable.advancedscalendar.common.shop;

import android.graphics.drawable.Drawable;

public class ShoppingCartItem {
    private String title;
    private String description;
    private String price;
    private String productId;
    private Drawable image = null;

    public ShoppingCartItem() {}

    public ShoppingCartItem(String title, String description, String price) {
        this.title = title;
        this.description = description;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
