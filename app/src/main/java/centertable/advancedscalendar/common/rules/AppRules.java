package centertable.advancedscalendar.common.rules;

public class AppRules {
    public static final int PIN_LENGTH = 4;
    public static final int NUMBER_OF_PARTNERS = 1;

    private AppRules() {}
}
