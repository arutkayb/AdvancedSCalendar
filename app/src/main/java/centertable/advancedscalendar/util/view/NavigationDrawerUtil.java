package centertable.advancedscalendar.util.view;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.WindowManager;

public class NavigationDrawerUtil {
    private NavigationDrawerUtil(){}

    public static int getHeaderHeight(Context context){
        CommonViewUtil.ScreenSize size = CommonViewUtil.getScreenSize(context);
        return (int)(getNavigationDrawerWidth(context)*(9d/16d));
    }

    public static int getNavigationDrawerWidth(Context context){
        CommonViewUtil.ScreenSize size = CommonViewUtil.getScreenSize(context);

        return size.width - CommonViewUtil.dpToPx(context, 56);
    }
}
