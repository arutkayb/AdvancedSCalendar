package centertable.advancedscalendar.util.view;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class ActionBarUtil {
    private static final String TAG = "ActionBarUtil";

    public static void setSupportActionBarTitle(AppCompatActivity activity, String title){
        try{
            activity.getSupportActionBar().setTitle(title);
        } catch (NullPointerException ex){
            Log.d(TAG, "Cannot set ActionBar title, ex: " + ex.toString());
        }
    }
}
