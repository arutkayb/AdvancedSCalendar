package centertable.advancedscalendar.util.view;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class CommonViewUtil {
    public static ScreenSize getScreenSize(Context context){
        ScreenSize ret = new ScreenSize();

        WindowManager windowManager = ((AppCompatActivity)context).getWindowManager();
        if(windowManager != null){
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            ret.width = size.x;
            ret.height = size.y;
        }

        return ret;
    }

    public static class ScreenSize{
        public int width = 0;
        public int height = 0;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));

        return px;
    }
}
