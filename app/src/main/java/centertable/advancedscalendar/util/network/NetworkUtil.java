package centertable.advancedscalendar.util.network;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkUtil {
    private NetworkUtil(){}

    public static boolean isNetworkAvailable(Context context) {
        boolean res = false;

        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));

        if(connectivityManager != null)
            res = connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();

        return res;
    }
}
