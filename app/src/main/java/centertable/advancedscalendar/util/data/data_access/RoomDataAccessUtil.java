package centertable.advancedscalendar.util.data.data_access;

import android.arch.persistence.room.Room;
import android.content.Context;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import centertable.advancedscalendar.data.definitions.EntryDefs;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.data.pojo.EntrySexType;
import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.util.time.TimeUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public final class RoomDataAccessUtil{
    private static EntryAccessUtil entryAccessUtil;
    private static PartnerAccessUtil partnerAccessUtil;
    private static UserAccessUtil userAccessUtil;
    private static AchievementAccessUtil achievementAccessUtil;
    private static NotificationAccessUtil notificationAccessUtil;

    private static boolean isInitialised = false;

    public static EntryAccessUtil getEntryAccessUtil() {
        return entryAccessUtil;
    }

    public static void initialise(Context context){
        if(!isInitialised) {
            ASCDatabase db = Room.databaseBuilder(context.getApplicationContext(),
                    ASCDatabase.class, DatabaseDefs.ASC_DB_NAME)
                    .build();

            entryAccessUtil = new EntryAccessUtil(db);
            partnerAccessUtil = new PartnerAccessUtil(db);
            userAccessUtil = new UserAccessUtil(db);
            achievementAccessUtil = new AchievementAccessUtil(db);
            notificationAccessUtil = new NotificationAccessUtil(db);

            isInitialised = true;
        }
    }

    public static void getEntriesForDate(Observer<List<Entry>> observer, String date) {
        Observable<List<Entry>> observable = Observable.fromCallable(new Callable<List<Entry>>() {
            @Override
            public List<Entry> call() {
                List<Entry> entryList = EntryAccessUtil.getPojoListFromEntityList(
                        entryAccessUtil.getEntriesForDate(date));

                // Get entries with sub entities such as photo, location, partner
                for(Entry entry : entryList){
                    fillSubEntitiesOfEntry(entry);
                }

                return entryList;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void deleteEntry(Observer<Boolean> observer, Entry entry) {
        //Deleting the entryEntity takes care of related database items of entry
        Observable<Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                entryAccessUtil.deleteEntry(entry);
                return true;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void insertEntry(Observer<Boolean> observer, Entry entry) {
        //TODO: insert entries related entities such as photo, location
        Observable<Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                long entryId = entryAccessUtil.insertEntry(entry);
                //TODO: insert entrypartnerentity if there are attached partners
                for(Partner partner:entry.getEntryPartners())
                    entryAccessUtil.attachPartnerToEntry(partner.getPartnerId(), entryId);

                for(EntryDefs.SEX_TYPE sexType : entry.getEntrySexTypes())
                    entryAccessUtil.attachSexTypeToEntry(sexType.getValue(), entryId);

                return entryId!=0;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void getAllEntries(Observer<List<Entry>> observer){
        //TODO: get entries with related entities such as photo, location
        Observable<List<Entry>> observable = Observable.fromCallable(new Callable<List<Entry>>() {
            @Override
            public List<Entry> call() {
                List<Entry> entryList = EntryAccessUtil.getPojoListFromEntityList(
                        entryAccessUtil.getAllEntries());

                // Get entries with sub entities such as photo, location, partner
                for(Entry entry : entryList){
                    fillSubEntitiesOfEntry(entry);
                }

                return entryList;

            }
        });

        subscribeObservable(observer, observable);
    }

    public static void getEntriesForTimeInterval(Observer<List<Entry>> observer, Date dateStart, Date dateEnd){
        Observable<List<Entry>> observable = Observable.fromCallable(new Callable<List<Entry>>() {
            @Override
            public List<Entry> call() {
                List<Entry> entryList = EntryAccessUtil.getPojoListFromEntityList(
                        entryAccessUtil.getEntriesForTimeInterval(TimeUtil.getDbDate(dateStart),TimeUtil.getDbDate(dateEnd)));

                // Get entries with sub entities such as photo, location, partner
                for(Entry entry : entryList){
                    fillSubEntitiesOfEntry(entry);
                }

                return entryList;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void getAllUsers(Observer<List<User>> observer){
        Observable<List<User>> observable = Observable.fromCallable(new Callable<List<User>>() {
            @Override
            public List<User> call() {
                return userAccessUtil.getAllUsers();
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void getUserById(Observer<User> observer, long id){
        Observable <User> observable = Observable.fromCallable(new Callable<User>() {
            @Override
            public User call() {
                return userAccessUtil.getUserById(id);
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void insertUser(Observer<Boolean> observer, User user){
        Observable <Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return userAccessUtil.insertUser(user);
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void updateUser(Observer<Boolean> observer, User user){
        Observable <Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                userAccessUtil.updateUser(user);
                return true;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void deleteUser(Observer<Boolean> observer, User user){
        Observable <Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                userAccessUtil.deleteUser(user);
                return true;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void clearUser(Observer<Boolean> observer, User user){
        Observable <Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                userAccessUtil.deleteUser(user);
                userAccessUtil.insertUser(user);
                return true;
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void getAllPartners(Observer<List<Partner>> observer){
        Observable <List<Partner>> observable = Observable.fromCallable(new Callable<List<Partner>>() {
            @Override
            public List<Partner> call() {
                return PartnerAccessUtil.getPojoListFromEntityList(
                        partnerAccessUtil.getAllPartnersForUser()
                );
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void getAllSexTypes(Observer<List<EntryDefs.SEX_TYPE>> observer, long entryId){
        Observable <List<EntryDefs.SEX_TYPE>> observable = Observable.fromCallable(new Callable<List<EntryDefs.SEX_TYPE>>(){
            @Override
            public List<EntryDefs.SEX_TYPE> call() throws Exception {
                return EntryAccessUtil.getSexTypeListFromEntityList(
                        entryAccessUtil.getAllSexTypesForEntry(entryId)
                );
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void insertPartner (Observer<Boolean> observer, Partner partner){
        Observable <Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return partnerAccessUtil.insertPartner(partner);
            }
        });

        subscribeObservable(observer, observable);
    }

    public static void deletePartner(Observer<Boolean> observer, Partner partner) {
        Observable<Boolean> observable = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                partnerAccessUtil.deletePartner(partner);
                return true;
            }
        });

        subscribeObservable(observer, observable);
    }

    private static <T> void subscribeObservable(Observer<T> observer, Observable<T> observable){
            observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer);
    }

    private static void fillSubEntitiesOfEntry(Entry entry){
        List<Partner> entryPartnerList = PartnerAccessUtil.getPojoListFromEntityList(partnerAccessUtil.getAllPartnersForEntry(entry.getEntryId()));
        entry.setEntryPartners(entryPartnerList);

        List<EntryDefs.SEX_TYPE> entrySexTypeList = EntryAccessUtil.getSexTypeListFromEntityList(entryAccessUtil.getAllSexTypesForEntry(entry.getEntryId()));
        entry.setEntrySexTypes(entrySexTypeList);

        //TODO: get photos and locations, too
    }
}
