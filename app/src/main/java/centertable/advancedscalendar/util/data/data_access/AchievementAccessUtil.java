package centertable.advancedscalendar.util.data.data_access;

import android.arch.persistence.room.RoomDatabase;

import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.AchievementDao;

class AchievementAccessUtil{
    private AchievementDao achievementDao;

    AchievementAccessUtil(ASCDatabase db){
        achievementDao = db.getAchievementDao();
    }
}
