package centertable.advancedscalendar.util.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

public class SharedPreferencesUtil {
    private SharedPreferencesUtil(){};

    public static SharedPreferences.Editor getSharedPrefEditor(Context context, String tag){
        return getSharedPref(context, tag).edit();
    }

    public static SharedPreferences getSharedPref(Context context, String tag){
        return context.getSharedPreferences(tag, Context.MODE_PRIVATE);
    }
}
