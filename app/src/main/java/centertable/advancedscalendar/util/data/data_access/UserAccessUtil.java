package centertable.advancedscalendar.util.data.data_access;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.UserEntity;

class UserAccessUtil{
    private UserDao userDao;

    UserAccessUtil(ASCDatabase db){
        userDao = db.getUserDao();
    }

    List<User> getAllUsers(){
        List<User> userList = new ArrayList<>();

        List<UserEntity> userEntityList = userDao.getAll();

        for(int i = 0; i < userEntityList.size(); i++){
            userList.add(new User(userEntityList.get(i)));
        }

        return userList;
    }

    User getUserById(long id){
        User user = null;
        UserEntity userEntity = userDao.getUserById(id);
        if(userEntity != null)
            user = new User(userEntity);

        return user;
    }

    boolean insertUser(User user){
        return userDao.insert(user.generateEntity())[0] != 0;
    }

    void updateUser(User user) {
        userDao.update(user.generateEntity());
    }

    void deleteUser(User user){
        UserEntity userEntity = user.generateEntity();
        userDao.delete(userEntity);
    }
}
