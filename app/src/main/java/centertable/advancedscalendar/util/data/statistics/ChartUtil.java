package centertable.advancedscalendar.util.data.statistics;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import centertable.advancedscalendar.util.time.TimeUtil;


public class ChartUtil {
    private ChartUtil(){}

    public static ArrayList<BarEntry> getBarChartEntryList(HashMap<Float,Float> hashMap) {
        ArrayList<BarEntry> result = new ArrayList<>();

        for (Map.Entry<Float, Float> entry : hashMap.entrySet()) {
            result.add(new BarEntry(entry.getKey(), entry.getValue()));
        }

        return result;
    }

    public static ArrayList<Entry> getLineChartEntryList(HashMap<Float,Float> hashMap) {
        ArrayList<Entry> result = new ArrayList<>();

        for (Map.Entry<Float, Float> entry : hashMap.entrySet()) {
            result.add(new Entry(entry.getKey(), entry.getValue()));
        }

        return result;
    }
}
