package centertable.advancedscalendar.util.data.data_access;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.data.definitions.EntryDefs;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.EntryDao;
import centertable.advancedscalendar.data.room.dao.EntryPartnerDao;
import centertable.advancedscalendar.data.room.dao.EntrySexTypeDao;
import centertable.advancedscalendar.data.room.entity.EntryEntity;
import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;
import centertable.advancedscalendar.data.room.entity.EntrySexTypeEntity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

public class EntryAccessUtil{
    private EntryDao entryDao;
    private EntryPartnerDao entryPartnerDao;
    private EntrySexTypeDao entrySexTypeDao;

    EntryAccessUtil(ASCDatabase db){
        entryDao = db.getEntryDao();
        entryPartnerDao = db.getEntryPartnerDao();
        entrySexTypeDao = db.getEntrySexTypeDao();
    }

    List<EntryEntity> getEntriesForDate(String date){
        return entryDao.getEntriesForUserForDate(AuthenticationInfo.getInstance().getCurrentUserId(), date);
    }

    List<EntryEntity> getAllEntries(){
        return entryDao.getAllEntriesForUser(AuthenticationInfo.getInstance().getCurrentUserId());
    }

    List<EntryEntity> getEntriesForTimeInterval(String dbDateStart, String dbDateEnd){
        return entryDao.getEntriesForUserForTimeInterval(
                AuthenticationInfo.getInstance().getCurrentUserId(),
                dbDateStart,
                dbDateEnd);
    }

    long insertEntry(Entry entry){
        entry.setUserId(AuthenticationInfo.getInstance().getCurrentUserId());
        EntryEntity entryEntity = entry.generateEntity();
        long resultId = entryDao.insert(entryEntity)[0];
        return resultId;
    }

    void deleteEntry(Entry entry){
        entry.setUserId(AuthenticationInfo.getInstance().getCurrentUserId());
        EntryEntity entryEntity = entry.generateEntity();
        entryDao.delete(entryEntity);
    }

    static List<Entry> getPojoListFromEntityList(List<EntryEntity> list) {
        List<Entry> entryList = new ArrayList<>();

        if(list != null) {
            for (int i = 0; i < list.size(); i++) {
                entryList.add(new Entry(list.get(i)));
            }
        }

        return entryList;
    }

    static List<EntryDefs.SEX_TYPE> getSexTypeListFromEntityList(List<EntrySexTypeEntity> list) {
        List<EntryDefs.SEX_TYPE> sexTypeList = new ArrayList<>();

        if(list != null) {
            for (int i = 0; i < list.size(); i++) {
                sexTypeList.add(EntryDefs.SEX_TYPE.fromValue(list.get(i).sexTypeId));
            }
        }

        return sexTypeList;
    }

    long attachPartnerToEntry(long partnerId, long entryId){
        EntryPartnerEntity entryPartnerEntity = new EntryPartnerEntity(0, partnerId, entryId);
        long resultId = (entryPartnerDao.insert(entryPartnerEntity))[0];

        return resultId;
    }

    long attachSexTypeToEntry(int sexTypeId, long entryId){
        EntrySexTypeEntity entryPartnerEntity = new EntrySexTypeEntity(0, sexTypeId, entryId);
        long resultId = (entrySexTypeDao.insert(entryPartnerEntity))[0];

        return resultId;
    }

    List<EntrySexTypeEntity> getAllSexTypesForEntry(long entryId){
        return entrySexTypeDao.getAllSexTypesForEntry(entryId);
    }
}
