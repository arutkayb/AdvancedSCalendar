package centertable.advancedscalendar.util.data.statistics;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import centertable.advancedscalendar.data.definitions.EntryDefs;
import centertable.advancedscalendar.data.pojo.Entry;
import centertable.advancedscalendar.util.time.TimeUtil;

public class StatisticsUtil {
    private StatisticsUtil(){}

    public static HashMap<Float, Float> getNumberOfEntriesByDateMap(List<Entry> entryList, @Nullable Date startDate, @Nullable Date endDate){
        HashMap<Float, Float> result = new HashMap<Float, Float>();
        Float entryCount = 0f;
        Float time = 0f;
        if(entryList!=null) {
            if(startDate == null)
                startDate = TimeUtil.getDateFromDbDate(entryList.get(0).getDateString());

            if(endDate == null)
                endDate = TimeUtil.getDateFromDbDate(entryList.get(entryList.size() - 1).getDateString());

            fillEmptyValuedDateMapForInterval(result, startDate, endDate);

            for (Entry entry : entryList) {
                Date date = TimeUtil.getDateFromDbDate(entry.getDateString());

                if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0 ) {
                    time = (float) (date.getTime() / TimeUtil.DAY_TO_MILLIS);

                    entryCount = result.get(time);
                    if (entryCount == null)
                        entryCount = 0f;

                    entryCount = entryCount + 1;

                    result.put(time, entryCount);
                }
            }
        }

        return result;
    }

    public static HashMap<Float, Float> getNumberOfOrgasmsByDateMap(List<Entry> entryList, @Nullable Date startDate, @Nullable Date endDate){
        HashMap<Float, Float> result = new HashMap<Float, Float>();
        Float totalOrgasms = 0f;
        Float time = 0f;
        if(entryList!=null) {
            if(startDate == null)
                startDate = TimeUtil.getDateFromDbDate(entryList.get(0).getDateString());

            if(endDate == null)
                endDate = TimeUtil.getDateFromDbDate(entryList.get(entryList.size() - 1).getDateString());

            fillEmptyValuedDateMapForInterval(result, startDate, endDate);

            for (Entry entry : entryList) {
                Date date = TimeUtil.getDateFromDbDate(entry.getDateString());

                if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0 ) {
                    time = (float) (date.getTime() / TimeUtil.DAY_TO_MILLIS);

                    totalOrgasms = result.get(time);
                    if (totalOrgasms == null)
                        totalOrgasms = 0f;

                    totalOrgasms = totalOrgasms + entry.getTotalOrg();

                    result.put(time, totalOrgasms);
                }
            }
        }

        return result;
    }

    public static HashMap<Float, Float> getTotalDurationByDateMap(List<Entry> entryList, @Nullable Date startDate, @Nullable Date endDate){
        HashMap<Float, Float> result = new HashMap<Float, Float>();
        Float totalDuration = 0f;
        Float time = 0f;
        if(entryList!=null) {
            if(startDate == null)
                startDate = TimeUtil.getDateFromDbDate(entryList.get(0).getDateString());

            if(endDate == null)
                endDate = TimeUtil.getDateFromDbDate(entryList.get(entryList.size() - 1).getDateString());

            fillEmptyValuedDateMapForInterval(result, startDate, endDate);

            for (Entry entry : entryList) {
                Date date = TimeUtil.getDateFromDbDate(entry.getDateString());

                if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0 ) {
                    time = (float) (date.getTime() / TimeUtil.DAY_TO_MILLIS);

                    totalDuration = result.get(time);
                    if (totalDuration == null)
                        totalDuration = 0f;

                    totalDuration = totalDuration + entry.getDuration();

                    result.put(time, totalDuration);
                }
            }
        }

        return result;
    }

    private static void fillEmptyValuedDateMapForInterval(HashMap<Float, Float> map, @NonNull Date startDate, @NonNull Date endDate){
        float time = 0f;

        for(Date date : TimeUtil.getDbDateListFromInterval(startDate, endDate)){
            time = (float) (date.getTime() / TimeUtil.DAY_TO_MILLIS);

            map.put(time, 0f);
        }
    }

    public static HashMap<EntryDefs.SEX_TYPE, Float> getTotalNumbersBySexTypeMap(List<Entry> entryList, @Nullable Date startDate, @Nullable Date endDate){
        HashMap<EntryDefs.SEX_TYPE, Float> result = new HashMap<EntryDefs.SEX_TYPE, Float>();

        Float count = 0f;
        EntryDefs.SEX_TYPE sexType = EntryDefs.SEX_TYPE.fromValue(0);

        if(entryList!=null) {
            if(startDate == null)
                startDate = TimeUtil.getDateFromDbDate(entryList.get(0).getDateString());

            if(endDate == null)
                endDate = TimeUtil.getDateFromDbDate(entryList.get(entryList.size() - 1).getDateString());


            for (Entry entry : entryList) {
                Date date = TimeUtil.getDateFromDbDate(entry.getDateString());

                if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0 ) {
                    for(EntryDefs.SEX_TYPE type : entry.getEntrySexTypes()) {
                        count = result.get(type);
                        if (count == null)
                            count = 0f;

                        count = count + 1;

                        result.put(type, count);
                    }
                }
            }
        }

        return result;
    }
}
