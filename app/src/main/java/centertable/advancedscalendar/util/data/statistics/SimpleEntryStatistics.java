package centertable.advancedscalendar.util.data.statistics;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.List;

import centertable.advancedscalendar.data.pojo.Entry;

public class SimpleEntryStatistics implements Parcelable {
    public static final String TAG_STATISTICS_PARCELABLE = "statistics_parcelable";
    /*
    Entry fields:
        duration
        note
        rating
        safetyStatus
        totalOrg

        entryPhoto
        entrySexType
        entryPlace
        entryPartner
     */

    //Non-empty field counts
    private int totalEntryCount = 0;
    private int totalDurationFieldCount = 0;
    private int totalOrgasmsFieldCount = 0;
    private int totalRatingFieldCount = 0;

    //Totals
    private int totalDurationInMinutes = 0;
    private int totalOrgasmCount = 0;
    //TODO: total numbers for each sex type, place, partner

    //Averages
    private double averageRating = 0.0;
    private double averageOrgasmCount = 0.0;
    private double averageDurationInMinutes = 0.0;

    //Max and Min
    private int maximumEntryCountForDate = 0;
    private int maximumOrgasmCountForEntry = 0;
    private int maximumDurationInMinutesForEntry = 0;

    //Last
    //TODO: last times for sex type, places, partners

    //Most and least
    //TODO: most and least for sex type, places, partners

    //Worst and best
    //TODO: List of worst and best entries according to rating


    public int getTotalEntryCount() {
        return totalEntryCount;
    }

    public int getTotalDurationFieldCount() {
        return totalDurationFieldCount;
    }

    public int getTotalOrgasmsFieldCount() {
        return totalOrgasmsFieldCount;
    }

    public int getTotalRatingFieldCount() {
        return totalRatingFieldCount;
    }

    public int getTotalDurationInMinutes() {
        return totalDurationInMinutes;
    }

    public int getTotalOrgasmCount() {
        return totalOrgasmCount;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public double getAverageOrgasmCount() {
        return averageOrgasmCount;
    }

    public double getAverageDurationInMinutes() {
        return averageDurationInMinutes;
    }

    public int getMaximumEntryCountForDate() {
        return maximumEntryCountForDate;
    }

    public int getMaximumOrgasmCountForEntry() {
        return maximumOrgasmCountForEntry;
    }

    public int getMaximumDurationInMinutesForEntry() {
        return maximumDurationInMinutesForEntry;
    }

    public void calculateStatisticsForEntryList(@NonNull List<Entry> entryList){
        totalEntryCount = entryList.size();

        int ratingSum = 0;

        for(Entry entry : entryList){
            if(entry.getRating() != 0) {
                ratingSum = ratingSum + entry.getRating();
                totalRatingFieldCount ++;
            }

            if(entry.getTotalOrg() != 0) {
                totalOrgasmCount = totalOrgasmCount + entry.getTotalOrg();
                totalOrgasmsFieldCount ++;
            }

            if(entry.getDuration() != 0) {
                totalDurationInMinutes = totalDurationInMinutes + entry.getDuration();
                totalDurationFieldCount ++;
            }
        }

        if(totalRatingFieldCount != 0)
            averageRating = (double)ratingSum/totalRatingFieldCount;

        if(totalOrgasmsFieldCount != 0)
            averageOrgasmCount = (double)totalOrgasmCount/totalOrgasmsFieldCount;

        if(totalDurationFieldCount != 0)
            averageDurationInMinutes = (double)totalDurationInMinutes/totalDurationFieldCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.totalEntryCount);
        dest.writeInt(this.totalDurationFieldCount);
        dest.writeInt(this.totalOrgasmsFieldCount);
        dest.writeInt(this.totalRatingFieldCount);
        dest.writeInt(this.totalDurationInMinutes);
        dest.writeInt(this.totalOrgasmCount);
        dest.writeDouble(this.averageRating);
        dest.writeDouble(this.averageOrgasmCount);
        dest.writeDouble(this.averageDurationInMinutes);
        dest.writeInt(this.maximumEntryCountForDate);
        dest.writeInt(this.maximumOrgasmCountForEntry);
        dest.writeInt(this.maximumDurationInMinutesForEntry);
    }

    public SimpleEntryStatistics() {
    }

    protected SimpleEntryStatistics(Parcel in) {
        this.totalEntryCount = in.readInt();
        this.totalDurationFieldCount = in.readInt();
        this.totalOrgasmsFieldCount = in.readInt();
        this.totalRatingFieldCount = in.readInt();
        this.totalDurationInMinutes = in.readInt();
        this.totalOrgasmCount = in.readInt();
        this.averageRating = in.readDouble();
        this.averageOrgasmCount = in.readDouble();
        this.averageDurationInMinutes = in.readDouble();
        this.maximumEntryCountForDate = in.readInt();
        this.maximumOrgasmCountForEntry = in.readInt();
        this.maximumDurationInMinutesForEntry = in.readInt();
    }

    public static final Parcelable.Creator<SimpleEntryStatistics> CREATOR = new Parcelable.Creator<SimpleEntryStatistics>() {
        @Override
        public SimpleEntryStatistics createFromParcel(Parcel source) {
            return new SimpleEntryStatistics(source);
        }

        @Override
        public SimpleEntryStatistics[] newArray(int size) {
            return new SimpleEntryStatistics[size];
        }
    };
}
