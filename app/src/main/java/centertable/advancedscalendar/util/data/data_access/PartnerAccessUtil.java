package centertable.advancedscalendar.util.data.data_access;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.data.pojo.Partner;
import centertable.advancedscalendar.data.room.ASCDatabase;
import centertable.advancedscalendar.data.room.dao.EntryPartnerDao;
import centertable.advancedscalendar.data.room.dao.PartnerDao;
import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;
import centertable.advancedscalendar.data.room.entity.PartnerEntity;
import centertable.advancedscalendar.modules.login.AuthenticationInfo;

class PartnerAccessUtil{
    private PartnerDao partnerDao;

    PartnerAccessUtil(ASCDatabase db){
        partnerDao = db.getPartnerDao();
    }

    List<PartnerEntity> getAllPartnersForUser(){
        return partnerDao.getAllPartnersForUser(AuthenticationInfo.getInstance().getCurrentUserId());
    }

    List<PartnerEntity> getAllPartnersForEntry(long entryId){
        return partnerDao.getAllPartnersForEntry(AuthenticationInfo.getInstance().getCurrentUserId(), entryId);
    }

    boolean insertPartner(Partner partner){
        partner.setUserId(AuthenticationInfo.getInstance().getCurrentUserId());

        long resultId = partnerDao.insert(partner.generateEntity())[0];
        return resultId != 0;
    }

    public void deletePartner(Partner partner){
        partner.setUserId(AuthenticationInfo.getInstance().getCurrentUserId());

        partnerDao.delete(partner.generateEntity());
    }

    static List<Partner> getPojoListFromEntityList(List<PartnerEntity> list) {
        List<Partner> entryList = new ArrayList<>();

        if(list != null) {
            for (int i = 0; i < list.size(); i++) {
                entryList.add(new Partner(list.get(i)));
            }
        }

        return entryList;
    }
}
