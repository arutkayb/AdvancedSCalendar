package centertable.advancedscalendar.util.purchase;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.data.remote.billing.InAppPurchase;

public class GooglePurchaseUtil {
    private  GooglePurchaseUtil(){}

    public static List<InAppPurchase.ProductDetails> getProductDetailsList(List<SkuDetails> skuDetailsList){
        List<InAppPurchase.ProductDetails> productDetailsList = new ArrayList<>();
        InAppPurchase.ProductDetails productDetails;

        for(SkuDetails skuDetails : skuDetailsList){
            productDetails = getProductDetails(skuDetails);

            productDetailsList.add(productDetails);
        }

        return  productDetailsList;
    }

    public static InAppPurchase.ProductDetails getProductDetails(SkuDetails skuDetails){
        InAppPurchase.ProductDetails productDetails = new InAppPurchase.ProductDetails();

        productDetails.setDescription(skuDetails.getDescription());
        productDetails.setPrice(skuDetails.getPrice());
        productDetails.setPriceCurrencyCode(skuDetails.getPriceCurrencyCode());
        productDetails.setProductId(skuDetails.getSku());

        return productDetails;
    }

    public static List<InAppPurchase.Purchase> getPurchasesList(List<Purchase> purchases){
        List<InAppPurchase.Purchase> purchaseList = new ArrayList<>();

        if(purchases != null) {
            InAppPurchase.Purchase genericPurchase;

            for (Purchase purchase : purchases) {
                genericPurchase = getPurchases(purchase);

                purchaseList.add(genericPurchase);
            }
        }

        return  purchaseList;
    }

    public static InAppPurchase.Purchase getPurchases(Purchase purchase){
        InAppPurchase.Purchase genericPurchase = new InAppPurchase.Purchase();

        genericPurchase.setProductId(purchase.getSku());
        genericPurchase.setAutoRenewing(purchase.isAutoRenewing());
        genericPurchase.setOrderId(purchase.getOrderId());
        genericPurchase.setPackageName(purchase.getPackageName());
        genericPurchase.setPurchaseEpochTime(purchase.getPurchaseTime());
        genericPurchase.setPurchaseToken(purchase.getPurchaseToken());

        return genericPurchase;
    }
    
    public static int getGenericResponseCode(int googlePurchaseResponse){
        int response;

        switch (googlePurchaseResponse){
            case BillingClient.BillingResponse.SERVICE_DISCONNECTED:
                response = InAppPurchase.ResponseCode.SERVICE_DISCONNECTED;
                break;
            case BillingClient.BillingResponse.OK:
                response = InAppPurchase.ResponseCode.OK;
                break;
            case BillingClient.BillingResponse.ITEM_ALREADY_OWNED:
                response = InAppPurchase.ResponseCode.ITEM_ALREADY_OWNED;
                break;
            case BillingClient.BillingResponse.ITEM_NOT_OWNED:
                response = InAppPurchase.ResponseCode.ITEM_NOT_OWNED;
                break;
            case BillingClient.BillingResponse.ITEM_UNAVAILABLE:
                response = InAppPurchase.ResponseCode.ITEM_UNAVAILABLE;
                break;
            case BillingClient.BillingResponse.USER_CANCELED:
                response = InAppPurchase.ResponseCode.USER_CANCELED;
                break;
            case BillingClient.BillingResponse.BILLING_UNAVAILABLE:
                response = InAppPurchase.ResponseCode.SERVICE_UNAVAILABLE;
                break;
            default:
                response = InAppPurchase.ResponseCode.ERROR;
                break;
        }
        
        return response;
    }
}
