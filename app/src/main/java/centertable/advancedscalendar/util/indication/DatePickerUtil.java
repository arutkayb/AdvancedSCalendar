package centertable.advancedscalendar.util.indication;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Date;

public class DatePickerUtil {

    public static void showDatePicker(Context context,
                                      DatePickerDialog.OnDateSetListener listener,
                                      Date selectedDate,
                                      @Nullable Date minSelectableDate,
                                      @Nullable Date maxSelectableDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(selectedDate);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                listener, year, month, day);
        if(minSelectableDate != null)
            datePickerDialog.getDatePicker().setMinDate(minSelectableDate.getTime());

        if(maxSelectableDate != null)
            datePickerDialog.getDatePicker().setMaxDate(maxSelectableDate.getTime());

        datePickerDialog.show();
    }
}
