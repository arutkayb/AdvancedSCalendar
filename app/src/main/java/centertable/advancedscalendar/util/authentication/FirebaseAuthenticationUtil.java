package centertable.advancedscalendar.util.authentication;

import com.google.firebase.auth.FirebaseUser;

import centertable.advancedscalendar.data.pojo.User;

public class FirebaseAuthenticationUtil {
    private FirebaseAuthenticationUtil(){}

    public static User generateUserWithFirebaseUser(FirebaseUser firebaseUser){
        User user = null;

        if(firebaseUser != null) {
            user = new User();

            user.setUserId(firebaseUser.getUid().hashCode());

            user.setRemoteUid(firebaseUser.getUid());

            String displayName = firebaseUser.getDisplayName();

            user.setName(displayName);

            user.setEmail(firebaseUser.getEmail());
        }

        return user;
    }
}
