package centertable.advancedscalendar.util.chart.value_formatter;

import android.os.Parcel;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ViewPortHandler;

public class IntegerTopValueFormatter implements ValueFormatter{

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        String ret = "";

        if (value == 0)
            ret = "";
        else if(value == (int)value)
            ret = String.valueOf((int)value);
        else
            ret = String.valueOf(value);

        return ret;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public IntegerTopValueFormatter() {
    }

    protected IntegerTopValueFormatter(Parcel in) {
    }

    public static final Creator<IntegerTopValueFormatter> CREATOR = new Creator<IntegerTopValueFormatter>() {
        @Override
        public IntegerTopValueFormatter createFromParcel(Parcel source) {
            return new IntegerTopValueFormatter(source);
        }

        @Override
        public IntegerTopValueFormatter[] newArray(int size) {
            return new IntegerTopValueFormatter[size];
        }
    };
}
