package centertable.advancedscalendar.util.chart;

import android.os.Parcel;
import android.os.Parcelable;

import centertable.advancedscalendar.util.chart.value_formatter.AxisValueFormatter;
import centertable.advancedscalendar.util.chart.value_formatter.ValueFormatter;

public class ChartSpecification implements Parcelable {
    private AxisValueFormatter xFormatter;
    private AxisValueFormatter yLeftFormatter;
    private AxisValueFormatter yRightFormatter;
    private ValueFormatter topFormatter;

    public String xDescription = "";
    public String yLeftDescription = "";
    public String yRightDescription = "";
    public String legendDestription = "";

    public ChartSpecification(){}

    public ChartSpecification(AxisValueFormatter xFormatter, AxisValueFormatter yLeftFormatter, AxisValueFormatter yRightFormatter, ValueFormatter topFormatter) {
        this.xFormatter = xFormatter;
        this.yLeftFormatter = yLeftFormatter;
        this.yRightFormatter = yRightFormatter;
        this.topFormatter = topFormatter;
    }

    public AxisValueFormatter getXFormatter() {
        return xFormatter;
    }

    public AxisValueFormatter getYLeftFormatter() {
        return yLeftFormatter;
    }

    public AxisValueFormatter getYRightFormatter() {
        return yRightFormatter;
    }

    public ValueFormatter getTopFormatter() {
        return topFormatter;
    }

    public void setXFormatter(AxisValueFormatter xFormatter) {
        this.xFormatter = xFormatter;
    }

    public void setYLeftFormatter(AxisValueFormatter yLeftFormatter) {
        this.yLeftFormatter = yLeftFormatter;
    }

    public void setYRightFormatter(AxisValueFormatter yRightFormatter) {
        this.yRightFormatter = yRightFormatter;
    }

    public void setTopFormatter(ValueFormatter topFormatter) {
        this.topFormatter = topFormatter;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.xFormatter, flags);
        dest.writeParcelable(this.yLeftFormatter, flags);
        dest.writeParcelable(this.yRightFormatter, flags);
        dest.writeParcelable(this.topFormatter, flags);
    }

    protected ChartSpecification(Parcel in) {
        this.xFormatter = in.readParcelable(AxisValueFormatter.class.getClassLoader());
        this.yLeftFormatter = in.readParcelable(AxisValueFormatter.class.getClassLoader());
        this.yRightFormatter = in.readParcelable(AxisValueFormatter.class.getClassLoader());
        this.topFormatter = in.readParcelable(ValueFormatter.class.getClassLoader());
    }

    public static final Parcelable.Creator<ChartSpecification> CREATOR = new Parcelable.Creator<ChartSpecification>() {
        @Override
        public ChartSpecification createFromParcel(Parcel source) {
            return new ChartSpecification(source);
        }

        @Override
        public ChartSpecification[] newArray(int size) {
            return new ChartSpecification[size];
        }
    };
}
