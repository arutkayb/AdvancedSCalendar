package centertable.advancedscalendar.util.chart.value_formatter;

import android.os.Parcel;

import com.github.mikephil.charting.components.AxisBase;

public class IntegerAxisValueFormatter implements AxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return String.valueOf((int)value);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public IntegerAxisValueFormatter() {
    }

    protected IntegerAxisValueFormatter(Parcel in) {
    }

    public static final Creator<IntegerAxisValueFormatter> CREATOR = new Creator<IntegerAxisValueFormatter>() {
        @Override
        public IntegerAxisValueFormatter createFromParcel(Parcel source) {
            return new IntegerAxisValueFormatter(source);
        }

        @Override
        public IntegerAxisValueFormatter[] newArray(int size) {
            return new IntegerAxisValueFormatter[size];
        }
    };
}
