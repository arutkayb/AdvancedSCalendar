package centertable.advancedscalendar.util.chart.value_formatter;

import android.os.Parcel;

import com.github.mikephil.charting.components.AxisBase;

import java.util.Date;

import centertable.advancedscalendar.util.time.TimeUtil;

public class DayAxisValueFormatter implements AxisValueFormatter {
    // value should be the time in day such as (Date.getTime / TimeUtil.DAY_TO_MILLIS)
    // TODO: getTime method of Date class gives a "long" type and passing it to float is not safe, refactor here
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        //TODO: +1f next to the "value" comes because of a bug, fix it
        Date date = new Date((long)((value+1f) * TimeUtil.DAY_TO_MILLIS));
        return TimeUtil.getChartDate(date);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public DayAxisValueFormatter() {
    }

    protected DayAxisValueFormatter(Parcel in) {
    }

    public static final Creator<DayAxisValueFormatter> CREATOR = new Creator<DayAxisValueFormatter>() {
        @Override
        public DayAxisValueFormatter createFromParcel(Parcel source) {
            return new DayAxisValueFormatter(source);
        }

        @Override
        public DayAxisValueFormatter[] newArray(int size) {
            return new DayAxisValueFormatter[size];
        }
    };
}
