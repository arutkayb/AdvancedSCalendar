package centertable.advancedscalendar.util.chart.value_formatter;

import android.os.Parcelable;

import com.github.mikephil.charting.formatter.IValueFormatter;

public interface ValueFormatter extends IValueFormatter, Parcelable {

}
