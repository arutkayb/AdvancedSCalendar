package centertable.advancedscalendar.util.chart.value_formatter;

import android.os.Parcelable;

import com.github.mikephil.charting.formatter.IAxisValueFormatter;

public interface AxisValueFormatter extends IAxisValueFormatter, Parcelable {
}
