package centertable.advancedscalendar.util.time;

import android.util.Log;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class TimeUtil {
    private static final String TAG = "TimeUtil";

    public static final long DAY_TO_MILLIS = 86400000;

    private final static String DB_TIME_PATTERN = "yyyy.MM.dd";
    private final static String CHART_TIME_PATTERN = "d MMM";

    public static String getDbDate(CalendarDay calendarDay){
        return getDbDate(calendarDay.getDate());
    }

    public static String getDbDate(Date date){
        SimpleDateFormat dbFormat = new SimpleDateFormat(DB_TIME_PATTERN);
        return dbFormat.format(date);
    }

    public static String getChartDate(Date date){
        SimpleDateFormat dbFormat = new SimpleDateFormat(CHART_TIME_PATTERN);
        return dbFormat.format(date);
    }

    public static CalendarDay getCalendarDayFromDbDate(String dbDate){
        return CalendarDay.from(getDateFromDbDate(dbDate));
    }

    public static Date getDateFromDbDate(String dbDate){
        Date date = new Date(0);

        try{
            date = new SimpleDateFormat(DB_TIME_PATTERN).parse(dbDate);
        }catch (ParseException ex){
            Log.e(TAG, "Date: '" + dbDate + "' cannot be parsed as dbDate!");
        }

        return date;
    }

    public static Date getToday(){
        Date today = Calendar.getInstance().getTime();
        return today;
    }

    // Positive values means future days while negative value means past
    public static Date getAnotherDateFromToday(int numberOfDays){
        return getAnotherDateFromParticularDate(new Date(), numberOfDays);
    }

    // Positive values means future days while negative value means past
    public static Date getAnotherDateFromParticularDate(Date date, int numberOfDays){
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, numberOfDays);
        return cal.getTime();
    }

    public static String getTodayAsDbDateString(){
        return getDbDate(getToday());
    }

    public static ArrayList<Date> getDbDateListFromInterval(Date startDate, Date endDate){
        ArrayList<Date> result = new ArrayList<>();

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);

        //Initialise 0 for all days in interval
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            // Workaround for making entry date equal to created date to replace in map later
            Date dbDate = TimeUtil.getDateFromDbDate(TimeUtil.getDbDate(date));

            result.add(dbDate);
        }

        return result;
    }
}
