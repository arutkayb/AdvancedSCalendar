package centertable.advancedscalendar.data.remote.authorization;

import centertable.advancedscalendar.data.pojo.User;

public interface AuthorizationDatabase {
    String REFERENCE_USERS = "users";

    String PURCHASES = "purchases";
    String PURCHASE_STATUS = "purchase_status";
    String PURCHASE_ORDER = "order_id";

    String EMAIL = "email";

    void initialise(User user);
    void insertUser();
    void readUser(OnStatusListener listener);
    void readPurchaseData(DataEventListener<ProductPurchaseDetail> listener);
    void updatePurchaseData(ProductPurchaseDetail productPurchaseDetail);

    interface OnStatusListener{
        void onSuccess();
        void onFailure();
    }

    interface DataEventListener<T>{
        void onDataChange(T result);
        void onCancelled();
    }

    class ProductPurchaseDetail{
        private String orderId;
        private String productId;
        private boolean purchased;

        public ProductPurchaseDetail() { }

        public ProductPurchaseDetail(String orderId, String productId, boolean purchased) {
            this.orderId = orderId;
            this.productId = productId;
            this.purchased = purchased;
        }

        public String getOrderId() {
            return orderId;
        }

        public String getProductId() {
            return productId;
        }

        public boolean isPurchased() {
            return purchased;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public void setPurchased(boolean purchased) {
            this.purchased = purchased;
        }
    }
}
