package centertable.advancedscalendar.data.remote.authentication;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;

import centertable.advancedscalendar.data.pojo.User;

public interface Authentication extends Parcelable {
    String AUTHENTICATION_PARCELABLE = "authentication_parcelable";

    enum REQUEST_CODE{
        SIGN_IN(0), SIGN_OUT(1);

        private final int id;

        REQUEST_CODE(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static REQUEST_CODE fromValue(int value) {
            for (REQUEST_CODE e: REQUEST_CODE.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }
    }

    enum RESPONSE_CODE{
        SUCCESSFUL(0), CANCELED(1), RETRY(2), PROVIDER_ERROR(3), NO_NETWORK(4), WRONG_CREDENTIAL(5), USER_NOT_FOUND(6);

        private final int id;

        RESPONSE_CODE(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static RESPONSE_CODE fromValue(int value) {
            for (RESPONSE_CODE e: RESPONSE_CODE.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }
    }

    void signUp(AppCompatActivity activity);
    void sendWelcomeMail();
    void signIn(AppCompatActivity activity);
    void signOut(AppCompatActivity activity, OnCompleteListener listener);

    boolean isUserSignedIn(long userId);
    RESPONSE_CODE getSignInResult(User user, Intent intent);
    void changePin();
    void changePassword();
    void changeEmail();

    interface OnCompleteListener{
        void onComplete(RESPONSE_CODE response_code);
    }
}
