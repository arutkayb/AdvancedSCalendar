package centertable.advancedscalendar.data.remote.billing;

import android.app.Activity;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public interface InAppPurchase {
    void startConnection(Activity activity, BillingClientStateListener stateListener,  BillingClientPurchaseUpdateListener purchaseUpdateListener);
    void getProductList(ProductDetailsResponseListener listener, ArrayList<String> productIds);
    int launchBillingFlow(Activity activity, String productId);
    void consumeProduct(Purchase purchase, ConsumeResponseListener listener);
    List<Purchase> getPurchases();

    interface Product {
        String LOCK_FEATURE = "lock_feature";
        String REMOVE_ADS = "remove_ads";
        String LIMITLESS_PARTNER = "limitless_partner";
    }

    interface ResponseCode{
        int SERVICE_UNAVAILABLE = -3;
        int SERVICE_DISCONNECTED = -2;
        int ERROR = -1;
        int OK = 0;
        int ITEM_ALREADY_OWNED = 1;
        int ITEM_NOT_OWNED = 2;
        int ITEM_UNAVAILABLE = 3;
        int USER_CANCELED = 4;
    }

    interface BillingClientStateListener {
        void onBillingSetupFinished(int responseCode);
        void onBillingServiceDisconnected();
    }

    interface BillingClientPurchaseUpdateListener{
        void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases);
    }

    class Purchase{
        String orderId;
        String packageName;
        long purchaseEpochTime;
        String productId;
        boolean isAutoRenewing;
        String purchaseToken;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public long getPurchaseEpochTime() {
            return purchaseEpochTime;
        }

        public void setPurchaseEpochTime(long purchaseEpochTime) {
            this.purchaseEpochTime = purchaseEpochTime;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public boolean isAutoRenewing() {
            return isAutoRenewing;
        }

        public void setAutoRenewing(boolean autoRenewing) {
            isAutoRenewing = autoRenewing;
        }

        public String getPurchaseToken() {
            return purchaseToken;
        }

        public void setPurchaseToken(String purchaseToken) {
            this.purchaseToken = purchaseToken;
        }
    }

    interface ProductDetailsResponseListener{
        void onProductDetailsResponse(int responseCode, List<ProductDetails> productDetailsList);
    }

    interface ConsumeResponseListener{
        void onConsumeResponse(int responseCode, String token);
    }

    class ProductDetails{
        private String description;
        private String price;
        private String priceCurrencyCode;
        private String productId;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPriceCurrencyCode() {
            return priceCurrencyCode;
        }

        public void setPriceCurrencyCode(String priceCurrencyCode) {
            this.priceCurrencyCode = priceCurrencyCode;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }
    }

}
