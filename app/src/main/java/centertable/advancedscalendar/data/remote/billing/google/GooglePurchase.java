package centertable.advancedscalendar.data.remote.billing.google;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.data.remote.billing.InAppPurchase;
import centertable.advancedscalendar.util.purchase.GooglePurchaseUtil;


public class GooglePurchase implements InAppPurchase {
    private BillingClient billingClient;

    @Override
    public void startConnection(Activity activity, BillingClientStateListener stateListener, BillingClientPurchaseUpdateListener purchaseUpdateListener){
        billingClient = BillingClient.newBuilder(activity).setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(int responseCode, @Nullable List<com.android.billingclient.api.Purchase> purchases) {
                int response = GooglePurchaseUtil.getGenericResponseCode(responseCode);
                if(response == ResponseCode.OK) {
                    if(purchases != null) {
                        List<Purchase> purchaseList = GooglePurchaseUtil.getPurchasesList(purchases);
                        purchaseUpdateListener.onPurchasesUpdated(responseCode, purchaseList);
                    }
                }
            }
        }).build();

        billingClient.startConnection(new com.android.billingclient.api.BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                Log.d(getClass().getName(), "Start connection response code: " + String.valueOf(responseCode));

                int response = GooglePurchaseUtil.getGenericResponseCode(responseCode);

                stateListener.onBillingSetupFinished(response);
            }

            @Override
            public void onBillingServiceDisconnected() {
                stateListener.onBillingServiceDisconnected();
            }
        });
    }

    @Override
    public void consumeProduct(Purchase purchase, ConsumeResponseListener listener) {
        com.android.billingclient.api.ConsumeResponseListener apiListener = new com.android.billingclient.api.ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(int responseCode, String purchaseToken) {
                int response = GooglePurchaseUtil.getGenericResponseCode(responseCode);
                listener.onConsumeResponse(response, purchaseToken);
            }
        };

        if(purchase != null)
            billingClient.consumeAsync(purchase.getPurchaseToken(), apiListener);
    }

    @Override
    public List<Purchase> getPurchases() {
        List<Purchase> purchaseList = new ArrayList<>();

        com.android.billingclient.api.Purchase.PurchasesResult res = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
        purchaseList = GooglePurchaseUtil.getPurchasesList(res.getPurchasesList());

        return purchaseList;
    }

    @Override
    public void getProductList(ProductDetailsResponseListener listener, ArrayList<String> productIds){
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(productIds).setType(BillingClient.SkuType.INAPP);

        billingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                listener.onProductDetailsResponse(responseCode, GooglePurchaseUtil.getProductDetailsList(skuDetailsList));
            }
        });
    }

    @Override
    public int launchBillingFlow(Activity activity, String productId) {
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSku(productId)
                .setType(BillingClient.SkuType.INAPP) // SkuType.SUB for subscription
                .build();
        int responseCode = billingClient.launchBillingFlow(activity, flowParams);
        
        return GooglePurchaseUtil.getGenericResponseCode(responseCode);
    }
}
