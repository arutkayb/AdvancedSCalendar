package centertable.advancedscalendar.data.remote.authentication.firebase;

import android.content.Intent;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import centertable.advancedscalendar.R;
import centertable.advancedscalendar.data.remote.authentication.Authentication;
import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.util.authentication.FirebaseAuthenticationUtil;

public class FirebaseAuthentication implements Authentication {
    @Override
    public void signUp(AppCompatActivity activity) {
        signIn(activity);
    }

    @Override
    public void signIn(AppCompatActivity activity) {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build());

        activity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setTheme(R.style.LoginTheme)
                        .setAvailableProviders(providers)
                        .setLogo(R.drawable.asc_icon_transparent)
                        .build(),
                REQUEST_CODE.SIGN_IN.getValue());
    }

    @Override
    public void signOut(AppCompatActivity activity, OnCompleteListener listener) {
        AuthUI.getInstance()
                .signOut(activity)
                .addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        RESPONSE_CODE response_code = RESPONSE_CODE.CANCELED;

                        if(task.isSuccessful())
                            response_code = RESPONSE_CODE.SUCCESSFUL;

                        listener.onComplete(response_code);
                    }
                });
    }

    @Override
    public boolean isUserSignedIn(long userId) {
        boolean ret = false;

        try {
            ret = FirebaseAuth.getInstance().getCurrentUser().getUid().hashCode() == userId;
        } catch (NullPointerException ex){
            Log.d(getClass().getName(), "Firebase cannot get current user");
        }

        return ret;
    }

    @Override
    public void sendWelcomeMail() {
        if(FirebaseAuth.getInstance().getCurrentUser() != null)
            FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification();
        else
            Log.d(getClass().getName(), "No currently signed in user");
    }

    @Override
    public RESPONSE_CODE getSignInResult(User user, Intent data) {
        RESPONSE_CODE responseCode = RESPONSE_CODE.RETRY;

        IdpResponse response = IdpResponse.fromResultIntent(data);

        if(response != null) {
            if (response.getError() != null) {
                switch (response.getError().getErrorCode()){
                    case ErrorCodes.NO_NETWORK:
                        responseCode = RESPONSE_CODE.NO_NETWORK;
                        break;
                    case ErrorCodes.PROVIDER_ERROR:
                        responseCode = RESPONSE_CODE.PROVIDER_ERROR;
                        break;
                    case ErrorCodes.EMAIL_MISMATCH_ERROR:
                        responseCode = RESPONSE_CODE.WRONG_CREDENTIAL;
                        break;
                    default:
                        responseCode = RESPONSE_CODE.RETRY;
                        break;
                }
            } else {
                // Successfully signed in
                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                if (firebaseUser != null) {
                    if (response.isNewUser()) {
                        firebaseUser.sendEmailVerification();
                    }

                    User newUser = FirebaseAuthenticationUtil.generateUserWithFirebaseUser(firebaseUser);

                    user.setUserId(newUser.getPrimaryId());
                    user.setName(newUser.getName());
                    user.setEmail(newUser.getEmail());
                    user.setRemoteUid(newUser.getRemoteUid());

                    responseCode = RESPONSE_CODE.SUCCESSFUL;
                }
            }
        } else {
            responseCode = RESPONSE_CODE.CANCELED;
        }

        return responseCode;
    }

    @Override
    public void changePin() {

    }

    @Override
    public void changePassword() {

    }

    @Override
    public void changeEmail() {

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public FirebaseAuthentication() {
    }

    protected FirebaseAuthentication(Parcel in) {
    }

    public static final Creator<FirebaseAuthentication> CREATOR = new Creator<FirebaseAuthentication>() {
        @Override
        public FirebaseAuthentication createFromParcel(Parcel source) {
            return new FirebaseAuthentication(source);
        }

        @Override
        public FirebaseAuthentication[] newArray(int size) {
            return new FirebaseAuthentication[size];
        }
    };
}
