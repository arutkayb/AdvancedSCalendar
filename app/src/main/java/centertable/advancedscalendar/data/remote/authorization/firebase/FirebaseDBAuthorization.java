package centertable.advancedscalendar.data.remote.authorization.firebase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import centertable.advancedscalendar.data.pojo.User;
import centertable.advancedscalendar.data.remote.authorization.AuthorizationDatabase;


public class FirebaseDBAuthorization implements AuthorizationDatabase {
    private FirebaseDatabase database;
    private DatabaseReference userDb;
    private User user;

    @Override
    public void initialise(@NonNull User user) {
        if(database == null) {
            database = FirebaseDatabase.getInstance();

            try {
                database.setPersistenceEnabled(true);
            }catch (Exception ex){}
        }

        this.user = user;
        userDb = FirebaseDatabase.getInstance().getReference(AuthorizationDatabase.REFERENCE_USERS).child(String.valueOf(user.getRemoteUid()));
    }

    @Override
    public void insertUser() {
        userDb.child(EMAIL).setValue(user.getEmail());
    }

    @Override
    public void readUser(OnStatusListener listener) {
        userDb.child(EMAIL).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GenericTypeIndicator<String> emailType = new GenericTypeIndicator<String>(){};

                String email = dataSnapshot.getValue(emailType);

                if(email == null || email.isEmpty()){
                    listener.onFailure();
                } else {
                    listener.onSuccess();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(getClass().getName(), "Error no reading user email data: " + databaseError.getMessage() + ", inserting email.");
                listener.onFailure();
            }
        });
    }

    @Override
    public void readPurchaseData(DataEventListener<ProductPurchaseDetail> listener) {
        userDb.child(PURCHASES).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ProductPurchaseDetail productDetails = null;

                for (DataSnapshot single : dataSnapshot.getChildren()) {
                     productDetails = single.getValue(ProductPurchaseDetail.class);
                }

                if(productDetails != null && productDetails.getOrderId() != null && !productDetails.getOrderId().isEmpty())
                    listener.onDataChange(productDetails);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(getClass().getName(), "Error no reading purchase data: " + databaseError.getMessage());
                listener.onCancelled();
            }
        });
    }

    @Override
    public void updatePurchaseData(ProductPurchaseDetail productPurchaseDetail) {
        userDb.child(PURCHASES).child(productPurchaseDetail.getProductId()).setValue(productPurchaseDetail);
    }
}
