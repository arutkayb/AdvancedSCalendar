package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Entity(tableName = DatabaseDefs.PARTNER_TABLE_NAME,
        indices = {@Index(value = {"user_id"}),
                @Index(value = {"partner_id"})},
                foreignKeys = @ForeignKey(entity = UserEntity.class,
                parentColumns = "user_id",
                childColumns = "user_id",
                onDelete = ForeignKey.CASCADE))
public class PartnerEntity {

    public PartnerEntity(long partnerId,
                         long userId,
                         String name,
                         int relationshipType,
                         int gender,
                         String dateCreatedString,
                         String note,
                         String photoLocation) {
        this.partnerId = partnerId;
        this.userId = userId;
        this.name = name;
        this.relationshipType = relationshipType;
        this.gender = gender;
        this.dateCreatedString = dateCreatedString;
        this.note = note;
        this.photoLocation = photoLocation;
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "partner_id")
    public long partnerId;

    //user id as a foreign key
    @ColumnInfo(name = "user_id")
    public long userId;

    //Full name of the Partner
    @ColumnInfo(name = "name")
    public String name;

    //Type of relationship between the partner and the user
    @ColumnInfo(name = "relationship_type")
    public int relationshipType;

    //Gender of the Partner
    @ColumnInfo(name = "gender")
    public int gender;

    //Date of Entry
    @ColumnInfo(name = "date_created")
    public String dateCreatedString;

    //Personal note for the Partner
    @ColumnInfo(name = "note")
    public String note;

    //Profile photo location of the Partner
    @ColumnInfo(name = "photo_location")
    public String photoLocation;

}
