package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.AchievementEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface AchievementDao extends IASCDao<AchievementEntity>{
    @Query("DELETE FROM " + DatabaseDefs.ACHIEVEMENT_TABLE_NAME + " WHERE user_id = :userId")
    public void clearAllAchievementsForUser(long userId );

    @Query("SELECT * FROM " + DatabaseDefs.ACHIEVEMENT_TABLE_NAME + " WHERE user_id = :userId")
    public List<AchievementEntity> getAllAchievementsForUser(long userId );
}
