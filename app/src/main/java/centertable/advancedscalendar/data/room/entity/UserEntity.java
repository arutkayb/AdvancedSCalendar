package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Entity(tableName = DatabaseDefs.USER_TABLE_NAME,
        indices = {@Index(value = {"user_id"}),
                @Index(value = {"name"})})
public class UserEntity {

    public UserEntity(long userId, String remoteUid, String name, String email, int gender) {
        this.userId = userId;
        this.remoteUid = remoteUid;
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    //
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_id")
    public long userId;

    //Nick name of the User, unique for app
    @ColumnInfo(name = "name")
    public String name;

    //Email address of the User
    @ColumnInfo(name = "email")
    public String email;

    //Remote database uid of the User
    @ColumnInfo(name = "remote_uid")
    public String remoteUid;

    //Gender of the User
    @ColumnInfo(name = "gender")
    public int gender;

}
