package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

//add indices for faster search on date and entry_no
@Entity( tableName = DatabaseDefs.ENTRY_TABLE_NAME,
        indices = {@Index(value = {"user_id"}),
                @Index(value = {"date"}),
                @Index(value = {"entry_id"})},
        foreignKeys = @ForeignKey(entity = UserEntity.class,
                parentColumns = "user_id",
                childColumns = "user_id",
                onDelete = ForeignKey.CASCADE))
public class EntryEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "entry_id") //unique identifier of data row
    public long entryId;

    //user id as a foreign key
    @ColumnInfo(name = "user_id")
    public long userId;

    //Date of Entry
    @ColumnInfo(name = "date")
    @NonNull
    public String dateString;

    //Total duration of entry activities
    @ColumnInfo(name = "duration")
    public int duration;

    //User note for the entry
    @ColumnInfo(name = "note")
    public String note;

    //User rating for the overall activities in the entry
    @ColumnInfo(name = "rating")
    public int rating;

    //Safety status for the overall activities in the entry
    @ColumnInfo(name = "safety_status")
    public boolean safetyStatus;

    //Total number of orgasms during the overall entry activities
    @ColumnInfo(name = "total_org")
    public int totalOrg;

    public EntryEntity(long entryId, long userId, String dateString, int duration, String note, int rating, boolean safetyStatus, int totalOrg) {
        this.entryId = entryId;
        this.userId = userId;
        this.dateString = dateString;
        this.duration = duration;
        this.note = note;
        this.rating = rating;
        this.safetyStatus = safetyStatus;
        this.totalOrg = totalOrg;
    }
}
