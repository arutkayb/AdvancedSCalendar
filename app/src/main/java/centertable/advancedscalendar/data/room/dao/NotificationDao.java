package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.NotificationEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface NotificationDao extends IASCDao<NotificationEntity> {
    @Query("DELETE FROM "+ DatabaseDefs.NOTIFICATION_TABLE_NAME + " WHERE user_id = :userId")
    public void clearAllNotificationsForUser(long userId);

    @Query("SELECT * FROM "+ DatabaseDefs.NOTIFICATION_TABLE_NAME + " WHERE user_id = :userId")
    List<NotificationEntity> getAllNotificationsForUser(long userId);
}
