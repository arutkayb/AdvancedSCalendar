package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Entity(tableName = DatabaseDefs.ENTRY_SEX_TYPE_TABLE_NAME,
        indices = {@Index(value = {"entry_id"}),
                @Index(value = {"id"})},
        foreignKeys = @ForeignKey(entity = EntryEntity.class,
                                parentColumns = "entry_id",
                                childColumns = "entry_id",
                                onDelete = ForeignKey.CASCADE))
public class EntrySexTypeEntity {
    //primary unique id for relationship between entry and sex type
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long uId;

    @ColumnInfo(name = "entry_id")
    public long entryId;

    //Sex types and unique identifiers of sex types will be mapped
    @ColumnInfo(name = "sex_type_id")
    public int sexTypeId;

    public EntrySexTypeEntity(long uId, int sexTypeId, long entryId) {
        this.uId = uId;
        this.entryId = entryId;
        this.sexTypeId = sexTypeId;
    }
}
