package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.EntrySexTypeEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface EntrySexTypeDao extends IASCDao<EntrySexTypeEntity>{
    @Query("DELETE FROM "+ DatabaseDefs.ENTRY_SEX_TYPE_TABLE_NAME +" WHERE entry_id = :entryId")
    public void clearAllSexTypesForEntry(long entryId);

    @Query("SELECT * FROM "+ DatabaseDefs.ENTRY_SEX_TYPE_TABLE_NAME +" WHERE entry_id = :entryId")
    List<EntrySexTypeEntity> getAllSexTypesForEntry(long entryId);
}
