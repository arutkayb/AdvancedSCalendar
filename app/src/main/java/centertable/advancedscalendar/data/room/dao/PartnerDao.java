package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.PartnerEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface PartnerDao extends IASCDao<PartnerEntity>{
    @Query("DELETE FROM "+ DatabaseDefs.PARTNER_TABLE_NAME + " WHERE user_id = :userId")
    void clearAllPartnersForUser(long userId);

    @Query("SELECT * FROM "+ DatabaseDefs.PARTNER_TABLE_NAME + " WHERE user_id = :userId")
    List<PartnerEntity> getAllPartnersForUser(long userId);

    @Query("SELECT * FROM "+ DatabaseDefs.PARTNER_TABLE_NAME + " WHERE partner_id = :partnerId")
    PartnerEntity getPartner(long partnerId);

    @Query("SELECT * FROM "
            + DatabaseDefs.PARTNER_TABLE_NAME
            + ", " + DatabaseDefs.ENTRY_PARTNER_TABLE_NAME
            + " WHERE "+ DatabaseDefs.PARTNER_TABLE_NAME +".user_id = :userId"
            + " AND "+ DatabaseDefs.ENTRY_PARTNER_TABLE_NAME +".entry_id = :entryId"
            + " AND "+ DatabaseDefs.ENTRY_PARTNER_TABLE_NAME +".partner_id = " + DatabaseDefs.PARTNER_TABLE_NAME +".partner_id"
    )
    List<PartnerEntity> getAllPartnersForEntry(long userId, long entryId);

}
