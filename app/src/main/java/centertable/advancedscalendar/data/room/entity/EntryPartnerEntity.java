package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

//entry has partners, a partner is used within entries (N-M relationship)
//So, new table is needed for these entry-partner pairs
@Entity( tableName = DatabaseDefs.ENTRY_PARTNER_TABLE_NAME,
        indices = {@Index(value = {"id"}),
                @Index(value = {"partner_id"}),
                @Index(value = {"entry_id"})},
        foreignKeys =
                {@ForeignKey(entity = PartnerEntity.class,
                    parentColumns = "partner_id",
                    childColumns = "partner_id",
                    onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = EntryEntity.class,
                    parentColumns = "entry_id",
                    childColumns = "entry_id",
                    onDelete = ForeignKey.CASCADE)})
public class EntryPartnerEntity {
    //primary unique id for relationship between entry and partner
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long uId;

    //partner id as a foreign key
    @ColumnInfo(name = "partner_id")
    public long partnerId;

    //entry id as a foreign key
    @ColumnInfo(name = "entry_id")
    public long entryId;

    public EntryPartnerEntity(long uId, long partnerId, long entryId) {
        this.uId = uId;
        this.partnerId = partnerId;
        this.entryId = entryId;
    }
}
