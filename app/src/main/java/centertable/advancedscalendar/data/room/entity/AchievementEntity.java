package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

//User has achievements (1-M relationship)
@Entity(tableName = DatabaseDefs.ACHIEVEMENT_TABLE_NAME,
        indices = {@Index(value = {"user_id"}),
                @Index(value = {"id"})},
        foreignKeys = @ForeignKey(entity = UserEntity.class,
                                parentColumns = "user_id",
                                childColumns = "user_id",
                                onDelete = ForeignKey.CASCADE))
public class AchievementEntity {
    public AchievementEntity(long uId, long userId, int achievement, boolean achievementStatus) {
        this.uId = uId;
        this.userId = userId;
        this.achievement = achievement;
        this.achievementStatus = achievementStatus;
    }

    //primary unique id for relationship between user and achievement
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long uId;

    //User id as a foreign key
    @ColumnInfo(name = "user_id")
    public long userId;

    //Achievement and unique identifiers of the achievements are mapped
    @ColumnInfo(name = "achievement")
    public int achievement;

    //An achievement can be enabled or disabled
    @ColumnInfo(name = "status")
    public boolean achievementStatus;

}
