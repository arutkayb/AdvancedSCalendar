package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface EntryPartnerDao extends IASCDao<EntryPartnerEntity>{
    @Query("DELETE FROM "+ DatabaseDefs.ENTRY_PARTNER_TABLE_NAME +" WHERE entry_id = :entryId")
    public void clearAllPartnersForEntry(long entryId);

    @Query("SELECT * FROM "+ DatabaseDefs.ENTRY_PARTNER_TABLE_NAME +" WHERE entry_id = :entryId")
    List<EntryPartnerEntity> getAllPartnersForEntry(long entryId);
}
