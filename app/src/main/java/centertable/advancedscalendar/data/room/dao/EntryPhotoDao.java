package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.EntryPhotoEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface EntryPhotoDao extends IASCDao<EntryPhotoEntity>{
    @Query("DELETE FROM "+ DatabaseDefs.ENTRY_PHOTO_TABLE_NAME +" WHERE entry_id = :entryId")
    public void clearAllPhotosForEntry(long entryId);

    @Query("SELECT * FROM "+ DatabaseDefs.ENTRY_PHOTO_TABLE_NAME +" WHERE entry_id = :entryId")
    List<EntryPhotoEntity> getAllPhotosForEntry(long entryId);
}
