package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.UserEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface UserDao extends IASCDao<UserEntity> {
    @Query("DELETE FROM "+ DatabaseDefs.USER_TABLE_NAME)
    void clearAll();

    @Query("SELECT * FROM "+ DatabaseDefs.USER_TABLE_NAME)
    List<UserEntity> getAll();

    @Query("SELECT * FROM "+ DatabaseDefs.USER_TABLE_NAME + " WHERE user_id = :userId")
    UserEntity getUserById(long userId);
}
