package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

/**
 * Created by Rutkay on 05.02.2018.
 */

@Dao
public interface IASCDao<Entity> {
    //insert operation is used for create a row. Replace on conflict. Returns the new rowId
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(Entity... entity);

    //update operation is used for update the row
    @Update
    void update(Entity... entity);

    //delete method is used for delete operation of the data
    @Delete
    void delete(Entity... entity);

}
