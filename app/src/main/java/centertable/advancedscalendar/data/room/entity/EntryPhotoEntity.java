package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

//entry has photos (1-M relationship)
@Entity( tableName = DatabaseDefs.ENTRY_PHOTO_TABLE_NAME,
        indices = {@Index(value = {"entry_id"}),
                @Index(value = {"id"})},
        foreignKeys = @ForeignKey(entity = EntryEntity.class,
                        parentColumns = "entry_id",
                        childColumns = "entry_id",
                        onDelete = ForeignKey.CASCADE))
public class EntryPhotoEntity {

    public EntryPhotoEntity(long uId, long entryId, String photoLocation) {
        this.uId = uId;
        this.entryId = entryId;
        this.photoLocation = photoLocation;
    }

    //primary unique id for relationship between entry and photo
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long uId;

    //entry id as a foreign key
    @ColumnInfo(name = "entry_id")
    public long entryId;

    @ColumnInfo(name = "photo_location")
    public String photoLocation;
}
