package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.EntryPlaceEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface EntryPlaceDao extends IASCDao<EntryPlaceEntity>{
    @Query("DELETE FROM "+ DatabaseDefs.ENTRY_PLACE_TABLE_NAME +" WHERE entry_id = :entryId")
    public void clearAllPlacesForEntry(long entryId);

    @Query("SELECT * FROM "+ DatabaseDefs.ENTRY_PLACE_TABLE_NAME +" WHERE entry_id = :entryId")
    List<EntryPlaceEntity> getAllPlacesForEntry(long entryId);

}
