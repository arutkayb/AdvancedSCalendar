package centertable.advancedscalendar.data.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;
import centertable.advancedscalendar.data.room.entity.EntryEntity;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Dao
public interface EntryDao extends IASCDao<EntryEntity>{
    @Query("DELETE FROM " + DatabaseDefs.ENTRY_TABLE_NAME + " WHERE user_id = :userId")
    public void clearAllEntriesForUser( long userId );

    @Query("SELECT * FROM " + DatabaseDefs.ENTRY_TABLE_NAME + " WHERE user_id = :userId ORDER BY date")
    public List<EntryEntity> getAllEntriesForUser(long userId );

    @Query("SELECT * FROM " + DatabaseDefs.ENTRY_TABLE_NAME + " WHERE user_id = :userId AND date = :date ORDER BY date")
    public List<EntryEntity> getEntriesForUserForDate( long userId, String date );

    @Query("SELECT * FROM " + DatabaseDefs.ENTRY_TABLE_NAME + " WHERE user_id = :userId AND date BETWEEN (:startDate) AND (:endDate) ORDER BY date")
    public List<EntryEntity> getEntriesForUserForTimeInterval( long userId, String startDate, String endDate );
}
