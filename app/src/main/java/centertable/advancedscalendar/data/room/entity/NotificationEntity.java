package centertable.advancedscalendar.data.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

//User has notifications (1-M relationship)
@Entity(tableName = DatabaseDefs.NOTIFICATION_TABLE_NAME,
        indices = {@Index(value = {"user_id"}),
                @Index(value = {"id"})},
        foreignKeys = @ForeignKey(entity = UserEntity.class,
                            parentColumns = "user_id",
                            childColumns = "user_id",
                            onDelete = ForeignKey.CASCADE))
public class NotificationEntity {
    //primary unique id for relationship between user and notification
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long uId;

    @ColumnInfo(name = "user_id")
    public long userId;

    //Notifications and unique identifiers of notifications will be mapped
    @ColumnInfo(name = "notification")
    public int notification;

    //Notifications can be enabled or disabled
    @ColumnInfo(name = "status")
    public boolean notificationStatus;

    public NotificationEntity(long uId, long userId, int notification, boolean notificationStatus) {
        this.uId = uId;
        this.userId = userId;
        this.notification = notification;
        this.notificationStatus = notificationStatus;
    }

}
