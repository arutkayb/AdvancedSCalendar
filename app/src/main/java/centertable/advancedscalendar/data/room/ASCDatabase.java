package centertable.advancedscalendar.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import centertable.advancedscalendar.data.room.dao.AchievementDao;
import centertable.advancedscalendar.data.room.dao.EntryDao;
import centertable.advancedscalendar.data.room.dao.EntryPartnerDao;
import centertable.advancedscalendar.data.room.dao.EntryPhotoDao;
import centertable.advancedscalendar.data.room.dao.EntryPlaceDao;
import centertable.advancedscalendar.data.room.dao.EntrySexTypeDao;
import centertable.advancedscalendar.data.room.dao.NotificationDao;
import centertable.advancedscalendar.data.room.dao.PartnerDao;
import centertable.advancedscalendar.data.room.dao.UserDao;
import centertable.advancedscalendar.data.room.entity.AchievementEntity;
import centertable.advancedscalendar.data.room.entity.EntryEntity;
import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;
import centertable.advancedscalendar.data.room.entity.EntryPhotoEntity;
import centertable.advancedscalendar.data.room.entity.EntryPlaceEntity;
import centertable.advancedscalendar.data.room.entity.EntrySexTypeEntity;
import centertable.advancedscalendar.data.room.entity.NotificationEntity;
import centertable.advancedscalendar.data.room.entity.PartnerEntity;
import centertable.advancedscalendar.data.room.entity.UserEntity;
import centertable.advancedscalendar.data.definitions.DatabaseDefs;

/**
 * Created by Rutkay on 27.01.2018.
 */

@Database(entities = {
            AchievementEntity.class,
            EntryEntity.class,
            EntryPartnerEntity.class,
            EntryPhotoEntity.class,
            EntryPlaceEntity.class,
            EntrySexTypeEntity.class,
            NotificationEntity.class,
            PartnerEntity.class,
            UserEntity.class },
        version = 1)
public abstract class ASCDatabase extends RoomDatabase {
    private static volatile ASCDatabase mASCDatabase;

    public static synchronized ASCDatabase getInstance(Context context) {
        if (mASCDatabase == null) {
            mASCDatabase = create(context);
        }
        return mASCDatabase;
    }

    private static ASCDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                ASCDatabase.class,
                DatabaseDefs.ASC_DB_NAME).build();
    }

    public abstract AchievementDao getAchievementDao();

    public abstract EntryDao getEntryDao();

    public abstract EntryPartnerDao getEntryPartnerDao();

    public abstract EntryPhotoDao getEntryPhotoDao();

    public abstract EntryPlaceDao getEntryPlaceDao();

    public abstract EntrySexTypeDao getEntrySexTypeDao();

    public abstract NotificationDao getNotificationDao();

    public abstract PartnerDao getPartnerDao();

    public abstract UserDao getUserDao();
}