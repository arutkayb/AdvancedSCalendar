package centertable.advancedscalendar.data.pojo;



import centertable.advancedscalendar.data.room.entity.NotificationEntity;
import centertable.advancedscalendar.data.definitions.NotificationDefs.NOTIFICATION;

/**
 * Created by Rutkay on 05.02.2018.
 */

public class Notification implements PersistantPojo<NotificationEntity> {
    private long userId;

    //primary unique id for relationship between user and notification
    private long uId;

    //Notifications and unique identifiers of notifications will be mapped
    private NOTIFICATION notificationId;

    //Notifications can be enabled or disabled
    private boolean notificationStatus;

    @Override
    public NotificationEntity generateEntity()
    {
        NotificationEntity notificationEntity= new NotificationEntity(
                uId,
                userId,
                notificationId.getValue(),
                notificationStatus);

        return notificationEntity;
    }

    @Override
    public void loadEntity(NotificationEntity notificationEntity)
    {
        uId = notificationEntity.uId;
        notificationId = NOTIFICATION.fromValue(notificationEntity.notification);
        notificationStatus = notificationEntity.notificationStatus;
        userId = notificationEntity.userId;
    }

    @Override
    public long getPrimaryId()
    {
        return uId;
    }
}
