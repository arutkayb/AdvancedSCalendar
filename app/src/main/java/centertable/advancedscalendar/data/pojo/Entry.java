package centertable.advancedscalendar.data.pojo;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import centertable.advancedscalendar.data.room.entity.EntryEntity;
import centertable.advancedscalendar.data.definitions.EntryDefs;

/**
 * Created by Rutkay on 05.02.2018.
 */


public class Entry implements PersistantPojo<EntryEntity>,Parcelable {
    public static final String ENTRY_PARCELABLE_TAG = "entry_parcelable";
    //unique id for entry
    private long entryId;

    //unique id for entry's user
    private long userId;

    //Date of Entry, part of a composite key
    private String dateString = null;

    //Total duration of entry activities
    private int duration;

    //User note for the entry
    private String note = "";

    //User rating for the overall activities in the entry
    private int rating;

    //Safety status for the overall activities in the entry
    private boolean safetyStatus;

    //Total number of orgasms during the overall entry activities
    private int totalOrg;

    private List<String> entryPhotoLocations = new ArrayList();

    private List<EntryDefs.SEX_TYPE> entrySexTypes = new ArrayList();;

    private List<EntryDefs.PLACE> entryPlaces = new ArrayList();;

    private List<Partner> entryPartners = new ArrayList();;

    public long getEntryId() {
        return entryId;
    }

    public void setEntryId(long entryId) {
        this.entryId = entryId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isSafe() {
        return safetyStatus;
    }

    public void setIsSafe(boolean safetyStatus) {
        this.safetyStatus = safetyStatus;
    }

    public int getTotalOrg() {
        return totalOrg;
    }

    public void setTotalOrg(int totalOrg) {
        this.totalOrg = totalOrg;
    }

    public List<String> getEntryPhotoLocations() {
        return entryPhotoLocations;
    }

    public void setEntryPhotoLocations(List<String> entryPhotoLocations) {
        this.entryPhotoLocations = entryPhotoLocations;
    }

    public List<EntryDefs.SEX_TYPE> getEntrySexTypes() {
        return entrySexTypes;
    }

    public void setEntrySexTypes(List<EntryDefs.SEX_TYPE> entrySexTypes) {
        this.entrySexTypes = entrySexTypes;
    }

    public List<EntryDefs.PLACE> getEntryPlaces() {
        return entryPlaces;
    }

    public void setEntryPlaces(List<EntryDefs.PLACE> entryPlaces) {
        this.entryPlaces = entryPlaces;
    }

    public List<Partner> getEntryPartners() {
        return entryPartners;
    }

    public void setEntryPartners(List<Partner> entryPartners) {
        this.entryPartners = entryPartners;
    }

    @Override
    public EntryEntity generateEntity()
    {
        EntryEntity entryEntity= new EntryEntity(
                entryId,
                userId,
                dateString,
                duration,
                note,
                rating,
                safetyStatus,
                totalOrg);

        return entryEntity;
    }

    @Override
    public void loadEntity(EntryEntity entryEntity)
    {
        entryId = entryEntity.entryId;
        dateString = entryEntity.dateString;
        duration = entryEntity.duration;
        note = entryEntity.note;
        rating = entryEntity.rating;
        safetyStatus = entryEntity.safetyStatus;
        totalOrg = entryEntity.totalOrg;
        userId = entryEntity.userId;
    }

    @Override
    public long getPrimaryId()
    {
        return entryId;
    }

    public Entry(EntryEntity entryEntity){
        loadEntity(entryEntity);
    }

    public Entry(){
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.entryId);
        dest.writeLong(this.userId);
        dest.writeString(this.dateString);
        dest.writeInt(this.duration);
        dest.writeString(this.note);
        dest.writeInt(this.rating);
        dest.writeByte(this.safetyStatus ? (byte) 1 : (byte) 0);
        dest.writeInt(this.totalOrg);
        dest.writeStringList(this.entryPhotoLocations);
        dest.writeList(this.entrySexTypes);
        dest.writeList(this.entryPlaces);
        dest.writeTypedList(this.entryPartners);
    }

    protected Entry(Parcel in) {
        this.entryId = in.readLong();
        this.userId = in.readLong();
        this.dateString = in.readString();
        this.duration = in.readInt();
        this.note = in.readString();
        this.rating = in.readInt();
        this.safetyStatus = in.readByte() != 0;
        this.totalOrg = in.readInt();
        this.entryPhotoLocations = in.createStringArrayList();
        this.entrySexTypes = new ArrayList<EntryDefs.SEX_TYPE>();
        in.readList(this.entrySexTypes, EntryDefs.SEX_TYPE.class.getClassLoader());
        this.entryPlaces = new ArrayList<EntryDefs.PLACE>();
        in.readList(this.entryPlaces, EntryDefs.PLACE.class.getClassLoader());
        this.entryPartners = in.createTypedArrayList(Partner.CREATOR);
    }

    public static final Creator<Entry> CREATOR = new Creator<Entry>() {
        @Override
        public Entry createFromParcel(Parcel source) {
            return new Entry(source);
        }

        @Override
        public Entry[] newArray(int size) {
            return new Entry[size];
        }
    };
}
