package centertable.advancedscalendar.data.pojo;



import centertable.advancedscalendar.data.room.entity.AchievementEntity;
import centertable.advancedscalendar.data.definitions.AchievementDefs.ACHIEVEMENT;

/**
 * Created by Rutkay on 05.02.2018.
 */

public class Achievement implements PersistantPojo<AchievementEntity> {
    private long userId;

    //primary unique id for relationship between user and achievement
    private long uId;

    //Achievement and unique identifiers of the achievements are mapped
    private ACHIEVEMENT achievementId;

    //An achievement can be enabled or disabled
    private boolean achievementStatus;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUId() {
        return uId;
    }

    public void setUId(int uId) {
        this.uId = uId;
    }

    public ACHIEVEMENT getAchievementId() {
        return achievementId;
    }

    public void setAchievementId(ACHIEVEMENT achievementId) {
        this.achievementId = achievementId;
    }

    public boolean isAchievementStatus() {
        return achievementStatus;
    }

    public void setAchievementStatus(boolean achievementStatus) {
        this.achievementStatus = achievementStatus;
    }

    @Override
    public AchievementEntity generateEntity()
    {
        AchievementEntity achievementEntity= new AchievementEntity(
                uId,
                userId,
                achievementId.getValue(),
                achievementStatus);

        return achievementEntity;
    }

    @Override
    public void loadEntity(AchievementEntity achievementEntity)
    {
        uId = achievementEntity.uId;
        achievementId = ACHIEVEMENT.fromValue(achievementEntity.achievement);
        achievementStatus = achievementEntity.achievementStatus;
        userId = achievementEntity.userId;
    }

    @Override
    public long getPrimaryId()
    {
        return uId;
    }
}
