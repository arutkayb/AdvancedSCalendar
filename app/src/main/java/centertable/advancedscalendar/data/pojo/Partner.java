package centertable.advancedscalendar.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;


import centertable.advancedscalendar.data.room.entity.PartnerEntity;
import centertable.advancedscalendar.data.definitions.ProfileDefinitions.GENDER;
import centertable.advancedscalendar.data.definitions.ProfileDefinitions.RELATIONSHIP;
/**
 * Created by Rutkay on 05.02.2018.
 */

public class Partner implements PersistantPojo<PartnerEntity>,Parcelable {
    public static final String PARTNER_PARCELABLE_TAG = "partner_parcelable";

    private long userId;

    //primary id comes after database insertion operation
    private long partnerId;

    //Full name of the Partner
    private String name = "";

    //Type of relationship between the partner and the user
    private RELATIONSHIP relationshipType = RELATIONSHIP.OTHER;

    //Gender of the Partner
    private GENDER gender = GENDER.NONE;

    //Date of Entry
    private String dateCreatedString = "";

    //Personal note for the Partner
    private String note = "";

    //Profile photo location of the Partner
    private String photoLocation = "";

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RELATIONSHIP getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(RELATIONSHIP relationshipType) {
        this.relationshipType = relationshipType;
    }

    public GENDER getGender() {
        return gender;
    }

    public void setGender(GENDER gender) {
        this.gender = gender;
    }

    public String getDateCreatedString() {
        return dateCreatedString;
    }

    public void setDateCreatedString(String dateCreatedString) {
        this.dateCreatedString = dateCreatedString;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhotoLocation() {
        return photoLocation;
    }

    public void setPhotoLocation(String photoLocation) {
        this.photoLocation = photoLocation;
    }

    @Override
    public PartnerEntity generateEntity()
    {
        PartnerEntity partnerEntity= new PartnerEntity(
                partnerId,
                userId,
                name,
                relationshipType.getValue(),
                gender.getValue(),
                dateCreatedString,
                note,
                photoLocation);

        return partnerEntity;
    }

    public Partner(PartnerEntity partnerEntity){
        loadEntity(partnerEntity);
    }

    public Partner(){
    }

    @Override
    public void loadEntity(PartnerEntity partnerEntity)
    {
        partnerId = partnerEntity.partnerId;
        name = partnerEntity.name;
        relationshipType = RELATIONSHIP.fromValue(partnerEntity.relationshipType);
        gender = GENDER.fromValue(partnerEntity.gender);
        dateCreatedString = partnerEntity.dateCreatedString;
        note = partnerEntity.note;
        photoLocation = partnerEntity.photoLocation;
        userId = partnerEntity.userId;
    }

    @Override
    public long getPrimaryId()
    {
        return partnerId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.userId);
        dest.writeLong(this.partnerId);
        dest.writeString(this.name);
        dest.writeInt(this.relationshipType == null ? -1 : this.relationshipType.ordinal());
        dest.writeInt(this.gender == null ? -1 : this.gender.ordinal());
        dest.writeString(this.dateCreatedString);
        dest.writeString(this.note);
        dest.writeString(this.photoLocation);
    }

    protected Partner(Parcel in) {
        this.userId = in.readLong();
        this.partnerId = in.readLong();
        this.name = in.readString();
        int tmpRelationshipType = in.readInt();
        this.relationshipType = tmpRelationshipType == -1 ? null : RELATIONSHIP.values()[tmpRelationshipType];
        int tmpGender = in.readInt();
        this.gender = tmpGender == -1 ? null : GENDER.values()[tmpGender];
        this.dateCreatedString = in.readString();
        this.note = in.readString();
        this.photoLocation = in.readString();
    }

    public static final Creator<Partner> CREATOR = new Creator<Partner>() {
        @Override
        public Partner createFromParcel(Parcel source) {
            return new Partner(source);
        }

        @Override
        public Partner[] newArray(int size) {
            return new Partner[size];
        }
    };
}
