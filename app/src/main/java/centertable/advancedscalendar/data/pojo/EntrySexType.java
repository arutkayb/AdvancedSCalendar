package centertable.advancedscalendar.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import centertable.advancedscalendar.data.room.entity.EntrySexTypeEntity;
import centertable.advancedscalendar.data.definitions.EntryDefs;

public class EntrySexType implements PersistantPojo<EntrySexTypeEntity>,Parcelable {
    private long entryId;
    //primary unique id for relationship between entry and sex type
    private long uId;

    //Sex types and unique identifiers of sex types will be mapped
    private EntryDefs.SEX_TYPE sexTypeId;

    public EntrySexType(int entryId) {
        this.entryId = entryId;
    }

    public EntrySexType(EntryDefs.SEX_TYPE sexTypeId) {
        this.sexTypeId = sexTypeId;
    }

    public long getUId() {
        return uId;
    }

    public void setUId(long mUId) {
        this.uId = mUId;
    }

    public EntryDefs.SEX_TYPE getSexTypeId() {
        return sexTypeId;
    }

    public void setSexTypeId(EntryDefs.SEX_TYPE sexTypeId) {
        this.sexTypeId = sexTypeId;
    }

    @Override
    public EntrySexTypeEntity generateEntity()
    {
        EntrySexTypeEntity entrySexTypeEntity = new EntrySexTypeEntity(
                uId,
                sexTypeId.getValue(),
                entryId);

        return entrySexTypeEntity;
    }

    @Override
    public void loadEntity(EntrySexTypeEntity entrySexTypeEntity)
    {
        uId = entrySexTypeEntity.uId;
        sexTypeId = EntryDefs.SEX_TYPE.fromValue(entrySexTypeEntity.sexTypeId);
        entryId = entrySexTypeEntity.entryId;
    }

    @Override
    public long getPrimaryId()
    {
        return uId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.entryId);
        dest.writeLong(this.uId);
        dest.writeInt(this.sexTypeId == null ? -1 : this.sexTypeId.ordinal());
    }

    protected EntrySexType(Parcel in) {
        this.entryId = in.readLong();
        this.uId = in.readLong();
        int tmpSexTypeId = in.readInt();
        this.sexTypeId = tmpSexTypeId == -1 ? null : EntryDefs.SEX_TYPE.values()[tmpSexTypeId];
    }

    public static final Creator<EntrySexType> CREATOR = new Creator<EntrySexType>() {
        @Override
        public EntrySexType createFromParcel(Parcel source) {
            return new EntrySexType(source);
        }

        @Override
        public EntrySexType[] newArray(int size) {
            return new EntrySexType[size];
        }
    };
}
