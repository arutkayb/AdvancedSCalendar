package centertable.advancedscalendar.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import centertable.advancedscalendar.data.room.entity.EntryPartnerEntity;

public class EntryPartner implements PersistantPojo<EntryPartnerEntity>,Parcelable {
    private long entryId;

    //primary unique id for relationship between entry and partner
    private long uId;

    //partner id as a foreign key
    private long partnerId;

    public EntryPartner(long entryId) {
        this.entryId = entryId;
    }

    public long getUId() {
        return uId;
    }

    public void setUId(long uId) {
        this.uId = uId;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    @Override
    public EntryPartnerEntity generateEntity()
    {
        EntryPartnerEntity entryPartnerEntity= new EntryPartnerEntity(
                uId,
                partnerId,
                entryId);

        return entryPartnerEntity;
    }

    @Override
    public void loadEntity(EntryPartnerEntity entryPartnerEntity)
    {
        uId = entryPartnerEntity.uId;
        partnerId = entryPartnerEntity.partnerId;
        entryId = entryPartnerEntity.entryId;
    }

    @Override
    public long getPrimaryId()
    {
        return uId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.entryId);
        dest.writeLong(this.uId);
        dest.writeLong(this.partnerId);
    }

    protected EntryPartner(Parcel in) {
        this.entryId = in.readLong();
        this.uId = in.readLong();
        this.partnerId = in.readLong();
    }

    public static final Parcelable.Creator<EntryPartner> CREATOR = new Parcelable.Creator<EntryPartner>() {
        @Override
        public EntryPartner createFromParcel(Parcel source) {
            return new EntryPartner(source);
        }

        @Override
        public EntryPartner[] newArray(int size) {
            return new EntryPartner[size];
        }
    };
}
