package centertable.advancedscalendar.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import centertable.advancedscalendar.data.room.entity.EntryPhotoEntity;

public class EntryPhoto implements PersistantPojo<EntryPhotoEntity>,Parcelable {

    private long entryId;
    //primary unique id for relationship between entry and photo
    private long uId;

    private String photoLocation;

    public EntryPhoto(long entryId) {
        this.entryId = entryId;
    }

    public long getUId() {
        return uId;
    }

    public void setUId(long uId) {
        this.uId = uId;
    }

    public String getPhotoLocation() {
        return photoLocation;
    }

    public void setPhotoLocation(String photoLocation) {
        this.photoLocation = photoLocation;
    }

    @Override
    public EntryPhotoEntity generateEntity()
    {
        EntryPhotoEntity entryPhotoEntity= new EntryPhotoEntity(
                uId,
                entryId,
                photoLocation);

        return entryPhotoEntity;
    }

    @Override
    public void loadEntity(EntryPhotoEntity entryPhotoEntity)
    {
        uId = entryPhotoEntity.uId;
        photoLocation = entryPhotoEntity.photoLocation;
        entryId = entryPhotoEntity.entryId;
    }

    @Override
    public long getPrimaryId()
    {
        return uId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.entryId);
        dest.writeLong(this.uId);
        dest.writeString(this.photoLocation);
    }

    protected EntryPhoto(Parcel in) {
        this.entryId = in.readLong();
        this.uId = in.readLong();
        this.photoLocation = in.readString();
    }

    public static final Parcelable.Creator<EntryPhoto> CREATOR = new Parcelable.Creator<EntryPhoto>() {
        @Override
        public EntryPhoto createFromParcel(Parcel source) {
            return new EntryPhoto(source);
        }

        @Override
        public EntryPhoto[] newArray(int size) {
            return new EntryPhoto[size];
        }
    };
}
