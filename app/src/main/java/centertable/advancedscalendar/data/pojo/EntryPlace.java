package centertable.advancedscalendar.data.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import centertable.advancedscalendar.data.room.entity.EntryPlaceEntity;
import centertable.advancedscalendar.data.definitions.EntryDefs;

public class EntryPlace implements PersistantPojo<EntryPlaceEntity>,Parcelable {

    private long entryId;
    //primary unique id for relationship between entry and place
    private long uId;

    private EntryDefs.PLACE placeId;

    public EntryPlace(int entryId) {
        this.entryId = entryId;
    }

    public long getUId() {
        return uId;
    }

    public void setUId(long uId) {
        this.uId = uId;
    }

    public EntryDefs.PLACE getPlaceId() {
        return placeId;
    }

    public void setPlaceId(EntryDefs.PLACE placeId) {
        this.placeId = placeId;
    }

    @Override
    public EntryPlaceEntity generateEntity()
    {
        EntryPlaceEntity entryPlaceEntity= new EntryPlaceEntity(
                uId,
                entryId,
                placeId.getValue());

        return entryPlaceEntity;
    }

    @Override
    public void loadEntity(EntryPlaceEntity entryPlaceEntity)
    {
        uId = entryPlaceEntity.uId;
        placeId = EntryDefs.PLACE.fromValue(entryPlaceEntity.placeId);
        entryId = entryPlaceEntity.entryId;
    }

    @Override
    public long getPrimaryId()
    {
        return uId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.entryId);
        dest.writeLong(this.uId);
        dest.writeLong(this.placeId == null ? -1 : this.placeId.ordinal());
    }

    protected EntryPlace(Parcel in) {
        this.entryId = in.readLong();
        this.uId = in.readLong();
        int tmpPlaceId = in.readInt();
        this.placeId = tmpPlaceId == -1 ? null : EntryDefs.PLACE.values()[tmpPlaceId];
    }

    public static final Parcelable.Creator<EntryPlace> CREATOR = new Parcelable.Creator<EntryPlace>() {
        @Override
        public EntryPlace createFromParcel(Parcel source) {
            return new EntryPlace(source);
        }

        @Override
        public EntryPlace[] newArray(int size) {
            return new EntryPlace[size];
        }
    };
}
