package centertable.advancedscalendar.data.pojo;



import centertable.advancedscalendar.data.definitions.ProfileDefinitions;
import centertable.advancedscalendar.data.room.entity.UserEntity;

/**
 * Created by Rutkay on 05.02.2018.
 */

public class User implements PersistantPojo<UserEntity> {
    //Auto generated primary user id
    private long userId;

    //Remote database uid of the User
    private String remoteUid;

    //Nick name of the User, unique for app
    private String name;

    //App email of the User
    private String email;

    //Gender of the User
    private ProfileDefinitions.GENDER gender = ProfileDefinitions.GENDER.NONE;

    //First sign in of user
    public boolean isNewUser;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRemoteUid() {
        return remoteUid;
    }

    public void setRemoteUid(String remoteUid) {
        this.remoteUid = remoteUid;
    }

    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean newUser) {
        isNewUser = newUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ProfileDefinitions.GENDER getGender() {
        return gender;
    }

    public void setGender(ProfileDefinitions.GENDER gender) {
        this.gender = gender;
    }

    @Override
    public UserEntity generateEntity()
    {
        UserEntity userEntity= new UserEntity(
                userId,
                remoteUid,
                name,
                email,
                gender.getValue());


        return userEntity;
    }

    @Override
    public void loadEntity(UserEntity userEntity)
    {
        userId = userEntity.userId;
        remoteUid = userEntity.remoteUid;
        name = userEntity.name;
        email = userEntity.email;
        gender = ProfileDefinitions.GENDER.fromValue(userEntity.gender);
    }

    @Override
    public long getPrimaryId()
    {
        return userId;
    }

    public User(UserEntity userEntity){
        loadEntity(userEntity);
    }
    public User(){}
}
