package centertable.advancedscalendar.data.pojo;

/**
 * Created by Rutkay on 05.02.2018.
 */

public interface PersistantPojo<Entity>{

    Entity generateEntity();

    void loadEntity(Entity entity);

    long getPrimaryId();

}
