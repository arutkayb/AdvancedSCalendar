package centertable.advancedscalendar.data.definitions;

import android.content.Context;
import android.util.Log;

import centertable.advancedscalendar.R;

/**
 * Created by Rutkay on 06.02.2018.
 */

public class EntryDefs {
    public enum PLACE
    {
        P0(0);
        //TODO generate places here

        private final int id;

        PLACE(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static PLACE fromValue(int value) {
            for (PLACE e: PLACE.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }
        //TODO create placeId-placeName map, get placeNames from resources

    }

    public enum SEX_TYPE
    {
        CASUAL(0), ORAL(1), HANDJOB(2), MASTURBATION(3), FINGER(4), TOY(5), ANAL(6);

        private final int id;

        SEX_TYPE(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static SEX_TYPE fromValue(int value) {
            for (SEX_TYPE e: SEX_TYPE.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }

        public String toString(Context context){
            String res = "";
            try{
                String[] sexTypeList = context.getResources().getStringArray(R.array.sex_types);

                res = sexTypeList[getValue()];
            }catch (ArrayIndexOutOfBoundsException ex){
                res = "";
                Log.e("SEX_TYPE", "Cannot found the string of given sex type" +
                        "with the number: " + String.valueOf(getValue()));
            }catch(Exception ex){
                Log.e("SEX_TYPE", "Cannot found the string of given sex type, exception: " + ex.toString());
            }

            return res;
        }
    }

    //TODO create sexTypeId-sexTypeName map, get sexTypeNames from resources
}
