package centertable.advancedscalendar.data.definitions;

/**
 * Created by Rutkay on 05.02.2018.
 */

public class PreferencesDefs {
    private PreferencesDefs(){}

    public enum THEME {
        COMMON(0),
        MANLY(1),
        GIRLY(2);

        private final int id;

        THEME(int id) {
            this.id = id;
        }

        int getValue() {
            return id;
        }

        public static THEME fromValue(int value) {
            for (THEME e: THEME.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }
    }

}
