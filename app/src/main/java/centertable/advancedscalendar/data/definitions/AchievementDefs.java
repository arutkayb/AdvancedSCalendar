package centertable.advancedscalendar.data.definitions;

/**
 * Created by Rutkay on 06.02.2018.
 */

public class AchievementDefs {
    public enum ACHIEVEMENT
    {
        A0(0),
        A1(1);
        //TODO generate achievements here

        private final int id;

        ACHIEVEMENT(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static ACHIEVEMENT fromValue(int value) {
            for (ACHIEVEMENT e: ACHIEVEMENT.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }
    }
}
