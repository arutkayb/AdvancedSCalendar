package centertable.advancedscalendar.data.definitions;

import android.content.Context;
import android.util.Log;

import centertable.advancedscalendar.R;

/**
 * Created by Rutkay on 05.02.2018.
 */

public class ProfileDefinitions {
    private ProfileDefinitions(){}

    // Mapped with relationship_array resource
    public enum RELATIONSHIP
    {
        OTHER (0),
        LOVER (1),
        FRIEND (2),
        STRANGER (3);

        private final int id;

        RELATIONSHIP(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static RELATIONSHIP fromValue(int value) {
            for (RELATIONSHIP e: RELATIONSHIP.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }

        public String toString(Context context){
            String res = "";
            try{
                String[] relationshipList = context.getResources().getStringArray(R.array.relationship_types);
                
                res = relationshipList[getValue()];
            }catch (ArrayIndexOutOfBoundsException ex){
                res = "";
                Log.e("RELATIONSHIP", "Cannot found the string of given relationship type" +
                        "with the number: " + String.valueOf(getValue()));
            }catch(Exception ex){
                Log.e("RELATIONSHIP", "Cannot found the string of given relationship type, exception: " + ex.toString());
            }
            
            return res;
        }
    }

    // Mapped with gender_array resource
    public enum GENDER {
        NONE(0),
        MALE(1),
        FEMALE(2);

        private final int id;

        GENDER(int id) {
            this.id = id;
        }

        public int getValue() {
            return id;
        }

        public static GENDER fromValue(int value) {
            for (GENDER e: GENDER.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }

        public String toString(Context context){
            String res = "";
            try{
                String[] genderList = context.getResources().getStringArray(R.array.genders);

                res = genderList[getValue()];
            }catch (ArrayIndexOutOfBoundsException ex){
                res = "";
                Log.e("GENDER", "Cannot found the string of given gender type" +
                        "with the number: " + String.valueOf(getValue()));
            }catch(Exception ex){
                Log.e("GENDER", "Cannot found the string of given gender type, exception: " + ex.toString());
            }

            return res;
        }
    }

}
