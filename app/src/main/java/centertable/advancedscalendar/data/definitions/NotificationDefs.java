package centertable.advancedscalendar.data.definitions;

/**
 * Created by Rutkay on 06.02.2018.
 */

public class NotificationDefs {
    public enum NOTIFICATION
    {
        NONE(0),
        ALL(1);

        private final int id;

        NOTIFICATION(int id)
        {
            this.id = id;
        }

        public int getValue()
        {
            return id;
        }

        public static NOTIFICATION fromValue(int value) {
            for (NOTIFICATION e: NOTIFICATION.values()) {
                if (e.getValue() == value) {
                    return e;
                }
            }

            return null;
        }
    }
}
