package centertable.advancedscalendar.data.definitions;

/**
 * Created by Rutkay on 29.01.2018.
 */

public class DatabaseDefs {
    private DatabaseDefs(){}
    public static final String ASC_DB_NAME = "ascdatabase.db";

    public static final String USER_TABLE_NAME = "users";
    public static final String PARTNER_TABLE_NAME = "partners";
    public static final String NOTIFICATION_TABLE_NAME = "notifications";
    public static final String ENTRY_SEX_TYPE_TABLE_NAME = "entry_sex_type";
    public static final String ENTRY_PLACE_TABLE_NAME = "entry_place";
    public static final String ENTRY_PHOTO_TABLE_NAME = "entry_photo";
    public static final String ENTRY_PARTNER_TABLE_NAME = "entry_partner";
    public static final String ENTRY_TABLE_NAME = "entries";
    public static final String ACHIEVEMENT_TABLE_NAME = "achievements";
}
